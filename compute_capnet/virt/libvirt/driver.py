
from oslo_config import cfg
from oslo_log import log as logging
from oslo_utils import importutils

from nova.i18n import _LE, _LI
from nova.virt.libvirt.driver import LibvirtDriver

LOG = logging.getLogger(__name__)

# We specify a default because there's no sense forcing the user to set
# yet another option, and the fact that they're using this driver means
# that of course they want our vif driver too ;).
capnet_libvirt_opts = [
    cfg.StrOpt('vif_driver',
               default=None,
               help='Specify a libvirt VIF driver.  Comment out'
                    ' to revert to regular libvirt behavior.'),
]

cfg.CONF.register_opts(capnet_libvirt_opts, 'libvirt')


class CapnetLibvirtDriver(LibvirtDriver):
    """
    The *sole* purpose of this class is to add a config option,
    vif_driver, for libvirt, so it can choose our Capnet
    """
    
    def __init__(self, virtapi, **kwargs):
        super(CapnetLibvirtDriver, self).__init__(virtapi,**kwargs)
        
        _vif_driver = cfg.CONF.libvirt.vif_driver

        if not _vif_driver:
            LOG.warn(_LE("Vif driver option required, but not specified;"
                         " falling back to default Libvirt vif driver"))
        else:
            LOG.info(_LI("Loading vif driver '%s'"),_vif_driver)
            try:
                self.vif_driver = importutils.import_object_ns('nova.virt',
                                                               _vif_driver)
            except ImportError:
                LOG.exception(_LE("Unable to load the libvirt vif_driver %s"),
                              _vif_driver)
                sys.exit(1)
            pass
        
        pass
    
    pass
