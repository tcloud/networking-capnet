
from oslo_config import cfg
from oslo_log import log as logging

import nova.virt.libvirt.vif

#from neutron import context as n_context

#import networking_capnet.common.topics as cn_topics
#import networking_capnet.common.constants as cn_const
#import networking_capnet.common.interface
#from networking_capnet.common import capnetneutronclient as cneutronclient
#from networking_capnet.common.interface import CapnetInterfaceMixin
#from networking_capnet.api.rpc.capnet_rpc_plugin_api \
#    import CapnetExtPluginApi

LOG = logging.getLogger(__name__)

# Pull in Neutron API client stuff, instead of using our client mixin stuff.
# Basically, the Nova Neutron API implementation pulls in the [neutron] cfg
# section, so we can't also pull it in here.
#from nova.network.neutronv2.api import get_client as get_neutron_client

# Pull in Neutron auth options from Nova config files
#cneutronclient.register_opts_for_nova_conf(cfg.CONF)

class CapnetLibvirtVIFDriver(nova.virt.libvirt.vif.LibvirtGenericVIFDriver): #,
#                             CapnetInterfaceMixin):
    
    def __init__(self):
        super(nova.virt.libvirt.vif.LibvirtGenericVIFDriver,self).__init__()
        #super(CapnetInterfaceMixin,self).__init__()
        
        #self.set_client(cneutronclient.get_client_for_nova_conf())
        
        #self.neutron_context = n_context.get_admin_context_without_session()
        #self.capnet_ext_plugin_rpc = CapnetExtPluginApi(
        #    cn_topics.PLUGIN_CAPNET,self.neutron_context,cfg.CONF.host)
        
        pass
    
    def get_config_capnet(self, instance, vif, image_meta, inst_type,
                          virt_type, host):
        network_id = vif['network']['id']
        #bridge = self._check_network_for_capnet(network_id)
        LOG.info("vif: %s" % (str(vif)))
        bridge = vif['network']['bridge']
        if not bridge:
            LOG.error("no bridge for network %s" % (network_id,))
            raise Exception("no bridge for network %s" % (network_id,))
        else:
            LOG.info("using bridge %s for network %s" % (bridge,network_id))
            pass

        vif['network']['bridge'] = bridge
        
        return self.get_config_ovs_bridge(instance, vif, image_meta, inst_type,
                                          virt_type, host)        

    def plug_capnet(self, instance, vif):
        return self.plug_ovs_bridge(instance, vif)
    
    def unplug_capnet(self, instance, vif):
        return self.unplug_ovs_bridge(instance, vif)
    
    pass
