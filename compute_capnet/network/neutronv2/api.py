
from oslo_config import cfg
from oslo_log import log as logging

from nova.network.neutronv2.api import API as NeutronAPI

from networking_capnet.common import config as cn_config


LOG = logging.getLogger(__name__)

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')


class API(NeutronAPI):
    def __init__(self, skip_policy_check=False):
        super(API, self).__init__(skip_policy_check=skip_policy_check)
        
        self.bridge_mappings = {}
        try:
            for mapping in cfg.CONF.CAPNET.bridge_mappings:
                sma = mapping.split(':')
                self.bridge_mappings[sma[0]] = sma[1]
                pass
            LOG.info("Initialized Capnet bridge_mappings: %s"
                     % (str(self.bridge_mappings)))
        except:
            LOG.exception("Failed to initialize Capnet Neutron Interface Driver;"
                          " will effectively revert to openvswitch driver")
        pass
    
    def _nw_info_build_network(self, port, networks, subnets):
        (network, ovs_interfaceid) = \
            super(API,self)._nw_info_build_network(port,networks,subnets)
            
        LOG.info("port = %s, networks = %s, subnets = %s"
                 % (str(port),str(networks),str(subnets)))
        
        for net in networks:
            if net['id'] == port['network_id'] \
                and 'provider:physical_network' in net \
                and net['provider:physical_network'] in self.bridge_mappings:
                network['physical_network'] = net['provider:physical_network']
                network['bridge'] = \
                    self.bridge_mappings[network['physical_network']]
                ovs_interfaceid = port['id']
                
                LOG.info("updated bridge for port %s network %s %s to %s/%s"
                         % (port['id'],net['name'],net['id'],
                            network['physical_network'],network['bridge']))
                break
            pass
        
        return (network, ovs_interfaceid)
        
    pass
