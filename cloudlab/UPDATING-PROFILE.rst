This is the source code (a ``geni-lib`` script, ``osp-capnet.py``) to create
a CloudLab profile to setup and run Capnet in an OpenStack.  This
profile lives at https://www.cloudlab.us/p/TCloud/OpenStack-Capnet .  It
is basically the CloudLab OpenStack profile
(https://www.cloudlab.us/p/emulab-ops/OpenStack), but adds several
Capnet parameters, creates physical networks for Capnet to use, and also
comes with a set of extension scripts to the CloudLab OpenStack profile
that install Capnet and configure it based on the user's specified
profile parameters.  It relies on this extension support to be present
in the core CloudLab OpenStack profile tarball.

To make the extension tarball, in this directory, do::

    tar -czvf setup-ext-capnet-vX.tar.gz setup-ext-capnet

Then if you need to change the canonical, official one installed on
boss.emulab.net, that the official profile references, get someone with
privs to handle that :).
