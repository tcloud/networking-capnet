Updating a running OpenStack-Capnet Cloudlab experiment
-------------------------------------------------------

If you want to update a running Cloudlab experiment to pull in recent
source code changes, you can update using the following instructions.

SSH to the ``ctl`` node, and do::

    sudo su -

    # (to pick up controller/protocol changes)

    cd /root/setup/capnet/capnet
    git pull
    cd ../capnet.obj
    make install

    # to pick up capnet Neutron plugin server-side changes)

    cd /root/setup/capnet/networking-capnet
    git pull
    cp -p /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak ; \
      rm -rf build/ networking_capnet.egg-info/ ; \
      python setup.py install --install-layout=deb --install-data / -v -f ; \
      diff -u /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini ; \
      cp -p /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini ; \
      rm -f /var/log/neutron/neutron-server.log ; \
      sleep 1 ; \
      systemctl restart neutron-server.service

    # (Note: we save the current ml2 capnet config file; force the python
    # module(s) install; restore the old config that is blown away by
    # module install; wipe the Neutron log file; and restart the service.
    # Change to your liking... i.e. don't wipe the logfile if you prefer
    # to save it.)


If the ``capnet`` library fails to compile, or if you've been told
specifically that you need an update due to a bug or feature, you may
need to pull in updates from the ``libcap`` or ``openmul``
repositories::

    cd /root/setup/capnet/libcap
    git pull
    cd ../libcap.obj
    make install

    cd /root/setup/capnet/openmul
    git pull
    cd ../openmul.obj
    make install


Next, SSH to the ``nm`` node, and do::

    sudo su -

    # (to pick up controller/protocol changes:)

    cd /root/setup/capnet/capnet
    git pull
    cd ../capnet.obj
    make install

    # (to pick up capnet Neutron plugin server-side changes:)

    cd /root/setup/capnet/networking-capnet
    git pull

    cp -p /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak ; \
      rm -rf build/ networking_capnet.egg-info/ ; \
      python setup.py install --install-layout=deb --install-data / -v -f ; \
      diff -u /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini ; \
      cp -p /etc/neutron/plugins/ml2/ml2_conf_capnet.ini.bak \
        /etc/neutron/plugins/ml2/ml2_conf_capnet.ini ; \
      rm -f /var/log/neutron/neutron-plugin-capnet-agent.log ; \
      sleep 1 ; \
      service neutron-plugin-capnet-agent restart


The instructions for the compute nodes are the same as the NM node, but
you don't have to reinstall the controller/protocol stuff, because we
don't run controllers on the compute nodes yet.

In fact, updating anything on the compute nodes is almost never
necessary.  Right now, it almost always suffices to update only the NM
because we don't have a distributed controller yet.
