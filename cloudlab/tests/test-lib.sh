#!/bin/sh

set -x

#
# The dir left by the Cloudlab setup scripts.  Lots of stuff is here,
# including openstack credential files!
#
OURDIR=/root/setup

#
# This is the Cloudlab setup script install library.  It has lots of
# useful variables.
#
SETUPLIB=/tmp/setup/setup-lib.sh

PSSH='/usr/bin/parallel-ssh -t 0 -O StrictHostKeyChecking=no '
PSCP='/usr/bin/parallel-scp -t 0 -O StrictHostKeyChecking=no '
SSH='/usr/bin/ssh -o StrictHostKeyChecking=no '
SCP='/usr/bin/ssh -o StrictHostKeyChecking=no '
