#!/bin/sh

#
# A script (untested, for archival purposes) that runs the commands used
# to generated the data for the OSDI 2016 submission.
#

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/test-lib.sh"
. "$SETUPLIB"

HOMEDIR=`readlink -f ~`
TESTBASEDIRNAME="$HOMEDIR/capnet-tests"
TESTBASENAME="test"
SIZES="50 100 150 200"
ITERATIONS=5
UTENANT="tenant-0"
STENANT="service-0"
NETWORK="capnetlan-1-net"
BRIDGE="br-capnetlan-1"

if [ $# -gt 1 ]; then
    TESTBASEDIRNAME="$1"
fi
if [ $# -gt 2 ]; then
    TESTBASENAME="$2"
fi

for size in $SIZES ; do
    j=0
    while [ $j -lt $ITERATIONS ]; do 
	j=`expr $j + 1`

        #
        # Clean out tenants and switches to make sure everything's
        # clean.  Sometimes a workflow agent doesn't get fully deleted,
        # probably because the switch restart acts before the capnet
        # agent can delete the wfagents.  So we just add sleeps and do
        # it all twice.
        #
        echo "Cleaning out tenant $STENANT ..."
	$DIRNAME/test-cleanup-tenant.sh $STENANT
	echo "Cleaning out tenant $UTENANT ..."
	$DIRNAME/test-cleanup-tenant.sh $UTENANT
	sleep 4
	echo "Cleaning out bridge $BRIDGE ..."
	$DIRNAME/test-cleanup-sw-restart-controller.sh $BRIDGE

	echo "Cleaning out tenant $STENANT ..."
	$DIRNAME/test-cleanup-tenant.sh $STENANT
	echo "Cleaning out tenant $UTENANT ..."
	$DIRNAME/test-cleanup-tenant.sh $UTENANT
	sleep 4
	echo "Cleaning out bridge $BRIDGE ..."
	$DIRNAME/test-cleanup-sw-restart-controller.sh $BRIDGE

        #
        # Ok, run the test.
        #
	echo "Running test-$size-$j ..."
	testname="${TESTBASENAME}-${size}-${j}"
	testdir="${TESTBASEDIRNAME}/${testname}"
	mkdir -p "$testdir"
	cdir=`cwd`
	cd $testdir
	$DIRNAME/test-hadoop.sh "$testdir" "$testname" $UTENANT $STENANT \
            $NETWORK $BRIDGE $size | tee $testdir/test.log 2>&1
	echo "Finished $testname (results in $testdir)..."
	cd $cdir
    done
done

echo "Done with $ITERATIONS iterations of test sizes $SIZES ."

exit 0
