#!/bin/sh

#
# A simple script that cleans up a single tenant: removes all its wfas
# and VMs.  Everything else is left alone.
#

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/test-lib.sh"

TENANTNAME=$1

if [ -z "$TENANTNAME" ]; then
    echo "ERROR: must specify tenant name"
    exit 1
fi

if [ ! -f $OURDIR/$TENANTNAME-user-openrc.sh ]; then
    echo "ERROR: no Cloudlab openstack creds for tenant $TENANTNAME !"
    exit 2
fi

. $OURDIR/$TENANTNAME-user-openrc.sh

# Delete all VMs:
vmlist=`nova list | awk ' / [0-9a-fA-F]*-[0-9a-fA-F-]* / { print $2 }' | xargs`
if [ -n "$vmlist" ]; then
    nova delete $vmlist
fi

. $OURDIR/admin-openrc.sh

TENANTID=`openstack project show $TENANTNAME | awk " / id / {print \\$4}"`

WFA_IDS=`neutron capnet-wfagent-list | awk " / $TENANTID / { print \\$2 }" | xargs`

for wfa_id in $WFA_IDS ; do
    neutron capnet-wfagent-delete $wfa_id
done

echo "Cleaned up tenant $TENANTNAME $TENANTID"

exit 0
