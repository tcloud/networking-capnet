#!/bin/sh

#
# Another test script; this one runs hadoop on a bunch of nodes.
#
# ./test-hadoop.sh <testdir> <user-tenant> <service-tenant> \
#     <networkname> <bridgename> <num-slaves> [hadoop-args ...]
#
# First we fire off the service wfagent; then all the VMs (waiting for
# them to boot); and then the user wfagent.
#   (Note: the master VM gets m1.large so it can create nearly 80GB input file
#   if the job needs.  The other VMs get m1.medium, so they have 4GB RAM.)
#
# To figure out when the test is done, we keep ssh'ing to the NM and
# looking through the user wfa's log looking for ^DONE$ .
#
# At the end, we have to scp the workflow agent logfile and controller
# logfile over here to the ctl node into our testdir.
#

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/test-lib.sh"
. "$SETUPLIB"

if [ $# -lt 7 ]; then
    echo "USAGE: $0 <testdir> <testname> <user-tenant> <service-tenant> <networkname>"
    echo "          <bridgename> <num-slaves> [<hadoop-args>]"
    exit 1
fi

#
# Launching all slaves from one nova command causes a Neutron lock!
# So don't do this for now by default.
#
FASTBOOT=1
#
# If WFAs don't show progress after this much time, abort.
#
WFATIMEOUT=1800
#
# If certain tests have already succeeded, just skip!
#
SKIPSUCCESSFUL=1

TESTDIR=$1
shift
TESTNAME=$1
shift
UTENANT=$1
shift
STENANT=$1
shift
NETWORK=$1
shift
BRIDGE=$1
shift
NSLAVES=$1
shift
ARGS=""

if [ $# -gt 0 ]; then
    ARGS="$@"
fi

if [ $SKIPSUCCESSFUL -eq 1 -a -f $TESTDIR/SUCCESS ]; then
    echo "*** Not retesting Hadoop: $TESTNAME already Done successfully (results in $TESTDIR)!"
    exit 0
fi
if [ -f $TESTDIR/SKIP ]; then
    echo "*** WARNING: immediate skip triggered before tests!"
    exit 0
fi

#
# Create some parallel SSH command lines...
#
PCHOSTS=""
for cn in $COMPUTENODES ; do
    fqdn=`getfqdn $cn`
    PCHOSTS="$PCHOSTS -H $fqdn"
done

PNHOSTS="$PCHOSTS"
fqdn=`getfqdn $NETWORKMANAGER`
PNHOSTS="-H $fqdn $PCHOSTS"

echo "*** Testing hadoop: $TESTDIR $TESTNAME $UTENANT $STENANT $NETWORK $BRIDGE $NSLAVES \"$ARGS\" ..."

mkdir -p $TESTDIR
cd $TESTDIR

# NB: Be admin when we fire off wfas...
. "$OURDIR/admin-openrc.sh"

NETWORKID=`openstack network show $NETWORK | awk ' / id / {print $4}'`
UTENANTID=`openstack project show $UTENANT | awk ' / id / {print $4}'`
STENANTID=`openstack project show $STENANT | awk ' / id / {print $4}'`

#
# Create the service WFA.
#
STENANTWFANAME="$STENANT-hadoop"
neutron capnet-wfagent-create --tenant-id $STENANTID --name $STENANTWFANAME \
    --master --wfapp-path /usr/bin/capnet-wfagent-service-tenant-hadoop-membrane \
    $NETWORK
if [ ! $? -eq 0 ]; then
    echo "ERROR: creating service wfa; aborting!"
    exit 1
fi
while [ 1 -eq 1 ]; do
    STENANTWFAID=`neutron capnet-wfagent-list | awk " / $STENANTWFANAME / { print \\$2 }"`
    if [ ! "x$STENANTWFAID" = "x" ]; then
	break
    fi
done

sleep 5

#
# Fire off VMs.
# NB: Be the tenant user when we fire off VMs.
#
. "$OURDIR/$UTENANT-user-openrc.sh"

nova boot --image hadoop --flavor m1.large --nic net-id=$NETWORKID master
if [ ! $? -eq 0 ]; then
    echo "ERROR: failed to create master VM; aborting!"
    exit 1
fi
nova boot --image hadoop --flavor m1.medium --nic net-id=$NETWORKID resourcemanager
if [ ! $? -eq 0 ]; then
    echo "ERROR: failed to create resourcemanager VM; aborting!"
    exit 1
fi
if [ $FASTBOOT -eq 1 ]; then
    nova boot --image hadoop --flavor m1.medium --nic net-id=$NETWORKID --min-count $NSLAVES slave
    if [ ! $? -eq 0 ]; then
	echo "ERROR: failed to create slave VMs; aborting!"
	exit 1
    fi
else
    i=1
    while [ $i -le $NSLAVES ]; do
	nova boot --image hadoop --flavor m1.medium --nic net-id=$NETWORKID slave-$i
        if [ ! $? -eq 0 ]; then
            print "ERROR: failed to create slave-$i VM; aborting!"
            exit 1
        fi
        i=`expr $i + 1`
    done
fi

#
# Wait for all nodes to have booted, then wait 10 seconds more...
#
allrunning=0
while [ $allrunning -eq 0 ]; do
    echo "*** Still waiting for nodes to reach Running state ..."
    sleep 5
    vsl=`nova list --limit -1 | awk ' / [0-9a-fA-F]*-[0-9a-fA-F-]* / { print $10 }' | xargs`
    if [ ! $? -eq 0 ]; then
	echo "Error listing nova VMs; will try again!"
	continue
    fi
    allrunning=1
    for status in $vsl ; do
	if [ ! "x$status" = "xRunning" ]; then
	    allrunning=0
	    break
	fi
    done
done

#
# Hedge our bets and hope all have booted to network by this point :)
#
echo "*** Sleeping 16 seconds to let all VMs come online, hopefully ..."
sleep 16

#
# Grab the flow tables for all switches:
#
mkdir -p $TESTDIR/out.flowtables-post-node-boot $TESTDIR/err.flowtables-post-node-boot
$PSSH $PNHOSTS \
    -o $TESTDIR/out.flowtables-post-node-boot \
    -e $TESTDIR/err.flowtables-post-node-boot \
    ovs-ofctl --protocol=OpenFlow13 dump-flows $BRIDGE

# NB: Be admin when we fire off wfas...
. "$OURDIR/admin-openrc.sh"

#
# Create the user wfa.
#
UTENANTWFANAME="$UTENANT-hadoop"
neutron capnet-wfagent-create --tenant-id $UTENANTID --name $UTENANTWFANAME \
    --master --wfapp-path /usr/bin/capnet-wfagent-user-tenant-hadoop-membrane \
    $NETWORK
if [ ! $? -eq 0 ]; then
    echo "ERROR: creating user wfa; aborting!"
    exit 1
fi
while [ 1 -eq 1 ]; do
    UTENANTWFAID=`neutron capnet-wfagent-list | awk " / $UTENANTWFANAME / { print \\$2 }"`
    if [ ! "x$UTENANTWFAID" = "x" ]; then
	break
    fi
done

#
# Wait for everything to finish, then sync back logfiles.
#
MULLOGFILE="/var/tmp/${BRIDGE}-mul-core.log"
CONTROLLERLOGFILE_SHORT="${BRIDGE}-controller.log"
CONTROLLERLOGFILE="/var/tmp/${CONTROLLERLOGFILE_SHORT}"
UWFALOGFILE_SHORT="wfagent.${UTENANTWFANAME}.${UTENANTWFAID}.log"
UWFALOGFILE="/var/tmp/${UWFALOGFILE_SHORT}"
SWFALOGFILE_SHORT="wfagent.${STENANTWFANAME}.${STENANTWFAID}.log"
SWFALOGFILE="/var/tmp/${SWFALOGFILE_SHORT}"
STATUS_UWFA=1
STATUS_SWFA=2
STAT_UWFA=""
STAT_SWFA=""
WAITED=0
while [ 1 -eq 1 ]; do
    if [ ! $STATUS_UWFA -eq 0 ]; then
	$SSH $NETWORKMANAGER \
             grep -E '\(^DONE\$\)\|\(^Traceback\)' $UWFALOGFILE
	STATUS_UWFA=$?
	newstat=`$SSH $NETWORKMANAGER stat -c "'%s %y'" $UWFALOGFILE`
	if [ "$newstat" != "$STAT_UWFA" ]; then
	    WAITED=0
	    echo "*** Progress in UWFA ($STAT_UWFA  ->  $newstat)";
	    STAT_UWFA="$newstat"
	fi
    fi
    if [ ! $STATUS_SWFA -eq 0 ]; then
	$SSH $NETWORKMANAGER \
             grep -E "'(^Finished setting up Hadoop)|(^Traceback)'" $SWFALOGFILE
	STATUS_SWFA=$?
	newstat=`$SSH $NETWORKMANAGER stat -c "'%s %y'" $SWFALOGFILE`
	if [ "$newstat" != "$STAT_SWFA" ]; then
	    WAITED=0
	    echo "*** Progress in SWFA ($STAT_SWFA  ->  $newstat)";
	    STAT_SWFA="$newstat"
	fi
    fi
    if [ $STATUS_UWFA -eq 0 -a $STATUS_SWFA -eq 0 ]; then
	break
    fi
    sleep 10
    WAITED=`expr $WAITED + 10`
    if [ $WFATIMEOUT -gt 0 -a $WAITED -ge $WFATIMEOUT ]; then
	echo "*** ERROR: timeout, WFAs making no progress; aborting!"
	break
    fi
    if [ -f $TESTDIR/SKIP ]; then
        echo "*** ERROR: immediate skip triggered during test!"
        break
    fi
done

echo "*** Test has completed, fetching logs ..."

$SCP $NETWORKMANAGER:$CONTROLLERLOGFILE $TESTDIR/
$SCP $NETWORKMANAGER:$MULLOGFILE $TESTDIR/
$SCP $NETWORKMANAGER:$UWFALOGFILE $TESTDIR/
$SCP $NETWORKMANAGER:$SWFALOGFILE $TESTDIR/
$SCP $NETWORKMANAGER:/var/tmp/cnc.metadata.\* $TESTDIR/

#
# Grab the flow tables for all switches again:
#
mkdir -p $TESTDIR/out.flowtables-post-hadoop $TESTDIR/err.flowtables-post-hadoop
$PSSH $PNHOSTS \
    -o $TESTDIR/out.flowtables-post-hadoop \
    -e $TESTDIR/err.flowtables-post-hadoop \
    ovs-ofctl --protocol=OpenFlow13 dump-flows $BRIDGE

#
# Process the results a bit.
#
UERR=0
UDONE=0
SERR=0
grep -E '^Traceback' $TESTDIR/${UWFALOGFILE_SHORT}
UERR=$?
if [ $UERR -eq 0 ]; then
    UMSG="uWfaErr=Exception!"
else
    UMSG=""
fi
grep -E '^DONE$' $TESTDIR/${UWFALOGFILE_SHORT}
UDONE=$?
if [ $UDONE -eq 0 ]; then
    DONEMSG="uWfaDone=Nope!"
else
    DONEMSG=""
fi
grep -E '^Traceback' $TESTDIR/${SWFALOGFILE_SHORT}
SERR=$?
if [ $SERR -eq 0 ]; then
    SMSG="sWfaErr=Exception!"
else
    SMSG=""
fi
NOPROGMSG=""
if [ $WFATIMEOUT -gt 0 -a $WAITED -ge $WFATIMEOUT ]; then
    NOPROGMSG="No Progress"
fi
if [ -f $TESTDIR/SKIP ]; then
    NOPROGMSG="SKIPPED $NOPROGMSG"
fi

if [ $UERR -ne 0 -a $UDONE -eq 0 -a $SERR -ne 0 ]; then
    touch $TESTDIR/SUCCESS
    if [ "x$TESTEMAIL" != "x" -a "x$TESTMAILSUCCESS" != "x" -a $TESTMAILSUCCESS -eq 1 ]; then
	echo "Success ${TESTNAME}" \
	    | mailx -s "Success ${TESTNAME}" \
	    -a $TESTDIR/${UWFALOGFILE_SHORT} -a $TESTDIR/${SWFALOGFILE_SHORT} \
	    "$TESTEMAIL"
    fi
else
    touch $TESTDIR/FAILURE
    if [ -n "$TESTMAILFAILURE" -a $TESTMAILFAILURE -eq 1 ]; then
	# Controller logfiles get large; just send last 500 lines in attachment
	TMPF=`mktemp`
	tail -500 $TESTDIR/${CONTROLLERLOGFILE_SHORT} > $TMPF
	BTMPF=`mktemp`
	echo "ERROR ${TESTNAME} $NOPROGMSG $DONEMSG $UMSG $SMSG (UERR=$UERR UDONE=$UDONE SERR=$SERR)" > $BTMPF
	if [ $UERR -eq 0 ]; then
	    start=`grep -n Traceback $TESTDIR/${UWFALOGFILE_SHORT} | cut -d: -f 1`
	    total=`wc -l $TESTDIR/${UWFALOGFILE_SHORT} | awk '/ / { print \$1 }'`
	    tlines=`expr $total - $start + 10`

	    echo "\nException from uWFA:\n" >> $BTMPF
	    tail -$tlines $TESTDIR/${UWFALOGFILE_SHORT} >> $BTMPF
	fi
	if [ $SERR -eq 0 ]; then
	    start=`grep -n Traceback $TESTDIR/${SWFALOGFILE_SHORT} | cut -d: -f 1`
	    total=`wc -l $TESTDIR/${SWFALOGFILE_SHORT} | awk '/ / { print \$1 }'`
	    tlines=`expr $total - $start + 10`

	    echo "\nException from sWFA:\n" >> $BTMPF
	    tail -$tlines $TESTDIR/${SWFALOGFILE_SHORT} >> $BTMPF
	fi
	cat $BTMPF | mailx -s "ERROR ${TESTNAME} $DONEMSG $UMSG $SMSG" \
	    -a $TESTDIR/${UWFALOGFILE_SHORT} -a $TESTDIR/${SWFALOGFILE_SHORT} \
	    -a $TMPF "$TESTEMAIL"
	rm -f $TMPF
	rm -f $BTMPF
    fi
fi
    
echo "*** Done ($TESTNAME); results in $TESTDIR"

exit 0
