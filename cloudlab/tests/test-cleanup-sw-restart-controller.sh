#!/bin/sh

set -x

#
# A simple program that pssh's to all the nm node and stops its
# neutron-plugin-capnet-agent clears the flows in the specified capnet
# bridge ; stops the neutron-plugin-capnet-agent on all compute nodes
# and removes all flows from the specified capnet bridge ; restarts the
# neutron-plugin-capnet-agent on the NM; and restarts the
# neutron-plugin-capnet-agent on all compute nodes.
#

DIRNAME=`dirname $0`
. "$DIRNAME/test-lib.sh"

. "$SETUPLIB"

BRIDGE=$1
if [ -z "$BRIDGE" ]; then
    echo "ERROR: must specify a bridge as argument"
    exit 1
fi

$SSH $NETWORKMANAGER service neutron-l3-agent stop
$SSH $NETWORKMANAGER service neutron-metadata-agent stop
$SSH $NETWORKMANAGER service neutron-dhcp-agent stop
$SSH $NETWORKMANAGER service neutron-plugin-capnet-agent stop
$SSH $NETWORKMANAGER ovs-ofctl del-flows $BRIDGE
$SSH $NETWORKMANAGER sh -c "'rm -fv /var/tmp/*'"

PHOSTS=""
for cn in $COMPUTENODES ; do
    fqdn=`getfqdn $cn`
    PHOSTS="$PHOSTS -H $fqdn"
done

$PSSH $PHOSTS service neutron-plugin-capnet-agent stop
$PSSH $PHOSTS sh -c "'rm -fv /var/tmp/*'"
$PSSH $PHOSTS ovs-ofctl del-flows $BRIDGE

$SSH $NETWORKMANAGER ovs-vsctl set bridge $BRIDGE protocols=OpenFlow10,OpenFlow13
$PSSH $PHOSTS ovs-vsctl set bridge $BRIDGE protocols=OpenFlow10,OpenFlow13

service neutron-server stop
sleep 4
service neutron-server restart
sleep 8

$SSH $NETWORKMANAGER service neutron-dhcp-agent start
$SSH $NETWORKMANAGER service neutron-l3-agent start
$SSH $NETWORKMANAGER service neutron-metadata-agent start
sleep 4
$SSH $NETWORKMANAGER service neutron-plugin-capnet-agent start

$PSSH $PHOSTS service neutron-plugin-capnet-agent start

exit 0
