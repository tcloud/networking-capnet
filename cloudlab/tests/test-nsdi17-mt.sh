#!/bin/sh

#
# A script (untested, for archival purposes) that runs the commands used
# to generated the data for the OSDI 2016 submission.
#

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/test-lib.sh"
. "$SETUPLIB"

HOMEDIR=`readlink -f ~`
TESTBASEDIRNAME="$HOMEDIR/capnet-tests"
TESTBASENAME="test"
NTENANTS=1
SIZES="100"
#SIZES="100 150"
#SIZES="50 100 150 200"
#SIZES="200 50 100 150"
ITERATIONS=5
UTENANT="tenant-0"
STENANT="service-0"
NETWORK="capnetlan-1-net"
BRIDGE="br-capnetlan-1"

SKIPSUCCESSFUL=1

if [ $# -ge 1 ]; then
    TESTBASEDIRNAME="$1"
fi
if [ $# -ge 2 ]; then
    TESTBASENAME="$2"
fi
if [ $# -ge 3 ]; then
    NTENANTS=$3
fi

for size in $SIZES ; do
    j=0
    while [ $j -lt $ITERATIONS ]; do 
	j=`expr $j + 1`
	testname="${TESTBASENAME}-${size}-${j}"
	testdir="${TESTBASEDIRNAME}/${testname}"

	if [ $SKIPSUCCESSFUL -eq 1 -a -f $TESTDIR/SUCCESS ]; then
	    echo "*** Not retesting Hadoop: $testname already Done successfully (results in $testdir)!"
	    exit 0
	fi
	if [ -f $TESTDIR/SKIP ]; then
	    echo "*** WARNING: immediate skip triggered before tests!"
	    exit 0
	fi

        #
        # Clean out tenants and switches to make sure everything's
        # clean.  Sometimes a workflow agent doesn't get fully deleted,
        # probably because the switch restart acts before the capnet
        # agent can delete the wfagents.  So we just add sleeps and do
        # it all twice.
        #
	t=0
	while [ $t -lt $NTENANTS ]; do
	    UTENANT="tenant-$t"
	    STENANT="service-$t"
            echo "Cleaning out tenant $STENANT ..."
	    $DIRNAME/test-cleanup-tenant.sh $STENANT
	    sleep 4
	    echo "Cleaning out tenant $UTENANT ..."
	    $DIRNAME/test-cleanup-tenant.sh $UTENANT
	    sleep 4
	    t=`expr $t + 1`
	done
	echo "Cleaning out bridge $BRIDGE ..."
	$DIRNAME/test-cleanup-sw-restart-controller.sh $BRIDGE

	#
	# Ensure all wfas are gone... XXX
	#
	. /root/setup/admin-openrc.sh
	while [ 1 -eq 1 ]; do
	    wfas=`neutron capnet-wfagent-list --all-tenants | awk '/ / {print \$2}' | grep -v ^id`
	    if [ "x$wfas" = "x" ]; then
		break
	    fi
	    for wfa in $wfas; do
		neutron capnet-wfagent-delete $wfa
	    done
	    sleep 4
	done

	t=0
	while [ $t -lt $NTENANTS ]; do
	    UTENANT="tenant-$t"
	    STENANT="service-$t"
            echo "Cleaning out tenant $STENANT ..."
	    $DIRNAME/test-cleanup-tenant.sh $STENANT
	    sleep 4
	    echo "Cleaning out tenant $UTENANT ..."
	    $DIRNAME/test-cleanup-tenant.sh $UTENANT
	    sleep 4
	    t=`expr $t + 1`
	done
	echo "Cleaning out bridge $BRIDGE ..."
	$DIRNAME/test-cleanup-sw-restart-controller.sh $BRIDGE

        #
        # Ok, run the test.
        #
	echo "Running test-$size-$j ..."
	mkdir -p "$testdir"
	cdir=`cwd`
	cd $testdir
	$DIRNAME/test-hadoop-mt.sh "$testdir" "$testname" tenant service $NTENANTS \
	    $NETWORK $BRIDGE $size | tee $testdir/test.log 2>&1
	echo "Finished $testname (results in $testdir)..."
	cd $cdir
    done
done

echo "Done with $ITERATIONS iterations of test sizes $SIZES ."

exit 0
