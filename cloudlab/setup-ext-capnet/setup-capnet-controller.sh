#!/bin/sh

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

if [ "$HOSTNAME" != "$CONTROLLER" ]; then
    exit 0;
fi

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

##
## First, we *don't* setup OVS stuff for Capnet physical networks.
## It's unnecessary for Neutron.
##
#$DIRNAME/setup-ovs-node.sh

##
## Second, however, we do all the Neutron config Capnet needs:
##
crudini --set /etc/neutron/neutron.conf DEFAULT core_plugin capnet

#crudini --set /etc/neutron/neutron.conf \
#    DEFAULT core_plugin networking_capnet.plugins.ml2.plugin.CapnetMl2Plugin

crudini --set /etc/neutron/neutron.conf DEFAULT api_extensions_path \
    /usr/lib/python2.7/dist-packages/networking_capnet/extensions

ML2TYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 type_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 type_drivers "capnet,$ML2TYPES"
ML2TENANTTYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 tenant_network_types`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 tenant_network_types "capnet,$ML2TENANTTYPES"
ML2MECHDRVS=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 mechanism_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 mechanism_drivers "capnet,$ML2MECHDRVS"

ML2EXT=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 extension_drivers`
if [ ! -z "$ML2EXT" ] ; then
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet,$ML2EXT"
else
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet"
fi

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini capnet pythonpath \
    /opt/tcloud/capnet/lib/python2.7/site-packages

crudini --set /etc/neutron/neutron.conf DEFAULT verbose True
crudini --set /etc/neutron/neutron.conf DEFAULT debug True

##
## Setup our physical Capnet connections -- but only available provider
## networks, not bridge mappings (because the controller has no bridges).
##
capnet_networks=""
for lan in $DATACAPNETLANS ; do
    if [ -n "${capnet_networks}" ]; then
	capnet_networks="${capnet_networks},"
    fi
    . $OURDIR/info.${lan}
    capnet_networks="${capnet_networks}${lan}"
done

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet capnet_networks "$capnet_networks"

##
## Hack the initscript a bit, to add ml2_conf_capnet.ini as a config file.
##
echo 'CONFIG_FILE="/etc/neutron/neutron.conf --config-file=/etc/neutron/plugins/ml2/ml2_conf_capnet.ini"' >> /etc/default/neutron-server

##
## Also, until we have migrations worked out, install the DB schema.
##
mysql neutron < $OURDIR/capnet/networking-capnet/networking_capnet/db/create.sql

##
## Finally, modify the neutron config a bit to help it scale to big
## instance workloads better.
##
## Whoops, these are supposed to be not just a large amount; they should
## be set to the same number as api_workers, and that defaults to the
## number of CPUs in the system.
##
ncpus=`cat /proc/cpuinfo | grep -i 'processor.*:' | wc -l`
crudini --set /etc/neutron/neutron.conf database max_overflow $ncpus
crudini --set /etc/neutron/neutron.conf database max_pool_size $ncpus
crudini --set /etc/neutron/neutron.conf database pool_timeout 30

##
## Ok, restart Neutron
##
service_restart neutron-server

##
## Finally, modify the nova config a bit to help it scale to big
## instance workloads better.
##
crudini --set /etc/nova/nova.conf api_database max_overflow $ncpus
crudini --set /etc/nova/nova.conf api_database max_pool_size $ncpus
crudini --set /etc/nova/nova.conf api_database pool_timeout 30

service nova-api restart
service nova-scheduler restart
service nova-conductor restart

#
# Also, kill this off.  It seems to cause locking problems in Neutron!
#
systemctl stop openstack-slothd
pkill openstack-slothd
systemctl disable openstack-slothd
