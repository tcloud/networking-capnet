#!/bin/sh

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

if [ "$HOSTNAME" != "$NETWORKMANAGER" ]; then
    exit 0;
fi
if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

##
## First, we setup OVS stuff for Capnet physical networks.
##
$DIRNAME/setup-ovs-node.sh

##
## Second, apply our configuration
##
ML2TYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 type_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 type_drivers "capnet,$ML2TYPES"
ML2TENANTTYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 tenant_network_types`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 tenant_network_types "capnet,$ML2TENANTTYPES"
ML2MECHDRVS=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 mechanism_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 mechanism_drivers "capnet,$ML2MECHDRVS"

ML2EXT=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 extension_drivers`
if [ ! -z "$ML2EXT" ] ; then
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet,$ML2EXT"
else
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet"
fi

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini DEFAULT interface_driver \
    networking_capnet.agent.linux.interface.CapnetOVSInterfaceDriver

crudini --set /etc/neutron/neutron.conf DEFAULT api_extensions_path \
    /usr/lib/python2.7/dist-packages/networking_capnet/extensions

crudini --set /etc/neutron/dhcp_agent.ini DEFAULT interface_driver \
    networking_capnet.agent.linux.interface.CapnetOVSInterfaceDriver
crudini --set /etc/neutron/l3_agent.ini DEFAULT interface_driver \
    networking_capnet.agent.linux.interface.CapnetOVSInterfaceDriver
crudini --set /etc/neutron/metering_agent.ini DEFAULT interface_driver \
    networking_capnet.agent.linux.interface.CapnetOVSInterfaceDriver

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini capnet pythonpath \
    /opt/tcloud/capnet/lib/python2.7/site-packages

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini capnet \
    neutron_metadata_service_location l3

##
## Setup our custom Dnsmasq config.
##
crudini --set /etc/neutron/dhcp_agent.ini DEFAULT dhcp_driver \
    networking_capnet.agent.linux.dhcp.CapnetDnsmasq
crudini --set /etc/neutron/dhcp_agent.ini capnet dnsmasq_config_file \
    /etc/neutron/capnet-dnsmasq-neutron.conf

cat <<EOF > /etc/neutron/capnet-dnsmasq-neutron.conf
dhcp-option-force=26,1454
log-queries
log-dhcp
no-resolv
EOF

##
## Setup our physical Capnet connections
##
capnet_networks=""
bridge_mappings=""
for lan in $DATACAPNETLANS ; do
    if [ -n "${capnet_networks}" ]; then
	capnet_networks="${capnet_networks},"
    fi
    if [ -n "${bridge_mappings}" ]; then
	bridge_mappings="${bridge_mappings},"
    fi
    . $OURDIR/info.${lan}
    capnet_networks="${capnet_networks}${lan}"
    bridge_mappings="${bridge_mappings}${lan}:${DATABRIDGE}"
done

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet bridge_mappings "$bridge_mappings"
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet capnet_networks "$capnet_networks"

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet hosts_controllers True
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet master_controller_ip "$MGMTIP"
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet multi_controller_mode False
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet hosts_workflow_apps True

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    DEFAULT verbose True
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    DEFAULT debug True
crudini --set /etc/neutron/neutron.conf DEFAULT verbose True
crudini --set /etc/neutron/neutron.conf DEFAULT debug True

crudini --set /etc/neutron/dhcp_agent.ini \
    capnet bridge_mappings "$bridge_mappings"
crudini --set /etc/neutron/l3_agent.ini \
    capnet bridge_mappings "$bridge_mappings"
crudini --set /etc/neutron/metering_agent.ini \
    capnet bridge_mappings "$bridge_mappings"

##
## NB: fix a "bug" in the default profile configuration of the metadata agent.
## This manifests under high load; the MDA calls to neutron via API if the
## internal RPC fails.  The profile wasn't setting this correctly; it's not
## in the install instructions.
##
project=`crudini --get /etc/neutron/metadata_agent.ini DEFAULT project_name`
username=`crudini --get /etc/neutron/metadata_agent.ini DEFAULT username`
password=`crudini --get /etc/neutron/metadata_agent.ini DEFAULT password`

crudini --set /etc/neutron/metadata_agent.ini DEFAULT \
    auth_url http://$CONTROLLER:35357/v2.0
crudini --set /etc/neutron/metadata_agent.ini DEFAULT admin_tenant_name $project
crudini --set /etc/neutron/metadata_agent.ini DEFAULT admin_user $username
crudini --set /etc/neutron/metadata_agent.ini DEFAULT admin_password $password

##
## Ok, restart Neutron Capnet ML2 plugin and other Neutron agents.
##
service_restart neutron-plugin-capnet-agent
service_enable neutron-plugin-capnet-agent

service_restart neutron-dhcp-agent
service_restart neutron-l3-agent
service_restart neutron-metadata-agent
service_restart neutron-metering-agent

service_restart neutron-plugin-openvswitch-agent
