#!/bin/sh

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/setup-lib.sh"

#
# Unpack our tarballs.
#
tar -xzvf $HADOOPTARBALL -C $HOMEDIR
cp -pR $HADOOPDIR/etc/hadoop $HADOOPDIR/etc/hadoop.bak
#rm -rf $HADOOPDIR/etc/hadoop
#tar -xzvf $RMCTARBALL -C $HADOOPDIR/etc/
#sudo rsync -av hadoop_resourcemanager_conf/hadoop/ /etc/hadoop/

# NB: just extract the generic config tarball now
tar -xzvf $GCTARBALL -C $HADOOPDIR/etc/

#
# Copy the slaves file into place
#
cp slaves.namefile $HADOOPDIR/etc/hadoop/slaves

mkdir -p $HADOOPDIR/logs
chmod 777 $HADOOPDIR/logs

sudo mkdir -p /mnt
sudo chown -R ubuntu /mnt
mkdir -p /mnt/hadoop
mkdir -p /mnt/datanode

exit 0
