#!/bin/sh

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/setup-lib.sh"

#
# Get the IPs of all our nodes and build parallel ssh hostfiles.
#
slaves=""
touch all.hostfile
touch slaves.namefile
touch slaves.hostfile
master="$1"
echo "$master" > master.hostfile
echo "$master" > all.hostfile
echo "master" > master.namefile
shift
resourcemanager="$1"
echo "$resourcemanager" > resourcemanager.hostfile
echo "$resourcemanager" >> all.hostfile
echo "resourcemanager" > resourcemanager.namefile
shift
# Slaves start with i=1 (slave-1)
i=1
while [ $# -gt 0 ]; do
    slaves="$slaves $1"
    echo "$1" >> all.hostfile
    echo "$1" >> slaves.hostfile
    echo "slave-${i}" >> slaves.namefile
    i=`expr $i + 1`
    shift
done

#
# Build an /etc/hosts file and append it to all VMs /etc/hosts.
#
echo "$master master-link-1 master-0 master" > etc.hosts
echo "$resourcemanager resourcemanager-link-1 resourcemanager-0 resourcemanager" \
    >> etc.hosts
i=1
for slave in $slaves ; do
    echo "${slave} slave-$i-link-1 slave-$i-0 slave-$i" >> etc.hosts
    i=`expr $i + 1`
done

#cat etc.hosts | sudo tee -a /etc/hosts
cat etc.hosts | $PSSH --send-input -h all.hostfile sudo tee -a /etc/hosts

#
# Setup an ssh config file with StrictHostKeyChecking no so that all
# the hadoop scripts don't get hung up on that.
#
touch $HOMEDIR/.ssh/config
chmod 600 $HOMEDIR/.ssh/config
chown ubuntu:ubuntu $HOMEDIR/.ssh/config
echo "StrictHostKeyChecking no" >> $HOMEDIR/.ssh/config
$PSCP -h all.hostfile $HOMEDIR/.ssh/config $HOMEDIR/.ssh/config

#
# parallel-scp the slaves file to all hosts.
#
$PSCP -h all.hostfile $HOMEDIR/slaves.namefile $HOMEDIR/slaves.namefile

#
# Setup the master.
#
$SSH $master $HOMEDIR/setup_master.sh

#
# Setup the resourcemanager.
#
$SSH $resourcemanager $HOMEDIR/setup_resourcemanager.sh

#
# Setup all the slaves.
#
$PSSH -h slaves.hostfile $HOMEDIR/setup_slave.sh

#
# Let a parallelized hadoop startup script actually start everything up
# on the master, resourcemanager, and slave nodes.
#
$HOMEDIR/hadoop-2.7.1/bin/hadoop namenode -format
$HOMEDIR/hadoop-2.7.1/sbin/start-dfs.sh

$HOMEDIR/hadoop-2.7.1/sbin/start-yarn.sh

exit 0
