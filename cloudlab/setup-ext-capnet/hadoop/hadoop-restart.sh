#!/bin/sh

set -x

HADOOPDIR=/home/ubuntu/hadoop-2.7.1

$HADOOPDIR/sbin/stop-yarn.sh
$HADOOPDIR/sbin/stop-dfs.sh
$HADOOPDIR/sbin/start-dfs.sh
$HADOOPDIR/sbin/start-yarn.sh

exit 0
