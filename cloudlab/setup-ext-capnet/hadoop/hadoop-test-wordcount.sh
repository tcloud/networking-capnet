#!/bin/sh

set -x

if [ $# -gt 0 ]; then
    SIZE=$1
else
    SIZE=`expr 1024 \* 1024 \* 10`
fi

HADOOPDIR=/home/ubuntu/hadoop-2.7.1
HDFS="$HADOOPDIR/bin/hdfs dfs"

$HDFS -mkdir /tests

$HDFS -ls /tests/words-input-$SIZE.txt
if [ ! $? -eq 0 ]; then
    # Create the input file
    rm -f words-input.txt
    touch words-input.txt
    cursize=`stat --printf "%s" words-input.txt`
    while [ $cursize -lt $SIZE ]; do
	cat words.txt >> words-input.txt
	cursize=`stat --printf "%s" words-input.txt`
    done

    # Upload into HDFS
    $HDFS -copyFromLocal words-input.txt /tests/words-input-$SIZE.txt
fi

OUTDIRNUM=0
$HDFS -ls /tests/output.$OUTDIRNUM
while [ $? -eq 0 ]; do
    OUTDIRNUM=`expr $OUTDIRNUM + 1`
    $HDFS -ls /tests/output.$OUTDIRNUM
done

$HADOOPDIR/bin/hadoop jar \
    $HADOOPDIR/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.1.jar \
    wordcount /tests/words-input-$SIZE.txt /tests/output.$OUTDIRNUM

exit 0
