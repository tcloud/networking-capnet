#!/bin/sh

set -x

arg1=30
arg2=30
if [ $# -gt 0 ]; then
    arg1=$1
fi
if [ $# -gt 1 ]; then
    arg2=$2
fi

HADOOPDIR=/home/ubuntu/hadoop-2.7.1
HDFS="$HADOOPDIR/bin/hdfs dfs"

$HADOOPDIR/bin/hadoop jar \
    $HADOOPDIR/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.1.jar \
    pi $arg1 $arg2

exit 0
