#!/bin/sh

PSCP='/usr/bin/parallel-scp -O StrictHostKeyChecking=no '
PSSH='/usr/bin/parallel-ssh -O StrictHostKeyChecking=no '
SSH='ssh -o StrictHostKeyChecking=no '

HOMEDIR=/home/ubuntu
TARBALLDIR="$HOMEDIR"
HADOOPTARBALL="${TARBALLDIR}/hadoop-2.7.1.tar.gz"
HADOOPDIR="$HOMEDIR/hadoop-2.7.1"
#MCTARBALL="${TARBALLDIR}/hadoop_master_conf.tar.gz"
#RMCTARBALL="${TARBALLDIR}/hadoop_resourcemanager_conf.tar.gz"
#SCTARBALL="${TARBALLDIR}/hadoop_slaves_conf.tar.gz"
GCTARBALL="${TARBALLDIR}/hadoop_generic_conf.tar.gz"
