#!/bin/sh

set -x

DIRNAME=`dirname $0`
. "$DIRNAME/setup-lib.sh"

#
# Unpack our tarballs.
#
tar -xzvf $HADOOPTARBALL -C $HOMEDIR
cp -pR $HADOOPDIR/etc/hadoop $HADOOPDIR/etc/hadoop.bak
#rm -rf $HADOOPDIR/etc/hadoop
#tar -xzvf $MCTARBALL -C $HADOOPDIR/etc/
#sudo rsync -av hadoop_master_conf/hadoop/ /etc/hadoop/

# NB: just extract the generic config tarball now
tar -xzvf $GCTARBALL -C $HADOOPDIR/etc/

#
# Copy the slaves file into place
#
cp $HOMEDIR/slaves.namefile $HADOOPDIR/etc/hadoop/slaves
# Don't make the master a slave (datanode) node.
#echo master >> $HADOOPDIR/etc/hadoop/slaves

# Note down the master
echo master > $HADOOPDIR/etc/hadoop/master

mkdir -p $HADOOPDIR/logs
chmod 777 $HADOOPDIR/logs

sudo mkdir -p /mnt
sudo chown -R ubuntu /mnt
mkdir -p /mnt/hadoop
mkdir -p /mnt/datanode

# Give hadoop more tmp space to work with.
sudo mount -o remount,size=2G /tmp

exit 0
