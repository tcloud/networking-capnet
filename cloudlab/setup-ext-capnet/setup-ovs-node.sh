#!/bin/sh

#
# This sets up openvswitch networks for Capnet networks.
# The networkmanager and compute nodes' physical interfaces
# have to get moved into br-capnet-lan-*.  The controller is special; it doesn't
# get an openvswitch setup.
#

set -x

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/../../setup-lib.sh"

#
# (Maybe) Setup the flat data networks
#
for lan in $DATACAPNETLANS ; do
    # suck in the vars we'll use to configure this one
    . $OURDIR/info.$lan

    DATABRIDGE="br-$lan"

    ovs-vsctl add-br ${DATABRIDGE}

    ovs-vsctl add-port ${DATABRIDGE} ${DATADEV}
    ifconfig ${DATADEV} 0 up
    cat <<EOF >> /etc/network/interfaces

auto ${DATABRIDGE}
iface ${DATABRIDGE} inet static
    address $DATAIP
    netmask $DATANETMASK

auto ${DATADEV}
iface ${DATADEV} inet static
    address 0.0.0.0
EOF
    if [ -n "$DATAVLANDEV" ]; then
	cat <<EOF >> /etc/network/interfaces
    vlan-raw-device ${DATAVLANDEV}
EOF
    fi

    ifconfig ${DATABRIDGE} $DATAIP netmask $DATANETMASK up
    # XXX!
    #route add -net 10.0.0.0/8 dev ${DATA_NETWORK_BRIDGE}
done

#service_restart openvswitch-switch

exit 0
