#!/bin/sh

##
## This script sets up Capnet networks, multiple tenants, drags in a
## hadoop image, wfagents for a hadoop demo, etc.  All Capnet networks
## are "shared" by default, meaning that any tenant can plug into them.
## Obviously, the tenants still have to exchange caps to have their
## nodes or wfagents talk.
##

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

CAPNET_AUTO_ALLPAIRS_WFA="0"

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

if [ "$HOSTNAME" != "$CONTROLLER" ]; then
    exit 0;
fi

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

. $OURDIR/admin-openrc.sh

#
# openstack CLI commands seem flakey sometimes on Kilo and Liberty.
# Don't know if it's WSGI, mysql dropping connections, an NTP
# thing... but until it gets solved more permanently, have to retry :(.
#
__openstack() {
    __err=1
    __debug=
    __times=0
    while [ $__times -lt 16 -a ! $__err -eq 0 ]; do
	openstack $__debug "$@"
	__err=$?
        if [ $__err -eq 0 ]; then
            break
        fi
	__debug=" --debug "
	__times=`expr $__times + 1`
	if [ $__times -gt 1 ]; then
	    echo "ERROR: openstack command failed: sleeping and trying again!"
	    sleep 8
	fi
    done
}

for lan in ${DATAOTHERLANS} ; do
    . $OURDIR/info.${lan}

    name="$lan"
    echo "*** Creating Capnet data network ${lan} and subnet ..."

    nmdataip=`cat $OURDIR/data-hosts.${lan} | grep ${NETWORKMANAGER} | sed -n -e 's/^\([0-9]*.[0-9]*.[0-9]*.[0-9]*\).*$/\1/p'`
    allocation_pool=`cat $OURDIR/data-allocation-pool.${lan}`
    cidr=`cat $OURDIR/data-cidr.${lan}`
    # Make sure to set the right gateway IP (to our router)
    routeripaddr=`cat $OURDIR/router-ipaddr.$lan`

    neutron net-show ${name}-net
    if [ ! $? -eq 0 ]; then
	neutron net-create ${name}-net --shared \
	    --provider:physical_network ${lan} --provider:network_type capnet
    fi
    netid=`neutron net-show ${name}-net | awk '/ id / {print $4}'`

    neutron subnet-show ${name}-subnet
    if [ ! $? -eq 0 ]; then
	neutron subnet-create ${name}-net --name ${name}-subnet \
	    --allocation-pool ${allocation_pool} --gateway $routeripaddr $cidr
    fi

    subnetid=`neutron subnet-show ${name}-subnet | awk '/ id / {print $4}'`

    neutron router-show ${name}-router
    if [ ! $? -eq 0 ]; then
	neutron router-create ${name}-router
    fi
    # See if it has a port; if so, don't add
    neutron router-port-list ${name}-router | grep -q subnet_id
    if [ ! $? -eq 0 ]; then
	neutron router-interface-add ${name}-router ${name}-subnet
    fi

    #if [ $PUBLICCOUNT -ge 3 ] ; then
    #    neutron router-gateway-set ${name}-router ext-net
    #fi

    # Fix up the dhcp agent port IP addr.  We can't set this in the
    # creation command, so do a port update!
    #ports=`neutron port-list | grep $subnetid | awk '{print $2}'`
    #for port in $ports ; do
    #    owner=`neutron port-show $port | awk '/ device_owner / {print $4}'`
	#if [ "x$owner" = "xnetwork:router_interface" -a -f $OURDIR/router-ipaddr.$lan ]; then
	#    newipaddr=`cat $OURDIR/router-ipaddr.$lan`
	#    neutron port-update $port --fixed-ip subnet_id=$subnetid,ip_address=$newipaddr
	#fi
    #    if [ "x$owner" = "xnetwork:dhcp" -a -f $OURDIR/dhcp-agent-ipaddr.$lan ]; then
    #        newipaddr=`cat $OURDIR/dhcp-agent-ipaddr.$lan`
    #        neutron port-update $port --fixed-ip subnet_id=$subnetid,ip_address=$newipaddr
    #    fi
    #done

    # Don't create a workflow agent -- probably in the future create a
    # default secgroup one, or something.
    if [ $CAPNET_AUTO_ALLPAIRS_WFA = "1" ]; then
	neutron capnet-wfagent-create --name wfa0 \
	    --master --wfapp_path=/usr/bin/capnet-wfagent-allpairs ${name}-net
    fi
done

##
## Create some tenants, and service tenants, and users in them (using the
## admin password).
##
if [ "x${ADMIN_PASS}" = "x" ]; then
    # Create the admin user -- temporarily use the random one for
    # ${ADMIN_API}; we change it right away below manually via sql
    APSWD="${ADMIN_API_PASS}"
else
    APSWD="${ADMIN_PASS}"
fi
DOMARG=""
#if [ $OSVERSION -gt $OSKILO ]; then
if [ "x$KEYSTONEAPIVERSION" = "x3" ]; then
    DOMARG="--domain default"
fi

i=0
while [ $i -lt 4 ]; do
    # Create the resource/compute tenant/user
    __openstack project list | grep " tenant-$i "
    if [ ! $? -eq 0 ]; then
	__openstack project create $DOMARG \
	    --description "tenant-$i Project" tenant-$i
    fi
    __openstack user list | grep " tenant-$i-user "
    if [ ! $? -eq 0 ]; then
	__openstack user create $DOMARG --password "${APSWD}" \
	    --email "${SWAPPER_EMAIL}" tenant-$i-user
        #__openstack role add --project admin --user admin admin
	__openstack role add --project tenant-$i --user tenant-$i-user user
    fi
    tid=`openstack project show tenant-$i | awk '/ id / { print $4}'`
    neutron quota-update --tenant-id $tid \
	--network -1 --subnet -1 --port -1 --floatingip -1 --router -1 \
	--security-group -1 --security-group-rule -1 --rbac-policy -1

    echo "export OS_PROJECT_DOMAIN_ID=default" > $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_USER_DOMAIN_ID=default" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_PROJECT_NAME=tenant-$i" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_TENANT_NAME=tenant-$i" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_USERNAME=tenant-$i-user" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_PASSWORD=${APSWD}" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_AUTH_URL=http://ctl:35357/v3" >> $OURDIR/tenant-$i-user-openrc.sh
    echo "export OS_IDENTITY_API_VERSION=3" >> $OURDIR/tenant-$i-user-openrc.sh

    # Create the service tenant/user
    __openstack project list | grep " service-$i "
    if [ ! $? -eq 0 ]; then
	__openstack project create $DOMARG \
	    --description "service-$i Project" service-$i
    fi
    __openstack user list | grep " service-$i-user "
    if [ ! $? -eq 0 ]; then
        __openstack user create $DOMARG --password "${APSWD}" \
	    --email "${SWAPPER_EMAIL}" service-$i-user
        #__openstack role add --project admin --user admin admin
	__openstack role add --project service-$i --user service-$i-user user
    fi
    tid=`openstack project show service-$i | awk '/ id / { print $4}'`
    neutron quota-update --tenant-id $tid \
	--network -1 --subnet -1 --port -1 --floatingip -1 --router -1 \
	--security-group -1 --security-group-rule -1 --rbac-policy -1

    echo "export OS_PROJECT_DOMAIN_ID=default" > $OURDIR/service-$i-user-openrc.sh
    echo "export OS_USER_DOMAIN_ID=default" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_PROJECT_NAME=service-$i" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_TENANT_NAME=service-$i" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_USERNAME=service-$i-user" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_PASSWORD=${APSWD}" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_AUTH_URL=http://ctl:35357/v3" >> $OURDIR/service-$i-user-openrc.sh
    echo "export OS_IDENTITY_API_VERSION=3" >> $OURDIR/service-$i-user-openrc.sh

    i=`expr $i + 1`
done

##
## Make an image with hadoop stuff preinstalled.  Assume that the
## trusty-server image made by the previous setup scripts is the
## multi-nic one, so undo all that stuff, then install our hadoop stuff.
##
glance image-list | grep ' hadoop '
if [ ! $? -eq 0 ]; then
    IMGFILE=$OURDIR/trusty-server-cloudimg-amd64-disk1.img

    lsmod nbd
    if [ ! $? -eq 0 ]; then
	modprobe nbd max_part=8
    fi
    qemu-nbd --connect=/dev/nbd0 $IMGFILE

    # Hadoop needs the full 2G -- expand the FS.
    resize2fs /dev/nbd0p1
    e2fsck -f /dev/nbd0p1

    MNT=$OURDIR/mnt
    mkdir -p $MNT

    mount /dev/nbd0p1 $MNT

    echo "*** Removing multi-nic stuff..."

    cat <<EOF > $MNT/etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
EOF

    sed -i -e 's/^start on (mounted MOUNTPOINT=\/$/start on (local-filesystems/' $MNT/etc/init/networking.conf

    rm -f $MNT/etc/network/if-pre-up.d/if-pre-up-upstart-faker

    rm -f $MNT/etc/resolv.conf
    cat <<EOF >$MNT/etc/resolv.conf
nameserver 8.8.8.8
EOF

    mount -o bind /proc $MNT/proc

    # Install hadoop and our config files
    chroot $MNT /usr/bin/apt-get update
    chroot $MNT /usr/bin/apt-get install -y openjdk-7-jdk pssh
    chroot $MNT /usr/bin/apt-get clean all
    
    # Download hadoop and our word file.
    wget -O hadoop-2.7.1.tar.gz \
	http://www.emulab.net/downloads/hadoop-2.7.1.tar.gz
    wget -O openstack-capnet-hadoop-words.txt.tar.gz \
	http://www.emulab.net/downloads/openstack-capnet-hadoop-words.txt.tar.gz

    # Let the VM extract the hadoop tarball once the disk has been expanded;
    # it's big.  But put the word file into place right away...
    cp hadoop-2.7.1.tar.gz $MNT/home/ubuntu
    tar -xzvf openstack-capnet-hadoop-words.txt.tar.gz -C $MNT/home/ubuntu

    # Copy the hadoop setup scripts from the src tarball into the the disk image
    rsync -av $DIRNAME/hadoop/*.sh $MNT/home/ubuntu/

    # Make our generic conf tarball from the src tree, and put it into the
    # image to be extracted later.
    cwd=`pwd`
    (cd $DIRNAME/hadoop/etc && tar -czvf $DIRNAME/hadoop_generic_conf.tar.gz ./* ; cd $cwd)
    cp $DIRNAME/hadoop_generic_conf.tar.gz $MNT/home/ubuntu

    mkdir -p $MNT/root/.ssh
    chmod 700 $MNT/root/.ssh
    touch $MNT/root/.ssh/authorized_keys
    chmod 600 $MNT/root/.ssh/authorized_keys
    cat /root/.ssh/id_rsa.pub >> $MNT/root/.ssh/authorized_keys
    cp -p /root/.ssh/id_rsa $MNT/root/.ssh/id_rsa

    mkdir -p $MNT/home/ubuntu/.ssh
    chmod 700 $MNT/home/ubuntu/.ssh
    touch $MNT/home/ubuntu/.ssh/authorized_keys
    chmod 600 $MNT/home/ubuntu/.ssh/authorized_keys
    cat /root/.ssh/id_rsa.pub >> $MNT/home/ubuntu/.ssh/authorized_keys
    cp -p /root/.ssh/id_rsa $MNT/home/ubuntu/.ssh/id_rsa

    # Make sure ubuntu user's homedir and .ssh is sane.  Do everything cause
    # we just copied in a bunch of stuff as root.
    chown 1000:1000 -R $MNT/home/ubuntu

    rm -f $MNT/etc/resolv.conf
    ln -s ../run/resolvconf/resolv.conf $MNT/etc/resolv.conf

    umount $MNT/proc

    umount $MNT

    sync
    sync

    echo "*** Importing new hadoop image ..."

    glance image-create --name hadoop --disk-format qcow2 --container-format bare --progress --file $IMGFILE

    qemu-nbd -d /dev/nbd0
fi

##
## Set all the images public so other tenants can boot them.
##
IMAGELIST=`glance image-list | awk ' / [0-9a-zA-Z]*-[0-9a-zA-Z-]* / { print $2 }' | xargs`
for image in $IMAGELIST ; do
    glance image-update --visibility public $image
done

exit 0
