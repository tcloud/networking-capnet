#!/bin/sh

##
## This script builds and installs the necessary deps required for the
## Capnet controller, as well as the Capnet Neutron Plugin, and
## configures it all based on the Cloudlab Openstack Profile
## (https://www.cloudlab.us/p/capnet/OpenStack-Capnet).
##

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

if [ "$HOSTNAME" != "$CONTROLLER" ]; then
    exit 0;
fi

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

maybe_install_packages pssh
PSSH='/usr/bin/parallel-ssh -t 0 -O StrictHostKeyChecking=no '

##
## First, we install Capnet at all of the nodes.
##
#$DIRNAME/setup-capnet-install.sh
#for node in $COMPUTENODES $NETWORKMANAGER ; do
#    fqdn=`getfqdn $node`
#    $SSH $fqdn $DIRNAME/setup-capnet-install.sh
#done
mkdir -p capnet-logs/stdout capnet-logs/stderr
HOSTS=""
fqdn=`getfqdn $CONTROLLER`
HOSTS="$HOSTS -H $fqdn"
fqdn=`getfqdn $NETWORKMANAGER`
HOSTS="$HOSTS -H $fqdn"
#HOSTS="-H $CONTROLLER -H $NETWORKMANAGER "
CPHOSTS=""
for node in $COMPUTENODES ; do
    fqdn=`getfqdn $node`
    CPHOSTS="$CPHOSTS -H $fqdn"
done

$PSSH $HOSTS $CPHOSTS -o capnet-logs/stdout -e capnet-logs/stderr $DIRNAME/setup-capnet-install.sh

##
## Setup the controller node:
##
$DIRNAME/setup-capnet-controller.sh

## Setup the networkmanager node:
fqdn=`getfqdn $NETWORKMANAGER`
$SSH $fqdn $DIRNAME/setup-capnet-networkmanager.sh

#for node in $COMPUTENODES ; do
#    fqdn=`getfqdn $node`
#    $SSH $fqdn $DIRNAME/setup-capnet-compute.sh
#done
$PSSH $CPHOSTS -o capnet-logs/stdout -e capnet-logs/stderr $DIRNAME/setup-capnet-compute.sh

##
## Finally, we create initial networks!
##
$DIRNAME/setup-capnet-basic.sh

echo "Done with Capnet Setup!"

exit 0
