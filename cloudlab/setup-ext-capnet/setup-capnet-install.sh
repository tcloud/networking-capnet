#!/bin/sh

##
## This script builds and installs the necessary deps required for the
## Capnet controller, as well as the Capnet Neutron Plugin, and
## configures it all based on the Cloudlab Openstack Profile
## (https://www.cloudlab.us/p/capnet/OpenStack-Capnet).
##

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

LIBCAP_REPO="https://gitlab.flux.utah.edu/xcap/libcap.git"
LIBCAP_BRANCH="master"
OPENMUL_REPO="https://gitlab.flux.utah.edu/tcloud/openmul.git"
OPENMUL_BRANCH="capnet"
CAPNET_REPO="https://gitlab.flux.utah.edu/tcloud/capnet.git"
CAPNET_BRANCH="master"
CAPNET_PLUGIN_REPO="https://gitlab.flux.utah.edu/tcloud/networking-capnet.git"
CAPNET_PLUGIN_BRANCH="master"

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

#if [ "$HOSTNAME" != "$CONTROLLER" ]; then
#    exit 0;
#fi

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

#
# openstack CLI commands seem flakey sometimes on Kilo and Liberty.
# Don't know if it's WSGI, mysql dropping connections, an NTP
# thing... but until it gets solved more permanently, have to retry :(.
#
__openstack() {
    __err=1
    __debug=
    __times=0
    while [ $__times -lt 16 -a ! $__err -eq 0 ]; do
	openstack $__debug "$@"
	__err=$?
        if [ $__err -eq 0 ]; then
            break
        fi
	__debug=" --debug "
	__times=`expr $__times + 1`
	if [ $__times -gt 1 ]; then
	    echo "ERROR: openstack command failed: sleeping and trying again!"
	    sleep 8
	fi
    done
}

##
## First, we install Capnet at all of the nodes.
##
cd $OURDIR
mkdir capnet
cd capnet

maybe_install_packages git
maybe_install_packages pkg-config glib2.0-dev
maybe_install_packages automake1.11 swig2.0 python2.7-dev gawk libevent-dev libcurl3-dev
maybe_install_packages python-protobuf
#maybe_install_packages protobuf-c-compiler libprotobuf-c-dev
maybe_install_packages libcap-dev

# Ubuntu protobuf-c is too old; install all that from src
# First grab protobuf itself:
wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.gz
tar -xzvf protobuf-2.6.1.tar.gz
cd protobuf-2.6.1
./configure --prefix=/usr/local
make && make install
ldconfig
cd ..

# Now protobuf-c
git clone https://github.com/protobuf-c/protobuf-c.git protobuf-c
cd protobuf-c && git checkout v1.2.1 && ./autogen.sh && cd ..
mkdir protobuf-c.obj && cd protobuf-c.obj
../protobuf-c/configure --prefix=/usr/local
make && make install
ldconfig
cd ..

#
# First, libcap.
#
git clone "$LIBCAP_REPO" libcap
cd libcap
git checkout "$LIBCAP_BRANCH"
./autogen.sh
cd ..
mkdir libcap.obj
cd libcap.obj
../libcap/configure --prefix=/opt/tcloud/libcap
make
make install
cd ..

#
# Second, our version of openmul.
#
git clone "$OPENMUL_REPO" openmul
cd openmul
git checkout "$OPENMUL_BRANCH"
./autogen.sh
cd ..
mkdir openmul.obj
cd openmul.obj
../openmul/configure --prefix=/opt/tcloud/mul
make
make install
cd ..

#
# Third, capnet controller.
#
git clone "$CAPNET_REPO" capnet
cd capnet
git checkout "$CAPNET_BRANCH"
./autogen.sh
cd ..
mkdir capnet.obj
cd capnet.obj
../capnet/configure --prefix=/opt/tcloud/capnet \
    --with-libcap=/opt/tcloud/libcap --with-mul=/opt/tcloud/mul \
    --with-protoc=/usr/local
make && make install
cd ..

#
# Finally, capnet Neutron plugin stuff.
#
echo "$CAPNET_PLUGIN_REPO" | grep -q tar\.gz
if [ $? = 0 ]; then
    wget -O networking-capnet.tar.gz "$CAPNET_PLUGIN_REPO"
    tar -xzf networking-capnet.tar.gz
else
    git clone "$CAPNET_PLUGIN_REPO" networking-capnet
fi
cd networking-capnet
git checkout "$CAPNET_PLUGIN_BRANCH"
rm -rf build networking_capnet.egg-info
# Install the Ubuntu way, and straight into dist-packages (i.e. /).
# Otherwise it goes into site-packages and Neutron can't find us.
python setup.py install --install-layout=deb --install-data / -v -f
cd ..

##
## 
##
