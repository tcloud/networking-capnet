#!/bin/sh

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "$DIRNAME/../../setup-lib.sh"

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

##
## First, we setup OVS stuff for Capnet physical networks.
##
$DIRNAME/setup-ovs-node.sh

##
## Second, however, we do all the Neutron config Capnet needs:
##
NMMGMTIP=`cat /etc/hosts | grep -E " $NETWORKMANAGER$" | head -1 | sed -n -e 's/^\\([0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\).*$/\\1/p'`

ML2TYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 type_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 type_drivers "capnet,$ML2TYPES"
ML2TENANTTYPES=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 tenant_network_types`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 tenant_network_types "capnet,$ML2TENANTTYPES"
ML2MECHDRVS=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 mechanism_drivers`
crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
    ml2 mechanism_drivers "capnet,$ML2MECHDRVS"

ML2EXT=`crudini --get /etc/neutron/plugins/ml2/ml2_conf.ini ml2 extension_drivers`
if [ ! -z "$ML2EXT" ] ; then
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet,$ML2EXT"
else
    crudini --set /etc/neutron/plugins/ml2/ml2_conf.ini \
	ml2 extension_drivers "capnet"
fi

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini DEFAULT interface_driver \
    networking_capnet.agent.linux.interface.CapnetOVSInterfaceDriver

crudini --set /etc/neutron/neutron.conf DEFAULT api_extensions_path \
    /usr/lib/python2.7/dist-packages/networking_capnet/extensions

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini capnet pythonpath \
    /opt/tcloud/capnet/lib/python2.7/site-packages

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini capnet \
    neutron_metadata_service_location l3

##
## Setup our physical Capnet connections
##
capnet_networks=""
bridge_mappings=""
for lan in $DATACAPNETLANS ; do
    if [ -n "${capnet_networks}" ]; then
	capnet_networks="${capnet_networks},"
    fi
    if [ -n "${bridge_mappings}" ]; then
	bridge_mappings="${bridge_mappings},"
    fi
    . $OURDIR/info.${lan}
    capnet_networks="${capnet_networks}${lan}"
    bridge_mappings="${bridge_mappings}${lan}:${DATABRIDGE}"
done

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet bridge_mappings "$bridge_mappings"
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet capnet_networks "$capnet_networks"

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    capnet master_controller_ip $NMMGMTIP

crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    DEFAULT verbose True
crudini --set /etc/neutron/plugins/ml2/ml2_conf_capnet.ini \
    DEFAULT debug True
crudini --set /etc/neutron/neutron.conf DEFAULT verbose True
crudini --set /etc/neutron/neutron.conf DEFAULT debug True

crudini --set /etc/nova/nova-compute.conf DEFAULT compute_driver \
    compute_capnet.virt.libvirt.driver.CapnetLibvirtDriver
crudini --set /etc/nova/nova-compute.conf libvirt vif_driver \
    compute_capnet.virt.libvirt.vif.CapnetLibvirtVIFDriver

crudini --set /etc/nova/nova-compute.conf \
    capnet bridge_mappings "$bridge_mappings"
crudini --set /etc/nova/nova-compute.conf \
    capnet capnet_networks "$capnet_networks"

crudini --set /etc/nova/nova.conf \
    DEFAULT network_api_class compute_capnet.network.neutronv2.api.API

##
## Ok, restart Neutron Capnet ML2 plugin 
##
service_restart neutron-plugin-capnet-agent
service_enable neutron-plugin-capnet-agent

# Also restart nova-compute to pick up our vif driver.
service_restart nova-compute
