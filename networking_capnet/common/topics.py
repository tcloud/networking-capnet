
#
# These are our extended Capnet resources
#
WORKFLOW_AGENT = 'wfagent'
PORT_BINDING = 'portbinding'

#
# These are our extended Capnet verbs
#
RESTART = 'restart'

#
# This is the message queue that the agents use to send messages to the
# Neutron server.
#
PLUGIN_CAPNET = 'q-plugin-capnet'

#
# This is the message queue that the Neutron server uses to send messages to
# the Capnet agents' extended RPC interfaces.
#
AGENT_CAPNET = 'q-agent-notifier-capnet'
