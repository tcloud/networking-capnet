
from oslo_config import cfg
from oslo_log import log as logging
from keystoneclient.i18n import _
from keystoneclient.auth.identity import v2 as v2_auth
from keystoneclient.auth.identity import v3 as v3_auth
from keystoneclient import discover
from keystoneclient.openstack.common.apiclient import exceptions as ks_exc
from keystoneclient import session
from neutronclient.v2_0 import client
from neutronclient.common import clientmanager


LOG = logging.getLogger(__name__)


keystone_opts = [
    cfg.StrOpt('auth_strategy', default='keystone',
               help=_("The type of authentication to use")),
]

keystone_auth_opts = [
    cfg.StrOpt('admin_token',
               help=_("Admin token")),
    cfg.StrOpt('url',
               help=_("Admin Token URL")),

    cfg.StrOpt('admin_user',
               help=_("Admin user")),
    cfg.StrOpt('username',
               help=_("Username")),
    cfg.StrOpt('admin_password',
               help=_("Admin password"),
               secret=True),
    cfg.StrOpt('password',
               help=_("Password"),
               secret=True),
    cfg.StrOpt('admin_tenant_name',
               help=_("Admin tenant name")),
    cfg.StrOpt('project_name',
               help=_("Project/tenant name")),

    cfg.StrOpt('project_domain_id',
               help=_("Project/tenant domain")),
    cfg.StrOpt('user_domain_id',
               help=_("Project/tenant domain")),
    cfg.StrOpt('project_domain_name',
               help=_("Project/tenant domain")),
    cfg.StrOpt('user_domain_name',
               help=_("Project/tenant domain")),

    cfg.StrOpt('auth_plugin',
               help=_("Authentication plugin")),

    cfg.StrOpt('auth_version',
               help=_("Authentication Identify API version")),
    cfg.StrOpt('identity_uri',
               help=_("Identity URI (admin)")),
    cfg.StrOpt('auth_uri',
               help=_("Authentication URI (public)")),
    cfg.StrOpt('auth_url',
               help=_("Authentication URL")),

    cfg.StrOpt('region_name',
               help=_("Authentication region")),
    #cfg.StrOpt('auth_region',
    #           help=_("Authentication region")),

    cfg.BoolOpt('insecure',
                default=False,
                help=_("Turn off verification of the certificate for ssl")),
    cfg.StrOpt('auth_ca_cert',
               help=_("Certificate Authority public key (CA cert) "
                      "file for ssl")),
    cfg.IntOpt('http_connect_timeout',
               help=_("Request timeout when connecting to Identity API server")),
    cfg.IntOpt('http_request_max_retries',
               default=3,
               help=_("How many times we try reconnect to Identity API server")),
]
    
def register_opts_for_neutron_conf(conf=None):
    if not conf:
        conf = cfg.CONF
        pass
    
    for opt in keystone_auth_opts:
        try:
            conf.register_opt(opt,group='keystone_authtoken')
        except:
            pass
        pass
    #conf.register_opts(keystone_auth_opts,"keystone_authtoken")
    for opt in keystone_opts:
        try:
            conf.register_opt(opt,group="DEFAULT")
        except:
            pass
        pass
    #conf.register_opts(keystone_opts,"DEFAULT")
    pass

def register_opts_for_nova_conf(conf=None):
    if not conf:
        conf = cfg.CONF
        pass
    
    for opt in keystone_auth_opts:
        try:
            conf.register_opt(opt,group='neutron')
        except:
            pass
        pass
    #if not 'neutron' in conf:
    #    conf.register_opts(keystone_auth_opts,"neutron")
    for opt in keystone_opts:
        try:
            conf.register_opt(opt,group="DEFAULT")
        except:
            pass
        pass
    #if not 'DEFAULT' in conf:
    #    conf.register_opts(keystone_opts,"DEFAULT")
    pass

def get_client_for_neutron_conf(conf=None):
    if not conf:
        conf = cfg.CONF
        pass
    return CapnetNeutronClient(auth_section=conf.keystone_authtoken,
                               keystone_section=conf.DEFAULT)

def get_client_for_nova_conf(conf=None):
    if not conf:
        conf = cfg.CONF
        pass
    return CapnetNeutronClient(auth_section=conf.neutron,
                               keystone_section=conf.DEFAULT)

class CapnetNeutronClient(object):
    """
    A simple Neutron client wrapper that authenticates through keystone
    based on canonical options supplied in a config file.  We use this
    primarily in our Capnet Neutron and Nova interface drivers because
    they have to call back to Neutron to figure out which OVS bridge to
    use for the interface they're trying to plug.
    
    We expect a "auth_strategy" variable in one section (defaults to
    'keystone'); and then a whole bunch of keystone authentication
    parameters, potentially in another section.
    """
    
    def __init__(self,auth_section=None,keystone_section=None,**kwargs):
        self.opts = {}
        self._client_manager = None
        
        self.opts['auth_strategy'] = 'keystone'
        
        # Populate the opts with our defaults first
        for opt in keystone_opts:
            self.opts[opt.name] = opt.default
        for opt in keystone_auth_opts:
            self.opts[opt.name] = opt.default
        
        # Ok, if they gave config sections, grab stuff from there
        if keystone_section is not None:
            for (opt,value) in keystone_section.iteritems():
                self.opts[opt] = value
                pass
            pass
        if auth_section is not None:
            for (opt,value) in auth_section.iteritems():
                self.opts[opt] = value
                pass
            pass
        
        # Ok, snarf any remaining kwargs into our opts dict
        for (k,v) in kwargs.iteritems():
            self.opts[k] = v
            pass
        
        LOG.info("opts: %s" % (str(self.opts)))
        
        pass
    
    def get_client(self):
        return self.get_client_manager().neutron
    
    def get_v2_auth(self, v2_auth_url):
        return v2_auth.Password(
            v2_auth_url,
            username=self.opts['username'] \
                or self.opts['admin_user'],
            password=self.opts['password'] \
                or self.opts['admin_password'],
            tenant_name=self.opts['project_name'] \
                or self.opts['admin_tenant_name'])

    def get_v3_auth(self, v3_auth_url):
        project_name = (self.opts['project_name'] or
                        self.opts['admin_tenant_name'])

        return v3_auth.Password(
            v3_auth_url,
            username=self.opts['username'],
            password=self.opts['password'],
            user_domain_name=self.opts['user_domain_name'],
            user_domain_id=self.opts['user_domain_id'],
            project_name=project_name,
            project_domain_name=self.opts['project_domain_name'],
            project_domain_id=self.opts['project_domain_id']
        )

    def _discover_auth_versions(self, session, auth_url):
        # discover the API versions the server is supporting base on the
        # given URL
        try:
            ks_discover = discover.Discover(session=session, auth_url=auth_url)
            return (ks_discover.url_for('2.0'), ks_discover.url_for('3.0'))
        except ks_exc.ClientException:
            # Identity service may not support discover API version.
            # Lets try to figure out the API version from the original URL.
            url_parts = urlparse.urlparse(auth_url)
            (scheme, netloc, path, params, query, fragment) = url_parts
            path = path.lower()
            if path.startswith('/v3'):
                return (None, auth_url)
            elif path.startswith('/v2'):
                return (auth_url, None)
            else:
                # not enough information to determine the auth version
                msg = _('Unable to determine the Keystone version '
                        'to authenticate with using the given '
                        'auth_url. Identity service may not support API '
                        'version discovery. Please provide a versioned '
                        'auth_url instead.')
                raise Exception(msg)
        
    def get_client_manager(self):
        if self._client_manager is not None:
            return self._client_manager
        
        if self.opts['auth_strategy'] == 'keystone':
            if self.opts['admin_token']: #or self.opts['url']:
                # Token flow auth takes priority
                #if not self.opts['admin_token']:
                #    raise Exception(_("You must provide a token when"
                #                      " providing a service URL"))
                if not self.opts['url']:
                    raise Exception(_("You must provide a service URL"
                                      " when providing a token"))
            else:
                # Validate password flow auth
                project_info = (self.opts['admin_tenant_name'] or
                                (self.opts['project_name'] and
                                    (self.opts['project_domain_name'] or
                                     self.opts['project_domain_id'])) or
                                self.opts['project_id'])

                if not self.opts['username'] and not self.opts['admin_user']:
                    raise Exception(_("You must provide a username or user ID"))

                if not self.opts['password'] and not self.opts['admin_password']:
                    raise Exception(_("You must provide a password"))

                if not project_info:
                    # tenent is deprecated in Keystone v3. Use the latest
                    # terminology instead.
                    raise Exception(
                        _("You must provide a project_id or project_name"
                          " (with project_domain_name or project_domain_id)"))

                if not self.opts['auth_url']:
                    raise Exception(_("You must provide an auth url"))

            # first create a Keystone session
            #cacert = self.conf.cacert or None
            #cert = self.conf.cert or None
            #key = self.conf.key or None
            #insecure = self.conf.insecure or False
            #sdict = dict(cacert=cacert,cert=cert,key=key,insecure=insecure)
            ks_session = session.Session.construct({})
            # discover the supported keystone versions using the given url
            (v2_auth_url, v3_auth_url) = self._discover_auth_versions(
                session=ks_session,auth_url=self.opts['auth_url'])

            # Determine which authentication plugin to use. First inspect the
            # auth_url to see the supported version. If both v3 and v2 are
            # supported, then use the highest version if possible.
            user_domain_name = self.opts['user_domain_name'] or None
            user_domain_id = self.opts['user_domain_id'] or None
            project_domain_name = self.opts['project_domain_name'] or None
            project_domain_id = self.opts['project_domain_id'] or None
            domain_info = (user_domain_name or user_domain_id or
                           project_domain_name or project_domain_id)

            if (v2_auth_url and not domain_info) or not v3_auth_url:
                ks_session.auth = self.get_v2_auth(v2_auth_url)
            else:
                ks_session.auth = self.get_v3_auth(v3_auth_url)
                
            auth_session = ks_session
            auth = auth_session.auth
        else:   # not keystone
            if not self.opts['url']:
                raise Exception(_("You must provide a service URL"))
            auth_session = None
            auth = None
        
        self.client_manager = clientmanager.ClientManager(
            token=self.opts['admin_token'],
            #url=self.opts['url'],
            auth_url=self.opts['auth_url'],
            tenant_name=self.opts['admin_tenant_name'] or self.opts['project_name'],
            username=self.opts['username'] or self.opts['admin_user'],
            password=self.opts['password'] or self.opts['admin_password'],
            region_name=self.opts['region_name'],
            api_version={'network':'2.0'},
            auth_strategy=self.opts['auth_strategy'],
            service_type='network',
            insecure=self.opts['insecure'],
            #ca_cert=self.conf.cacert,
            timeout=self.opts['http_connect_timeout'],
            retries=self.opts['http_request_max_retries'],
            raise_errors=False,
            session=auth_session,
            auth=auth,
            log_credentials=True)
        
        self.client_manager.initialize()
        
        return self.client_manager
    pass
