
from oslo_config import cfg
from oslo_log import log as logging

#from neutron.agent.common import ovs_lib

from networking_capnet.common import config as cn_config
#from networking_capnet.common import capnetneutronclient as cneutronclient

LOG = logging.getLogger(__name__)

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')

#def use_in_neutron_context():
#    cneutronclient.register_opts_for_neutron_conf(cfg.CONF)
#    pass

#def use_in_nova_context():
#    cneutronclient.register_opts_for_nova_conf(cfg.CONF)
#    pass

class CapnetInterfaceMixin(object):
    """
    Driver for creating an internal interface on an OVS bridge.
    
    If this is a capnet network, we figure out which OVS bridge to plug
    the port into, instead of assuming the integration bridge.  We do
    this by contacting the neutron server and querying the network
    
    Backwards-compat with the OVSInterfaceDriver... it's just an overlay.
    """

    def __init__(self):
        self.bridge_mappings = {}
        # network id to OVS bridge name
        self.network_id_to_br = {}
        # network id to port id list
        self.network_ports = {}
        # device_name to bridge name
        self.device_bridges = {}
        # A CapnetNeutronClient
        self._client = None
        pass
    
    def set_client(self,client):
        self._client = client
        
        try:
            self.bridge_mappings = {}
            for mapping in cfg.CONF.CAPNET.bridge_mappings:
                sma = mapping.split(':')
                self.bridge_mappings[sma[0]] = sma[1]
                pass
            LOG.info("Initialized Capnet bridge_mappings: %s"
                     % (str(self.bridge_mappings)))
            
            client = self._client.get_client()
            networks = client.list_networks()
            LOG.info("Current networks: %s" % (str(networks)))
            
            if networks and networks.has_key('networks') \
              and len(networks['networks']) > 0:
                #dev_prefix = n_interface.OVSInterfaceDriver.DEV_NAME_PREFIX
                # Go through all our physical bridges, finding
                # interfaces named with the superclass's prefix, and add
                # all that junk to our cache.
                for network in networks['networks']:
                    if network.has_key('provider:physical_network') \
                      and self.bridge_mappings.has_key(network['provider:physical_network']):
                        network_id = network['id']
                        physical_network = network['provider:physical_network']
                        bridge_name = self.bridge_mappings[physical_network]
                        self.network_id_to_br[network_id] = bridge_name
                        #br = ovs_lib.OVSBridge(bridge_name)
                        #ports = br.get_vif_ports()
                        #for port in ports:
                        #    if port.port_name.startswith(dev_prefix):
                        #        self._cache_capnet_network_port(
                        #            network_id,port.vif_id,port.port_name,
                        #            bridge_name)
                        #        pass
                        #    pass
                        pass
                    pass
                pass
            LOG.info("Capnet state: network_id_to_br = %s"
                     % (str(self.network_id_to_br)))
            LOG.info("Capnet state: network_ports = %s"
                     % (str(self.network_ports)))
            LOG.info("Capnet state: device_bridges = %s"
                     % (str(self.device_bridges)))
            pass
        except Exception:
            LOG.exception("Failed to initialize Capnet Neutron Interface Driver;"
                          " will effectively revert to openvswitch driver")
            self.bridge_mappings = {}
            self.network_id_to_br = {}
            self.network_ports = {}
            self.device_bridges = {}
            self._client = None
            pass
        
        pass

    def _check_network_for_capnet(self, network_id):
        if len(self.bridge_mappings) == 0:
            LOG.debug("Could not check network_id %s to see if it is Capnet;"
                      " no bridge_mappings specified in configuration!"
                      % (network_id,))
            return None
        elif network_id in self.network_id_to_br:
            return self.network_id_to_br[network_id]
        
        try:
            client = self._client.get_client()
            resp = client.show_network(network_id)
            if resp and resp.has_key('network') \
              and resp['network'].has_key('provider:physical_network'):
                physical_network = resp['network']['provider:physical_network']
                
                if self.bridge_mappings.has_key(physical_network):
                    # Ok, this is a Capnet network...
                    bridge_name = self.bridge_mappings[physical_network]
                    return bridge_name
                pass
            pass
        except Exception:
            LOG.exception("Could not check network_id %s to see if it is Capnet"
                          % (network_id,))
            return None
            pass
        
        # Didn't find anything...
        return None
    
    def _cache_capnet_network_port(self, network_id, port_id, device_name,
                                   bridge_name):
        # ...so enable our cache
        self.network_id_to_br[network_id] = bridge_name
        if not self.network_ports.has_key(network_id):
            self.network_ports[network_id] = [port_id]
        else:
            self.network_ports[network_id].append(port_id)
        self.device_bridges[device_name] = (bridge_name,network_id,port_id)
        
        LOG.debug("cached Capnet device %s on bridge %s (net %s, port %s)"
                  % (device_name,bridge_name,network_id,port_id))
        pass
    
    def _capnet_lookup_device(self, device_name):
        if self.device_bridges.has_key(device_name):
            # Clean out the cache as much as possible.
            # If we have no more ports for a given network, we bump it
            # entirely out of our cache.
            (bridge_name,network_id,port_id) = self.device_bridges[device_name]
        else:
            (bridge_name,network_id,port_id) = (None,None,None)
            pass
        
    
    def _uncache_capnet_network_port(self, device_name):
        if bridge_name and network_id and port_id:
            del self.device_bridges[device_name]
            self.network_ports[network_id].remove(port_id)
            if len(self.network_ports[network_id]) == 0:
                del self.network_ports[network_id]
                del self.network_id_to_br[network_id]
                pass
            pass
