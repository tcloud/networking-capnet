
from oslo_config import cfg

#
# Each capnet virtual network has a mode -- learning or enforcing; a workflow
# app (or else the default); the workflow app's env (container or node);
# isolated or open (i.e., whether networks can contact each other or not); plus
# many of the default options below (arp, dhcp, metadata, workflow app/env).
#
# To support an openstack shared network, we will start a master workflow app,
# whose sole purpose is to delegate node privs to a per-tenant workflow app that
# the user specifies when it creates VMs on the shared network.  Nodes can then
# interact if they can negotiate the appropriate caps.
#

capnet_opts = [
    cfg.ListOpt('bridge_mappings',default=[],
                help="List of <physical_network>:<bridge>."),
    cfg.ListOpt('capnet_networks',default=[],
                help="List of physical_network names with which capnet "
                       "networks can be created.  Each physical network name "
                       "may be postfixed with a [:MAX_VIRT_NETWORKS] spec "
                       "to limit the amount of virtual networks created on "
                       "each physical network."),
    
    cfg.BoolOpt('hosts_controllers',default=False,
                help="If True, this host will create a local Capnet OpenFlow"
                     " controller for physical Capnet network (OVS bridge). "
                     " (Currently, only one physical host can have this set. "
                     " Futhermore, that physical host should be the one that"
                     " has hosts_workflow_apps (below) set to True, for"
                     " efficiency."),
    cfg.StrOpt('master_controller_ip',default='',
               help="Set this to the IP address that the master Capnet OpenFlow"
                    " controller is listening for switch connections on. "
                    " If this node is the master controller, this value should"
                    " be set to its management network IP.  If this node is not"
                    " the master controller, this should point to the master"
                    " controller host's IP."),
    cfg.BoolOpt('multi_controller_mode',default=False,
                help="This should only be set on the physical host that hosts"
                     " the master controller (i.e., has master_controller_ip"
                     " set to one of its local IPs).  Also, it is currently"
                     " unsupported!"),

    cfg.BoolOpt('hosts_workflow_apps', default=False,
                help="Set True if this host should run workflow apps."
                     "  For now, only one physical host should have this set."),

    cfg.BoolOpt('use_namespaces', default=True,
                help="Set True if we should isolate workflow apps in namespaces."
                     " This is currently the only supported value."),
    
    #cfg.IPOpt('of_listen_address', default='127.0.0.1',
    #          help="Address to listen on for OpenFlow connections. "),
    #cfg.IntOpt('of_listen_port', default=6633,
    #           help="Port to listen on for OpenFlow connections. "),
    #cfg.IntOpt('of_connect_timeout',default=30,
    #           help="Timeout in seconds to wait for "
    #                  "the local switch connecting the controller. "),
    #cfg.IntOpt('of_request_timeout',default=10,
    #           help="Timeout (seconds) to wait for each OpenFlow request."),

    cfg.BoolOpt('default_arp_responder', default=True,
                help="Enable local ARP responder."),
    cfg.BoolOpt('default_dhcp_flows',default=True,
                help="Enable automatic DHCP flows between router and VMs."),
    cfg.BoolOpt('default_metadata_flows',default=True,
                help="Enable automatic metadata flows between router and VMs."),
    cfg.StrOpt('wfapp_launcher',
               default="/usr/bin/capnet-wfagent-launcher",
               help="Specify the default workflow application launcher.  This must be in capnet.filters, and point to a valid application.  It is run as root because it needs to open a raw socket in the wfagent's netns."),
    cfg.StrOpt('default_wfapp_path',
               default="/usr/bin/capnet-wfagent-allpairs",
               help="Specify the default workflow application."),
    cfg.StrOpt('default_wfapp_env',default='netns',
               help="Specify whether the WA is in a 'netns' or on a 'node'."),
    
    cfg.ListOpt('pythonpath',
                default="/opt/tcloud/capnet/lib/python2.7/site-packages",
                help="Specify the path where the Capnet controller client python bindings are installed (and any other paths you need, as a comma-separated list)"),
    
    cfg.StrOpt('mul_core_path',default="mul",
               help="Specify the filter name of the path to the MUL core binary.  This is a rootwrap-based command, so you add a new command by adding a new filter in /etc/neutron/rootwrap.d/capnet.filters.  In that file, you'll see a 'mul' filter; add another one just like it, but that points to the binary you want to execute instead.  Give it a custom name that's not already in the file, and set this value to that name.  BUT -- NB -- if you specify a full path, rootwrap will not be used to run this command, and your binary will run with regular neutron privileges!  That is probably not what you want."),
    cfg.StrOpt('mul_core_args',default="-d -l 0 -n",
               help="Specify extra args to the MUL core binary."),
    cfg.IPOpt('mul_core_listen_address',default='127.0.0.1',
              help="MUL core address to listen on for OpenFlow connections. "),
    cfg.IntOpt('mul_core_base_port',default=6633,
               help="MUL core base port (for incoming OpenFlow connections)."),
    cfg.StrOpt('mul_cli_path',default="mulcli",
               help="Specify the filter name of the path to the MUL CLI binary.  This is a rootwrap-based command, so you add a new command by adding a new filter in /etc/neutron/rootwrap.d/capnet.filters.  In that file, you'll see a 'mulcli' filter; add another one just like it, but that points to the binary you want to execute instead.  Give it a custom name that's not already in the file, and set this value to that name.  BUT -- NB -- if you specify a full path, rootwrap will not be used to run this command, and your binary will run with regular neutron privileges!  That is probably not what you want."),
    cfg.StrOpt('mul_cli_args',default="-d",
               help="Specify extra args to the MUL CLI binary."),
    cfg.IntOpt('mul_cli_base_port',default=6933,
               help="MUL CLI base port."),
    cfg.StrOpt('capnet_controller_path',
               default="capnet-controller",
               help="Specify the filter name of the path to the Capnet controller binary.  This is a rootwrap-based command, so you add a new command by adding a new filter in /etc/neutron/rootwrap.d/capnet.filters.  In that file, you'll see a 'capnet-controller' filter; add another one just like it, but that points to the binary you want to execute instead.  Give it a custom name that's not already in the file, and set this value to that name.  BUT -- NB -- if you specify a full path, rootwrap will not be used to run this command, and your binary will run with regular neutron privileges!  That is probably not what you want."),
    cfg.StrOpt('capnet_controller_rundir',default="/var/tmp",
               help="A rundir for the capnet controller to keep state."),
    cfg.StrOpt('capnet_controller_args',
               default="-d -Z 16 -W 16 -L ALL --redirect-stdio",
               help="Specify extra args to the Capnet switch controller."),
    cfg.IntOpt('capnet_controller_base_port',default=6733,
               help="Controller base port for multi-controller mode."),
    cfg.IntOpt('capnet_controller_gdb_attach_wait_time',default=0,
               help="Wait this many seconds for GDB to attach to the controller before firing anything else off (like a workflow agent)."),
               
    cfg.BoolOpt('impotent_mode',default=False,
                help="Do not set the bridges' controllers to Capnet controller; just install a NORMAL L2 forwarding flow."),
    
    cfg.StrOpt('neutron_metadata_service_location',
               default="",
               help="Specify where, if any, the Neutron metadata service proxy is running.  This is configurable in Neutron to run either in the DHCP agent or in the L3 agent.  Thus, here you can set either 'dhcp', 'l3', or '' (if you don't want the metadata service to be automatically enabled."),

    cfg.StrOpt('dnsmasq_config_file',default='',
               help="A custom dnsmasq config file for Capnet networks."),
]


cfg.CONF.register_opts(capnet_opts, "CAPNET")
