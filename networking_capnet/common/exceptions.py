
from neutron.common import exceptions as n_exc

class NoAgentsAvailable(n_exc.ResourceExhausted):
    message = _("Unable to create the resource. No agent is available.")

class PortNotBound(n_exc.NotFound):
    message = _("Unable to complete operation on port; port is not bound")
