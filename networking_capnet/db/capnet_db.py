
from oslo_log import log as logging

from neutron.api import extensions
from neutron.api.v2 import base
from neutron.api.v2 import resource
from neutron.api.v2 import attributes
from neutron.common import exceptions as n_exc
from neutron import manager
from neutron import policy
from neutron import wsgi
from neutron.common import rpc as n_rpc
from neutron.common import exceptions as n_exc
from neutron.db import agents_db

from neutron.plugins.ml2 import db as ml2_db

from networking_capnet.common import constants as cn_const
from networking_capnet.common import exceptions as cn_exc
from networking_capnet.db.models.capnet_models \
    import CapnetAllocation,CapnetNetwork,CapnetPolicy, \
        CapnetWorkflowAgent,CapnetPortBinding,CapnetWorkflowAgentCap
from networking_capnet.api.rpc.agentnotifiers.capnet_rpc_agent_api \
    import CapnetExtAgentAPI


LOG = logging.getLogger(__name__)


class CapnetDBMixin(agents_db.AgentDbMixin):
    
    @property
    def __plugin(self):
        return manager.NeutronManager.get_plugin()
    
    @property
    def _capnet_ext_agent_api(self):
        # Try to avoid spawning more rpc clients than we need.
        api = None
        try:
            LOG.warning(_LW("current Neutron plugin does not support"
                            " agents; Capnet requires agents!"))
            api = self.__plugin.agent_notifiers.get(cn_const.CAPNET_AGENT)
        except:
            api = CapnetExtAgentAPI(plugin=self.__plugin)
        return api
    
    def _schedule_workflow_host(self,context,wfagent):
        """
        Pick a capnet agent to run this workflow app instance.
        """
        # Be nice about agent support
        if not hasattr(self.__plugin,'get_agents'):
            raise n_exc.NeutronException("current Neutron plugin doesn't support"
                                         " agents; Capnet requires agents!")
        afilters = { 'agent_type' : [cn_const.AGENT_TYPE_CAPNET],
                     'admin_state_up' : [True],
                     'alive': [True] }
        agents = self.__plugin.get_agents(context,filters=afilters)

        # XXX: for now, just find the agent that is both the controller
        # host and workflow app host:
        ret = None
        for agent in agents:
            conf = agent['configurations']
            LOG.debug("considering agent: %s///%s" % (str(agent),str(conf)))
            if 'hosts_controllers' in conf and conf['hosts_controllers'] \
              and 'hosts_workflow_apps' in conf and conf['hosts_workflow_apps']:
                ret = agent
                break
            else:
                agent = None
            pass
        
        return ret
    
    def _get_master_controller_host(self,context):
        """
        If one Capnet agent is hosting the controllers, return that agent;
        else, return None.
        """
        # Be nice about agent support
        if not hasattr(self.__plugin,'get_agents'):
            raise n_exc.NeutronException("current Neutron plugin doesn't support"
                                         " agents; Capnet requires agents!")
        afilters = { 'agent_type' : [cn_const.AGENT_TYPE_CAPNET],
                     'admin_state_up' : [True],
                     'alive': [True] }
        agents = self.__plugin.get_agents(context,filters=afilters)

        # XXX: for now, just find the agent that is both the controller
        # host and workflow app host:
        master = None
        for agent in agents:
            conf = agent['configurations']
            LOG.debug("considering agent: %s///%s" % (str(agent),str(conf)))
            if 'hosts_controllers' in conf and conf['hosts_controllers'] \
              and 'is_master' in conf and conf['is_master']:
                if master:
                    LOG.debug("multiple masters found!")
                    return None
                else:
                    master = agent
                    pass
                pass
            pass
        
        return master
    
    # XXX: this means we are tied to the ML2 plugin.  How else to get
    # the machine hosting the port thought?  Every plugin seems to store
    # its own binding details :(.
    def _get_capnet_port_binding_host(self, session, port_id):
        return ml2_db.get_port_binding_host(session, port_id)
    
    def _wfagent_create(self,context,wfagent):
        """
        Create the workflow agent.  If successful, push the wfagent
        object to the DB and notify the appropriate phys host.
        """
        session = context.session
        
        with session.begin(subtransactions=True):
            session.add(wfagent)
        
            # Ok, now we have to notify our agent that it's got a new
            # workflow port
            if wfagent.port_id:
                # Don't schedule in this case; just find out which host
                # the port is bound to, and send it a notification.
                host = self.get_capnet_port_binding_host(session,wfagent.port_id)
                if not host:
                    raise cn_exc.PortNotBound()
                pass
            else:
                # We need to schedule a capnet agent to launch the workflow app
                agent = self._schedule_workflow_host(context,wfagent)
                if not agent:
                    raise cn_exc.NoAgentsAvailable()
                host = agent['host']
            
                LOG.debug("notifying capnet agent on %s : wfagent_create"
                          % (host,))
                pass
            pass
        
        # Notify our agent that it needs to create a port, or learn
        # about an existing port, and update the capnet binding once
        # it does that.
        self._capnet_ext_agent_api.wfagent_create(context,wfagent,host)
        pass
    
    def _wfagent_delete(self,context,wfagent):
        session = context.session
        if wfagent.port_id:
            host = self._get_capnet_port_binding_host(session,wfagent.port_id)
            if not host:
                raise cn_exc.PortNotBound()
        
            LOG.debug("notifying capnet agent on %s : wfagent_create" % (host,))
            
            self._capnet_ext_agent_api.wfagent_delete(context,wfagent,host)
            pass
        else:
            LOG.debug("skipping delete notification for workflow agent %s/%s"
                      " because it is not bound to a port"
                      % (wfagent.name,wfagent.id))
            pass
        
        session = context.session
        with session.begin(subtransactions=True):
            session.delete(wfagent)
            pass
        
        return {}
        
    def _db_capnet_wfagent_update(self,context,wfagent):
        session = context.session
        with session.begin(subtransactions=True):
            query = session.query(CapnetWorkflowAgent).filter(CapnetWorkflowAgent.id==wfagent['id'])
            if len(query.all()) == 1:
                wfa = query.first()
                wfa.status = wfagent['status']
                wfa.port_id = wfagent['port_id']
                session.merge(wfa)
            else:
                raise Exception("could not find wfagent %s" % (wfagent['id'],))
            pass
        return
    
    def __db_capnet_port_binding_notify(self,context,binding):
        # Notify the right set of capnet agents to update metadata
        # files.  If we're in multi-controller mode, notify all capnet
        # agents.  If not, only notify the host that has a controller.
        master_agent = self._get_master_controller_host(context)
        if master_agent:
            master_host = master_agent['host']
        else:
            master_host = None

        self._capnet_ext_agent_api.portbinding_update(context,binding,
                                                      host=master_host)
        pass

    def _db_capnet_port_binding_update(self,context,binding):
        """
        Update binding info from our agent, and send out a notification
        announcing the new (or updated) port.  The notification is sent
        to the single phys controller host if in single controller mode;
        otherwise it is a global message.  This happens for every capnet
        port.
        """
        if 'role' in binding and binding['role'] == 'uplink':
            # Just send it straight to the agent(s)
            self.__db_capnet_port_binding_notify(context,binding)
            return
        
        # Otherwise, need to put it into the DB.
        port_id = binding['port_id']
        session = context.session
        
        # yank out 'name' and 'dns_name' and put into port
        name = None
        dns_name = None
        if 'name' in binding:
            name = binding['name']
            del binding['name']
            pass
        if 'dns_name' in binding:
            name = binding['dns_name']
            del binding['dns_name']
            pass
        
        with session.begin(subtransactions=True):
            query = session.query(CapnetPortBinding).filter(
                CapnetPortBinding.port_id==port_id)
            if len(query.all()) > 0:
                cpb = query.first()
                for (k,v) in binding.iteritems():
                    if k == 'port_id':
                        pass
                    elif k == 'host' and cpb.host != v:
                        raise Exception("cannot change 'host' from %s to %s!"
                                        % (cpb.host,v,))
                    else:
                        setattr(cpb,k,v)
                cpb = session.merge(cpb)
                pass
            else:
                cpb = CapnetPortBinding(**binding)
                session.add(cpb)
                pass
        
            # XXX: why do I have to manually force these to be loaded?
            # lazy=False in the sqlalchemy relationships...
            #foo = cpb.port
            #bar = cpb.ipallocation
            
            pass
        
        # Resolve it again -- port and ipallocation don't seem to be
        # joined always.  Perhaps until we commit, the join doesn't happen?
        query = session.query(CapnetPortBinding).filter(
            CapnetPortBinding.port_id==port_id)
        
        cpb = query.first()
        foo = cpb.wfagent
            
        with session.begin(subtransactions=True):
            if not cpb.port is None and (name or dns_name):
                if name:
                    cpb.port.name = name
                if dns_name:
                    cpb.port.dns_name = dns_name
                
                cpb = session.merge(cpb)
                pass
            pass
        
        self.__db_capnet_port_binding_notify(context,dict(cpb))
        pass
    
    def _get_agent_bindings(self,context):
        # Be nice about agent support
        if not hasattr(self.__plugin,'get_agents'):
            raise n_exc.NeutronException("current Neutron plugin doesn't support"
                                         " agents; Capnet requires agents!")
        afilters = { 'agent_type' : [cn_const.AGENT_TYPE_CAPNET] } #,alive=[True])
        agents = self.__plugin.get_agents(context,filters=afilters)

        retval = []
        
        # XXX: for now, just find the agent that is both the controller
        # host and workflow app host:
        master = None
        for agent in agents:
            conf = agent['configurations']
            #LOG.debug("considering agent: %s///%s" % (str(agent),str(conf)))
            if agent['agent_type'] == cn_const.AGENT_TYPE_CAPNET \
              and agent['alive'] \
              and 'uplink_bindings' in conf:
                for cpb in conf['uplink_bindings']:
                    retval.append(cpb)
                    pass
                pass
            pass
        
        return retval
    
    def _db_capnet_list_port_bindings(self,context):
        retval = self._get_agent_bindings(context)
        
        session = context.session
        with session.begin(subtransactions=True):
            query = session.query(CapnetPortBinding)
            for cpb in query.all():
                retval.append(dict(cpb))
                pass
            pass
        
        return retval
    
    pass
