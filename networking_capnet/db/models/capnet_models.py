
from oslo_log import log as logging
import sqlalchemy as sa

#from neutron.api.v2 import attributes as attrs
from neutron.db import model_base
from neutron.db import models_v2

LOG = logging.getLogger(__name__)


class CapnetAllocation(model_base.BASEV2):
    """
    Represent persistent allocation state of a physical network.

    This table just exists to track how many capnet networks we have on a
    particular physical network, and make sure we don't 1) exceed the max and
    2) don't reuse recent capnet_ids, at least not unless the user wants that
    behavior.
    """

    __tablename__ = 'ml2_capnet_allocations'
    
    physical_network = sa.Column(sa.String(64), nullable=False)
    # This ID isn't used at present; capnet doesn't need a segmentation
    # number to enforce segmentation.  It's just here to track how many
    # virt capnet networks are atop each physical networks.
    capnet_id = sa.Column(sa.Integer, nullable=False, primary_key=True,
                          autoincrement=False)
    __table_args__ = (
        sa.UniqueConstraint(
            physical_network, capnet_id,
            name='uniq_ml2_capnet_allocations0physical_network0capnet_id'),
    )


class CapnetNetwork(model_base.BASEV2):
    __tablename__ = 'capnet_networks'
    
    network_id = sa.Column(sa.String(36),
                           sa.ForeignKey('networks.id',ondelete='CASCADE'),
                           nullable=False,primary_key=True)
    arp_responder = sa.Column(sa.Boolean,nullable=False)
    dhcp_flows = sa.Column(sa.Boolean,nullable=False)
    metadata_flows = sa.Column(sa.Boolean,nullable=False)
    
    network = sa.orm.relationship(
        models_v2.Network,
        backref=sa.orm.backref("BR_capnet_network_binding",uselist=False,
                               cascade='delete',lazy='joined'))


class CapnetPolicy(model_base.BASEV2):
    __tablename__ = 'capnet_policies'
    
    id = sa.Column(sa.String(36),nullable=False,primary_key=True)
    network_id = sa.Column(sa.String(36),
                           sa.ForeignKey('networks.id',ondelete='CASCADE'),
                           nullable=False)
    subnet_id = sa.Column(sa.String(36),
                          sa.ForeignKey('subnets.id',ondelete='CASCADE'))
    tenant_id = sa.Column(sa.String(36),nullable=False)
    
    wfapp_policy = sa.Column(sa.Enum('default','explicit'),
                             nullable=False,default='default')
    wfapp_path = sa.Column(sa.String(255))
    wfapp_env = sa.Column(sa.Enum('netns'))
    
    network = sa.orm.relationship(
        models_v2.Network,
        backref=sa.orm.backref("BR_capnet_policy_network_binding",uselist=False,
                               cascade='delete',lazy='joined'))
    
    __table_args__ = (
        sa.UniqueConstraint(
            id,network_id,subnet_id,tenant_id,
            name='uniq_capnet_policies0id0network_id0subnet_id0tenant_id'),
    )


class CapnetWorkflowAgent(model_base.BASEV2):
    __tablename__ = 'capnet_workflow_agents'
    
    id = sa.Column(sa.String(36),nullable=False,primary_key=True)
    name = sa.Column(sa.String(255),nullable=True)
    network_id = sa.Column(sa.String(36),
                           sa.ForeignKey('networks.id',ondelete='CASCADE'),
                           nullable=False)
    subnet_id = sa.Column(sa.String(36),
                          sa.ForeignKey('subnets.id',ondelete='CASCADE'))
    tenant_id = sa.Column(sa.String(36))
    port_id = sa.Column(sa.String(36),
                        sa.ForeignKey('ports.id',ondelete='CASCADE'))
    master = sa.Column(sa.Boolean(),default=False,nullable=False)
    wfapp_path = sa.Column(sa.String(255))
    wfapp_args = sa.Column(sa.Text(65536))
    wfapp_env = sa.Column(sa.Enum('netns'))
    status = sa.Column(sa.String(16),nullable=False,default='initializing')
    
    network = sa.orm.relationship(models_v2.Network) #,lazy=False)
    
    subnet = sa.orm.relationship(models_v2.Subnet) #,lazy=False)
    
    port = sa.orm.relationship(models_v2.Port) #,lazy=False)
    
    #binding = sa.orm.relationship(
    #    'CapnetPortBinding',
    #    primaryjoin='CapnetWorkflowAgent.port_id==CapnetPortBinding.port_id',
    #    foreign_keys=[port_id], #back_populates='wfagent')
    #    backref=sa.orm.backref("wfagent",uselist=False),lazy=False)
    
    caps = sa.orm.relationship('CapnetWorkflowAgentCap',
                               back_populates='wfagent') #lazy=False)
    
    __table_args__ = (
        sa.UniqueConstraint(
            id,port_id,
            name='uniq_capnet_workflow_agents0id0port_id'),
        sa.UniqueConstraint(
            network_id,subnet_id,tenant_id,port_id,
            name='uniq_capnet_workflow_agents0network_id0subnet_id0tenant_id0port_id'),
    )


class CapnetPortBinding(model_base.BASEV2):
    __tablename__ = 'capnet_port_bindings'
    
    port_id = sa.Column(sa.String(36),
                        sa.ForeignKey('ports.id',ondelete='CASCADE'),
                        #sa.ForeignKey('ipallocations.port_id'),
                        nullable=False,primary_key=True,)
    host = sa.Column(sa.String(255),nullable=False)
    datapath_id = sa.Column(sa.String(16))
    ofport = sa.Column(sa.Integer)
    device_name = sa.Column(sa.String(255))
    
    port = sa.orm.relationship(models_v2.Port,lazy=False)
    
    wfagent = sa.orm.relationship(
        CapnetWorkflowAgent,
        primaryjoin='CapnetPortBinding.port_id==CapnetWorkflowAgent.port_id',
        foreign_keys=[CapnetWorkflowAgent.port_id], #back_populates='wfagent')
        backref=sa.orm.backref("binding",uselist=False,lazy=False),uselist=False,lazy=False)

    #wfagent = sa.orm.relationship(
    #    CapnetWorkflowAgent,
    #    primaryjoin='CapnetPortBinding.port_id==CapnetWorkflowAgent.port_id',
    #    foreign_keys=[port_id],#back_populates='binding')
    #    backref="binding") #sa.orm.backref("binding",uselist=False,
    #                       #    cascade='delete',lazy='joined'))

    ipallocation = sa.orm.relationship(
        models_v2.IPAllocation,lazy=False,
        primaryjoin='CapnetPortBinding.port_id==models_v2.IPAllocation.port_id',
        foreign_keys=[port_id])


class CapnetWorkflowAgentCap(model_base.BASEV2):
    __tablename__ = 'capnet_workflow_agent_caps'

    id = sa.Column(sa.String(36),nullable=False,primary_key=True)
    agent_id = sa.Column(sa.String(36),
                         sa.ForeignKey('capnet_workflow_agents.id',
                                       ondelete='CASCADE'),
                         nullable=False)
    status = sa.Column(sa.String(16),nullable=False,default='initializing')
    cap = sa.Column(sa.String(255),nullable=False)
    
    wfagent = sa.orm.relationship(
        CapnetWorkflowAgent, back_populates='caps')
        #backref=sa.orm.backref("caps",uselist=True,
        #                       cascade='delete',lazy='joined'))
    
    __table_args__ = (
        sa.UniqueConstraint(
            id,agent_id,
            name='uniq_capnet_workflow_agent_caps0id0agent_id'),
    )

