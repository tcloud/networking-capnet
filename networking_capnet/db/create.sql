CREATE TABLE `ml2_capnet_allocations` (
  `physical_network` varchar(64) NOT NULL,
  `capnet_id` int unsigned NOT NULL,
  PRIMARY KEY (`capnet_id`),
  UNIQUE KEY (`physical_network`,`capnet_id`)
);

CREATE TABLE capnet_networks (
  network_id VARCHAR(36) NOT NULL PRIMARY KEY,
  arp_responder BOOLEAN NOT NULL default True,
  dhcp_flows BOOLEAN NOT NULL default True,
  metadata_flows BOOLEAN NOT NULL default True,
  FOREIGN KEY (network_id) REFERENCES networks (id) ON DELETE CASCADE
);


CREATE TABLE capnet_policies (
  id VARCHAR(36) NOT NULL PRIMARY KEY,
  network_id VARCHAR(36) NOT NULL,
  subnet_id VARCHAR(36),
  tenant_id VARCHAR(255) NOT NULL,
  wfapp_policy enum('default','explicit') NOT NULL default 'default',
  wfapp_path VARCHAR(255),
  wfapp_env enum('netns'),
  UNIQUE KEY (`network_id`,`tenant_id`),
  FOREIGN KEY (network_id) REFERENCES networks (id) ON DELETE CASCADE,
  FOREIGN KEY (subnet_id) REFERENCES subnets (id) ON DELETE CASCADE
);


CREATE TABLE capnet_port_bindings (
  port_id VARCHAR(36) NOT NULL PRIMARY KEY,
  host VARCHAR(255) NOT NULL,
  datapath_id VARCHAR(16),
  ofport INT UNSIGNED,
  device_name VARCHAR(255),
  FOREIGN KEY (port_id) REFERENCES ports (id) ON DELETE CASCADE
);


CREATE TABLE capnet_workflow_agents (
  id VARCHAR(36) NOT NULL PRIMARY KEY,
  name VARCHAR(255),
  network_id VARCHAR(36) NOT NULL,
  subnet_id VARCHAR(36),
  tenant_id VARCHAR(255) NOT NULL,
  port_id VARCHAR(36),
  master BOOLEAN NOT NULL default FALSE,
  wfapp_path VARCHAR(255),
  wfapp_args TEXT,
  wfapp_env enum('netns'),
  status VARCHAR(16) NOT NULL,
  UNIQUE KEY (`id`,`port_id`),
  UNIQUE KEY (`network_id`,`subnet_id`,`tenant_id`,`port_id`),
  FOREIGN KEY (network_id) REFERENCES networks (id) ON DELETE CASCADE,
  FOREIGN KEY (subnet_id) REFERENCES subnets (id) ON DELETE CASCADE,
  FOREIGN KEY (port_id) REFERENCES ports (id) ON DELETE CASCADE
);


CREATE TABLE capnet_workflow_agent_caps (
  id VARCHAR(36) NOT NULL PRIMARY KEY,
  agent_id VARCHAR(36) NOT NULL,
  status VARCHAR(16) NOT NULL,
  cap VARCHAR(255) NOT NULL,
  UNIQUE KEY (`id`,`agent_id`),
  FOREIGN KEY (agent_id) REFERENCES capnet_workflow_agents (id) ON DELETE CASCADE
);
