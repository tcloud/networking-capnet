
from oslo_log import log as logging

import uuid

from neutron.api import extensions
from neutron.api.v2 import base
from neutron.api.v2 import resource
from neutron.api.v2 import attributes
from neutron.common import exceptions
from neutron import manager
from neutron import policy
from neutron import wsgi
from neutron.common import rpc as n_rpc
from neutron.common import exceptions as n_exc

from networking_capnet.common import constants as cn_const
from networking_capnet.common import exceptions as cn_exc
from networking_capnet.db.models.capnet_models \
    import CapnetPolicy,CapnetWorkflowAgent
from networking_capnet.db import capnet_db as cn_db


LOG = logging.getLogger(__name__)

EXTENDED_ATTRIBUTES_2_0 = {
    'networks': {
        'capnet:arp_responder': {
            'allow_post': True,
            'allow_put': False,
            'default': attributes.ATTR_NOT_SPECIFIED,
            'is_visible': True,
        },
        'capnet:dhcp_flows': {
            'allow_post': True,
            'allow_put': False,
            'default': attributes.ATTR_NOT_SPECIFIED,
            'is_visible': True,
        },
        'capnet:metadata_flows': {
            'allow_post': True,
            'allow_put': False,
            'default': attributes.ATTR_NOT_SPECIFIED,
            'is_visible': True,
        },
    }
}

CAPNET_POLICY = 'capnet_policy'
CAPNET_POLICIES = 'capnet_policies'
CAPNET_WORKFLOW_AGENT = 'capnet_workflow_agent'
CAPNET_WORKFLOW_AGENTS = CAPNET_WORKFLOW_AGENT + 's'


class CapnetPolicyController(wsgi.Controller):
    
    def index(self, request, **kwargs):
        #plugin = manager.NeutronManager.get_plugin()
        policy.enforce(request.context,
                       "get_%s" % CAPNET_POLICIES,
                       {})
        args = dict(**kwargs)
        context = request.context
        session = context.session
        LOG.debug("request = %s ; context = %s ; args = %s ; kwargs = %s"
                  % (str(request),str(context),str(args),str(dict(**kwargs))))
        query = session.query(CapnetPolicy)
        filter = {}
        if 'id' in args:
            filter['id'] = args['id']
        if 'network_id' in args:
            filter['network_id'] = args['network_id']
        if 'subnet_id' in args:
            filter['subnet_id'] = args['subnet_id']
        if not context.is_admin:
            if 'tenant_id' in args and context.tenant_id != args[tenant_id]:
                raise n_exc.NotAuthorized()
            else:
                filter['tenant_id'] = context.tenant_id
            pass
        elif 'tenant_id' in args:
            filter['tenant_id'] = args['tenant_id']
            pass
        query = query.filter(**filter)
        result = { CAPNET_POLICIES : [] }
        for qres in query:
            result[CAPNET_POLICIES].append(dict(qres))
        return result
    
    def show(self, request, id, **kwargs):
        policy.enforce(request.context,
                       "get_%s" % CAPNET_POLICY,
                       {})
        LOG.debug("id = %s" % (str(id)))
        context = request.context
        session = context.session
        query = session.query(CapnetPolicy).filter(id == id)
        if len(query.all()) < 1:
            raise n_exc.ObjectNotFound()
        cp = query.first()
        if not context.is_admin and cp.tenant_id != context.tenant_id:
            raise n_exc.NotAuthorized()
        return { CAPNET_POLICY: dict(cp) }

    def create(self, request, body, **kwargs):
        policy.enforce(request.context,
                       "create_%s" % CAPNET_POLICY,
                       {})
        args = body['capnet_policy']
        LOG.debug("args = %s" % (str(args)))
        context = request.context
        session = context.session
        query = session.query(CapnetPolicy)
        filter = {}
        for arg in ['subnet_id','network_id']:
            if arg in args:
                filter[arg] = args[arg]
            else:
                filter[arg] = None
            pass
        if 'tenant_id' in args:
            filter['tenant_id'] = args['tenant_id']
        else:
            filter['tenant_id'] = context.tenant_id
            pass
        if 'master' in args and args['master'] == True:
            filter['master'] = args['master']
            pass
        query = query.filter_by(**filter)
        if len(query.all()) > 0:
            raise n_exc.InUse()
        if not context.is_admin \
          and 'tenant_id' in args and context.tenant_id != args[tenant_id]:
            raise n_exc.NotAuthorized()
        elif not 'tenant_id' in args:
            args['tenant_id'] = context.tenant_id
            pass
            
        id = uuid.uuid4()
        args['id'] = str(id)
        with session.begin(subtransactions=True):
            db = CapnetPolicy(**args)
            session.add(db)
        return { CAPNET_POLICY : dict(db) }

    def delete(self, request, id, **kwargs):
        policy.enforce(request.context,
                       "delete_%s" % CAPNET_POLICY,
                       {})
        LOG.debug("id = %s" % (str(id)))
        context = request.context
        session = context.session
        query = session.query(CapnetPolicy).filter(id == id)
        if len(query.all()) < 1:
            raise n_exc.ObjectNotFound()
        cp = query.first()
        if not context.is_admin and cp.tenant_id != context.tenant_id:
            raise n_exc.NotAuthorized()
        with session.begin(subtransactions=True):
            session.delete(cp)
            pass
        return {}


class CapnetWorkflowAgentController(wsgi.Controller,cn_db.CapnetDBMixin):

    def index(self, request, id=None, name=None, **kwargs):
        policy.enforce(request.context,
                       "get_%s" % CAPNET_WORKFLOW_AGENTS,
                       {})
        args = dict(**kwargs)
        context = request.context
        session = context.session
        LOG.debug("request = %s ; context = %s ; id = %s name = %s ; args = %s ; kwargs = %s"
                  % (str(request),str(context),str(id),str(name),str(args),str(dict(**kwargs))))
        filter = {}
        if 'id' in args:
            filter['id'] = args['id']
        if 'name' in args:
            filter['id'] = args['id']
        if 'network_id' in args:
            filter['network_id'] = args['network_id']
        if 'subnet_id' in args:
            filter['subnet_id'] = args['subnet_id']
        if 'port_id' in args:
            filter['port_id'] = args['port_id']
        if not context.is_admin:
            if 'tenant_id' in args and context.tenant_id != args[tenant_id]:
                raise n_exc.NotAuthorized()
            else:
                filter['tenant_id'] = context.tenant_id
            pass
        elif 'tenant_id' in args:
            filter['tenant_id'] = args['tenant_id']
            pass
        query = session.query(CapnetWorkflowAgent)
        query = query.filter(**filter)
        result = { CAPNET_WORKFLOW_AGENTS : [] }
        for qres in query:
            result[CAPNET_WORKFLOW_AGENTS].append(dict(qres))
        return result
    
    def show(self, request, id, **kwargs):
        policy.enforce(request.context,
                       "get_%s" % CAPNET_WORKFLOW_AGENT,
                       {})
        LOG.debug("id = %s" % (str(id)))
        context = request.context
        session = context.session
        query = session.query(CapnetWorkflowAgent).filter(id == id)
        if len(query.all()) < 1:
            raise n_exc.ObjectNotFound()
        cp = query.first()
        if not context.is_admin and cp.tenant_id != context.tenant_id:
            raise n_exc.NotAuthorized()
        return { CAPNET_WORKFLOW_AGENT: dict(cp) }

    def create(self, request, body, **kwargs):
        plugin = manager.NeutronManager.get_plugin()
        policy.enforce(request.context,
                       "create_%s" % CAPNET_WORKFLOW_AGENT,
                       {})
        args = body['capnet_workflow_agent']
        LOG.debug("args = %s" % (str(args)))
        if 'port_id' in args and ('wfapp_path' in args or 'wfapp_env' in args):
            raise n_exc.BadRequest("cannot specify port and wfapp_path or wfapp_env")
        context = request.context
        session = context.session
        query = session.query(CapnetWorkflowAgent)
        # We only filter for dups if name or master is the same for a given
        # (network_id,subnet_id,tenant_id tuple)
        if 'name' in args or ('master' in args and args['master'] == True):
            filter = {}
            for arg in ['subnet_id','network_id','port_id']:
                if arg in args:
                    filter[arg] = args[arg]
                else:
                    filter[arg] = None
                pass
            if 'name' in args:
                filter['name'] = args['name']
            if 'master' in args and args['master']:
                filter['master'] = args['master']
                pass
            query = query.filter_by(**filter)
            if len(query.all()) > 0:
                raise n_exc.InUse()
            pass
        if not context.is_admin \
          and 'tenant_id' in args and context.tenant_id != args[tenant_id]:
            raise n_exc.NotAuthorized()
        elif not 'tenant_id' in args:
            args['tenant_id'] = context.tenant_id
            pass

        id = uuid.uuid4()
        args['id'] = str(id)
        db = CapnetWorkflowAgent(**args)
        
        # Call to cn_db mixin to create the agent.  This is semi-synchronous;
        # we create a port, and call to the agent to ensure it has received
        # the creation.
        self._wfagent_create(context,db)
        
        return { CAPNET_WORKFLOW_AGENT : dict(db) }

    def delete(self, request, id, **kwargs):
        plugin = manager.NeutronManager.get_plugin()
        policy.enforce(request.context,
                       "delete_%s" % CAPNET_WORKFLOW_AGENT,
                       {})
        LOG.debug("id = %s" % (str(id)))
        context = request.context
        session = context.session
        query = session.query(CapnetWorkflowAgent).filter(id == id)
        if len(query.all()) < 1:
            raise n_exc.ObjectNotFound()
        cp = query.first()
        if not context.is_admin and cp.tenant_id != context.tenant_id:
            raise n_exc.NotAuthorized()
        
        return self._wfagent_delete(context,cp)
    
    pass

class Capnet(extensions.ExtensionDescriptor):

    @classmethod
    def get_name(cls):
        return "Capnet Extension"

    @classmethod
    def get_alias(cls):
        return "capnet"

    @classmethod
    def get_description(cls):
        return "Add capnet attributes to network resource"

    @classmethod
    def get_updated(cls):
        return "2016-03-18T16:56:06-00:00"

    @classmethod
    def get_resources(cls):
        """Returns Ext Resources."""
        exts = []
        controller = resource.Resource(CapnetPolicyController(),
                                       base.FAULT_MAP)
        exts.append(extensions.ResourceExtension(
            CAPNET_POLICIES, controller))

        controller = resource.Resource(CapnetWorkflowAgentController(),
                                       base.FAULT_MAP)
        exts.append(extensions.ResourceExtension(
            CAPNET_WORKFLOW_AGENTS, controller))
        return exts

    def get_extended_resources(self, version):
        if version == "2.0":
            return EXTENDED_ATTRIBUTES_2_0
        else:
            return {}
    pass

#def notify(context, action, network_id, agent_id):
#    info = {'id': agent_id, 'network_id': network_id}
#    notifier = n_rpc.get_notifier('network')
#    notifier.info(context, action, {'agent': info})

