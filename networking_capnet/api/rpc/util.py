
import itertools
from oslo_log import log as logging

from neutron.common import topics
from neutron.common import rpc as n_rpc

LOG = logging.getLogger(__name__)

## class DynamicConnection(n_rpc.Connection):

##     def __init__(self):
##         super(DynamicConnection, self).__init__()
##         self.servers = []
##         self.running = False

##     def create_consumer(self, topic, endpoints, fanout=False):
##         target = oslo_messaging.Target(
##             topic=topic, server=cfg.CONF.host, fanout=fanout)
##         server = get_server(target, endpoints)
##         self.servers.append(server)

##     def add_consumer(self, topic, endpoints, fanout=False):
##         target = oslo_messaging.Target(
##             topic=topic, server=cfg.CONF.host, fanout=fanout)
##         server = get_server(target, endpoints)
##         self.servers.append(server)

##     def consume_in_threads(self):
##         self.running = True
##         for server in self.servers:
##             server.start()
##         return self.servers

##     def close(self):
##         for server in self.servers:
##             server.stop()
##         for server in self.servers:
##             server.wait()
##         self.running = False


def add_consumers(connection, endpoints, prefix, topic_details):
    """
    Add agent RPC consumers to an existing connection.  Derived from
    neutron.agent.rpc:create_consumers.

    :param endpoints: The list of endpoints to process the incoming messages.
    :param prefix: Common prefix for the plugin/agent message queues.
    :param topic_details: A list of topics. Each topic has a name, an
                          operation, and an optional host param keying the
                          subscription to topic.host for plugin calls.
    :param node_only: Set to True if you only want the host-specific topics
                      to be created.

    :returns: None
    """

    for details in topic_details:
        topic, operation, node_name = itertools.islice(
            itertools.chain(details, [None]), 3)

        topic_name = topics.get_topic_name(prefix, topic, operation)
        LOG.debug("%s %s" % (str(topic_name),str(endpoints)))
        connection.create_consumer(topic_name, endpoints, fanout=True)
        if node_name:
            node_topic_name = '%s.%s' % (topic_name, node_name)
            LOG.debug("%s %s" % (str(node_topic_name),str(endpoints)))
            connection.create_consumer(node_topic_name,
                                       endpoints,
                                       fanout=False)
    return None
