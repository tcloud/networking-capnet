# Copyright (c) 2012 OpenStack Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import copy
import itertools
import operator

from oslo_config import cfg
from oslo_db import exception as db_exc
from oslo_log import log as logging
import oslo_messaging
from oslo_utils import excutils

from neutron.api.v2 import attributes
from neutron.common import constants
from neutron.common import exceptions as n_exc
from neutron.common import utils
from neutron.db import api as db_api
from neutron.extensions import portbindings
from neutron.i18n import _LW
from neutron import manager
from neutron.plugins.common import utils as p_utils
from neutron.quota import resource_registry

from networking_capnet.common import constants as cn_const
from networking_capnet.db.models.capnet_models \
    import CapnetAllocation,CapnetNetwork,CapnetPolicy, \
        CapnetWorkflowAgent,CapnetPortBinding,CapnetWorkflowAgentCap
from networking_capnet.db import capnet_db as cn_db


LOG = logging.getLogger(__name__)


class CapnetExtRpcCallbacks(cn_db.CapnetDBMixin):
    """
    Capnet agent calls to this RPC interface in plugin implementations (for
    workflow apps and notifications).

    This class implements the server side of an rpc interface.  The
    client side of this interface can be found in
    neutron.plugins.ml2.drivers.capnet.capnet_neutron_agent.CapnetExtPluginApi.
    For more information about changing rpc interfaces, see
    doc/source/devref/rpc_api.rst.

    API version history:
        1.0 - Initial version.
    """
    target = oslo_messaging.Target(namespace=cn_const.RPC_NAMESPACE_CAPNET_PLUGIN,
                                   version='1.0')
    
    def hello_world(self,context,**kwargs):
        return "Hello, World!"
    
    def capnet_get_port_bindings(self,context):
        """
        Returns a list of all Capnet port bindings, plus special uplink
        port bindings stored by each Capnet agent as metadata.
        """
        return self._db_capnet_list_port_bindings(context)
    
    def capnet_port_binding_update(self,context,binding):
        """
        Update Capnet port binding info.
        """
        if not 'port_id' in binding:
            raise n_exc.BadRequest("missing port_id field")
        
        return self._db_capnet_port_binding_update(context,binding)

    @db_api.retry_db_errors
    @resource_registry.mark_resources_dirty
    def wfagent_port_create(self,context,port,host):
        """
        Create and return wfagent port information.

        If an expected failure occurs, a None port is returned.
        """
        # Note(pbondar): Create deep copy of port to prevent operating
        # on changed dict if RetryRequest is raised
        portc = copy.deepcopy(port)
        LOG.debug('Create wfagent port %(port)s from %(host)s.',
                  {'port': portc,'host': host})

        portc['device_owner'] = cn_const.DEVICE_OWNER_CAPNET_WFAGENT
        portc[portbindings.HOST_ID] = host
        if 'mac_address' not in portc:
            portc['mac_address'] = attributes.ATTR_NOT_SPECIFIED
        plugin = manager.NeutronManager.get_plugin()
        return self._port_action(plugin,context,{'port':portc},'create_port')

    @db_api.retry_db_errors
    def wfagent_port_delete(self,context,port):
        """Release the port currently being used by a DHCP agent."""
        network_id = port.get('network_id')
        device_id = port.get('device_id')

        LOG.debug('wfagent port deletion for %s' % (device_id,))
        plugin = manager.NeutronManager.get_plugin()
        plugin.delete_ports_by_device_id(context,device_id,network_id)
        pass

    @db_api.retry_db_errors
    def wfagent_update(self,context,wfagent):
        """Update the capnet wfagent's port_id and status."""
        
        LOG.debug('Update wfagent: %s' % (str(wfagent),))

        return self._db_capnet_wfagent_update(context,wfagent)

    def _port_action(self, plugin, context, port, action):
        """Perform port operations taking care of concurrency issues."""
        try:
            if action == 'create_port':
                return p_utils.create_port(plugin, context, port)
            elif action == 'update_port':
                return plugin.update_port(context, port['id'], port)
            else:
                msg = _('Unrecognized action')
                raise n_exc.Invalid(message=msg)
        except (db_exc.DBError, n_exc.NetworkNotFound,
                n_exc.SubnetNotFound, n_exc.IpAddressGenerationFailure) as e:
            with excutils.save_and_reraise_exception(reraise=False) as ctxt:
                if isinstance(e, n_exc.IpAddressGenerationFailure):
                    # Check if the subnet still exists and if it does not,
                    # this is the reason why the ip address generation failed.
                    # In any other unlikely event re-raise
                    try:
                        subnet_id = port['port']['fixed_ips'][0]['subnet_id']
                        plugin.get_subnet(context, subnet_id)
                    except n_exc.SubnetNotFound:
                        pass
                    else:
                        ctxt.reraise = True
                net_id = port['port']['network_id']
                LOG.warn(_LW("Action %(action)s for network %(net_id)s "
                             "could not complete successfully: %(reason)s"),
                         {"action": action, "net_id": net_id, 'reason': e})

    def capnet_get_network_info(self, context, **kwargs):
        """
        Retrieve and return full information about one or all Capnet
        networks.  This method returns *all* Capnet network and subnet
        info, whether or not a host filter kwarg is specified.  If a
        host filter kwarg is specified, though, it only returns ports
        bound to this host.
        """
        session = context.session
        
        network_filters = { 'provider:network_type':'capnet' }
        if 'network_id' in kwargs:
            network_filters['network_id'] = [kwargs.get('network_id')]
        host = None
        if 'host' in kwargs:
            host = kwargs.get('host')
        
        LOG.debug('Returning Capnet network info (network_filters=%s,host=%s)'
                  % (str(network_filters),str(host)))

        plugin = manager.NeutronManager.get_plugin()
        try:
            networks = plugin.get_networks(context,filters=network_filters)
        except n_exc.NetworkNotFound:
            LOG.warn(_LW("Network %s could not be found, it might have "
                         "been deleted concurrently."), network_id)
            return
        for network in networks:
            filters = { 'network_id':[network['id']] }
            network['subnets'] = plugin.get_subnets(context,filters=filters)
            qres = session.query(CapnetWorkflowAgent).filter(CapnetWorkflowAgent.network_id==network['id'])
            if qres and len(qres.all()) > 0:
                network['wfagents'] = qres.all()
            #if host:
            #    filters['binding:host_id'] = host
            #    pass
            ports = plugin.get_ports(context,filters=filters)
            # Ugh, this sucks, but unless we query the ML2 binding
            # directly, not sure how we can filter the ports on host.
            if host:
                newports = []
                for port in ports:
                    if port['binding:host_id'] == host:
                        newports.append(port)
                ports = newports
                pass
            network['ports'] = ports
        return networks

    ## @db_api.retry_db_errors
    ## def release_port_fixed_ip(self, context, **kwargs):
    ##     """Release the fixed_ip associated the subnet on a port."""
    ##     host = kwargs.get('host')
    ##     network_id = kwargs.get('network_id')
    ##     device_id = kwargs.get('device_id')
    ##     subnet_id = kwargs.get('subnet_id')

    ##     LOG.debug('DHCP port remove fixed_ip for %(subnet_id)s request '
    ##               'from %(host)s',
    ##               {'subnet_id': subnet_id, 'host': host})
    ##     plugin = manager.NeutronManager.get_plugin()
    ##     filters = dict(network_id=[network_id], device_id=[device_id])
    ##     ports = plugin.get_ports(context, filters=filters)

    ##     if ports:
    ##         port = ports[0]

    ##         fixed_ips = port.get('fixed_ips', [])
    ##         for i in range(len(fixed_ips)):
    ##             if fixed_ips[i]['subnet_id'] == subnet_id:
    ##                 del fixed_ips[i]
    ##                 break
    ##         plugin.update_port(context, port['id'], dict(port=port))
    
