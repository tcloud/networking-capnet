
import sys
import os
import socket
import traceback

##
## Ok, we want the user to be able to specify where the Capnet Protocol
## bindings are installed, so we can set an appropriate PYTHONPATH
## before launching their agent.  We put this into the Neutron config
## files... so we have to load them up.  I don't understand oslo_config,
## so I'm just doing what neutron-server does... ugh!
##
from oslo_config import cfg
from neutron.common import config

config_file_args = [ '--config-file','/etc/neutron/plugins/ml2/ml2_conf.ini',
                     '--config-file','/etc/neutron/neutron.conf',
                     '--config-file','/etc/neutron/plugins/ml2/ml2_conf_capnet.ini' ]
config.init(config_file_args)

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')

##
## A simple script that execs its args.  Eventually, this script will
## take a uid/gid to drop privs to, after opening a raw sock on the
## specified interface.  For now, it takes a pidfile, a logfile, and an
## interface name to execute the workflow app against.
##

def main():
    pidfile = sys.argv[1]
    logfile = sys.argv[2]
    iface = sys.argv[3]
    prog = sys.argv[4]
    args = [prog,iface]
    if len(sys.argv) > 5:
        for a in sys.argv[5:]:
           args.append(a)
        pass
    
    pf = file(pidfile,'w')
    lf = file(logfile,'a')
    os.dup2(lf.fileno(),1)
    os.dup2(lf.fileno(),2)
    
    extrapaths = []
    newenviron = dict(os.environ)
    try:
        for path in cfg.CONF.CAPNET.pythonpath:
            extrapaths.append(path)
        if len(extrapaths) > 0:
            newenviron['PYTHONPATH'] = ':'.join(extrapaths)
        pass
    except:
        traceback.print_exc()
        pass
    
    pid = os.fork()
    if pid == 0:
        pf.write("%d" % (os.getpid(),))
        pf.close()
        if len(extrapaths) > 0:
            print "DEBUG: running %s (with PYTHONPATH=%s)" % (str(args),newenviron['PYTHONPATH'])
        else:
            print "DEBUG: running %s" % (str(args))
            pass
        os.execve(prog,args,newenviron)
        return -1
    else:
        return 0
    pass
