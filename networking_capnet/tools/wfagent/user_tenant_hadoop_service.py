#!/usr/bin/env python

##
## This is one half of a simple example that uses a brokered service
## tenant to configure flows on behalf of some compute tenant.  This is
## the master wfa for the compute tenant.  It looks up the "hadoop"
## service, and each time it receives a capability to a node it owns, it
## passes it to the hadoop service tenant.  The hadoop service
## tenant master wfa is in service_tenant_hadoop_service.py .
##

import sys
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import capnet.capnet as cn

def log(msg):
    print msg
    sys.stdout.flush()
    pass

Entry = namedtuple("Entry", ["node", "info", "rp", "flow"])

def main():
    nodes = {}

    p = cn.Protocol(sys.argv[1])
    i = 0
    while True:
        try:
            log("getting rp0 (try %d)" % (i,))
            rp0 = p.rp0()
            break
        except:
            log("ERROR: failed to get rp0 in 5 seconds; will keep trying!")
            traceback.print_exc()
            pass
        i += 1
        pass
    
    me = None
    myname = None

    # The wfagent cap comes to us first, after rp0.
    log("rp0 recv me")
    me = rp0.recv()
    log("node info")
    info = me.info()
    log("node flow")
    flow = me.flow()
    node_rp = p.create(cn.RP)
    nodes[info.name] = Entry(me,info,node_rp,flow)
    myname = info.name
    log("Received self cap: %s" % (info.name,))

    log("rp0 recv broker")
    broker = rp0.recv()
    if type(broker) != cn.Broker:
        log("Second cap not a broker (was %s)" % (str(type(broker)),))
        pass
    log("Received broker cap")
    
    # The RP for the hadoop service
    service_rp = None
    
    # Now receive node caps forever, and grant flows from us to everyone
    # else, and vice versa.
    while True:
        (node,info,flow,name) = (None,None,None,None)
        try:
            log("rp0 recv")
            node = rp0.recv()
            log("node info")
            info = node.info()
            log("node flow")
            flow = node.flow()
            node_rp = p.create(cn.RP)
            node.reset(node_rp)
            name = info.name
            nodes[name] = Entry(node,info,node_rp,flow)
            log("Received new node: %s" % (str(info),))
        except Exception, e:
            (node,info,flow,node_rp) = (None,None,None,None)
            log("Exception %s" % (str(e),))
            pass
        
        if node:
            # Allow it to communicate with us, we'll want to talk to the
            # master and maybe other nodes later.
            try:
                log("Sending flow ->%s to %s" % (myname,name))
                p.debug_grant(node,nodes[myname].flow)
                #a.rp.send(b.flow)
                
                log("Sending flow ->%s to %s" % (name,myname))
                p.debug_grant(me,flow)
                #b.rp.send(a.flow)
            except:
                log("Exception %s" % (str(e),))
                pass
            pass
        
        if not service_rp:
            try:
                service_rp = broker.lookup("hadoop")
            
                log("Found service hadoop, sending it %d nodes" \
                    % (len(nodes.keys()),))
            
                # Send all the nodes we've received so far!
                for (name,entry) in nodes.iteritems():
                    # Except don't send our wfa!
                    if entry.node == me:
                        continue
                    else:
                        service_rp.send(entry.node)
                        pass
                    pass
                pass
            except Exception, e:
                log("Exception %s" % (str(e),))
                pass
            pass
        elif node:
            try:
                log("Sending cap to %s to hadoop service" % (info.name,))
                service_rp.send(node)
                log("Sent cap to %s to hadoop service" % (info.name,))
            except Exception, e:
                log("Exception %s" % (str(e),))
                pass
            pass

        time.sleep(2)
        
        pass

    pass
