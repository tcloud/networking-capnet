#!/usr/bin/env python

##
## This is the service half of a simple example that uses a brokered
## service tenant to configure flows on behalf of some compute tenant.
## This is the master wfa for the service tenant.  It registers itself
## with the broker as the "hadoop" service, and each time it receives
## a capability to a node, it adds it to an all-pairs flow mesh.  The
## hadoop user tenant master wfa is in user_tenant_hadoop_service.py
##

import sys
import os
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import capnet.capnet as cn

def log(msg):
    print msg
    sys.stdout.flush()
    pass

Entry = namedtuple("Entry", ["node", "info", "rp", "flow"])

def main():
    nodes = {}

    p = cn.Protocol(sys.argv[1])
    i = 0
    while True:
        try:
            log("getting rp0 (try %d)" % (i,))
            rp0 = p.rp0()
            break
        except:
            log("ERROR: failed to get rp0 in 5 seconds; will keep trying!")
            traceback.print_exc()
            pass
        i += 1
        pass
    
    me = None
    myname = None

    # The wfagent cap comes to us first, after rp0.
    log("rp0 recv me")
    me = rp0.recv()
    log("node info")
    info = me.info()
    log("node flow")
    flow = me.flow()
    node_rp = p.create(cn.RP)
    nodes[info.name] = Entry(me,info,node_rp,flow)
    myname = info.name
    log("Received self cap: %s" % (info.name,))

    log("rp0 recv broker")
    broker = rp0.recv()
    if type(broker) != cn.Broker:
        log("Second cap not a broker (was %s)" % (str(type(broker)),))
        pass
    log("Received broker cap")
    
    # The RP for the hadoop service
    service_rp = p.create(cn.RP)
    broker.register("hadoop",service_rp)
    log("Registered hadoop service")
    
    # Now receive node caps until we have a master, a resourcemanager,
    # and we haven't heard from the controller for 60 N-second cycles
    # (N is currently ~2s).
    master = None
    resourcemanager = None
    slaves = []
    counter = 0
    while True:
        (node,info,flow,name) = (None,None,None,None)
        try:
            log("service_rp recv")
            node = service_rp.recv()
            log("node info")
            info = node.info()
            log("node flow")
            flow = node.flow()
            node_rp = p.create(cn.RP)
            node.reset(node_rp)
            name = info.name
            nodes[name] = Entry(node,info,node_rp,flow)
            log("Received new node: %s" % (str(info),))
            counter = 0
        except Exception, e:
            counter += 1
            (node,info,flow,node_rp,name) = (None,None,None,None,None)
            log("Exception %s" % (str(e),))
            pass
        
        if not name is None:
            # Figure out which node this is.
            if name == 'master':
                master = nodes[name]
            elif name == 'resourcemanager':
                resourcemanager = nodes[name]
            else:
                slaves.append(nodes[name])
                pass
            
            # Setup the allpairs communication, because this is what
            # hadoop needs if we don't have fine-grained flows.
            try:
                a = nodes[name]
                for (bname,b) in nodes.iteritems():
                    log("Sending flow ->%s to %s" % (b.info.name,name))
                    p.debug_grant(a.node,b.flow)
                    #a.rp.send(b.flow)
                    
                    log("Sending flow ->%s to %s" % (name,b.info.name))
                    p.debug_grant(b.node,a.flow)
                    #b.rp.send(a.flow)
                    pass
            except:
                log("ERROR: while adding node %s into all-pairs mesh:" % (name,))
                traceback.print_exc()
                pass
            pass
        
        if master and resourcemanager and counter > 59:
            break

        time.sleep(1)
        
        pass
    
    log("Assuming all nodes have been granted to us; setting up hadoop!")
    args = ""
    log("master = %s %s %s" % (master.info.name,master.info.ipv4,
                               master.info.mac))
    args += " %s" % (master.info.ipv4,)
    log("resourcemanager = %s %s %s" % (resourcemanager.info.name,
                                        resourcemanager.info.ipv4,
                                        resourcemanager.info.mac))
    args += " %s" % (resourcemanager.info.ipv4,)
    for slave in slaves:
        log("slave = %s %s %s" % (slave.info.name,slave.info.ipv4,
                                  slave.info.mac))
        args += " %s" % (slave.info.ipv4,)
        pass
    
    user = 'ubuntu'
    mastercmd = "/home/ubuntu/setup.sh"
    cmd = "ssh -o StrictHostKeyChecking=no %s@%s %s %s" % (user,master.info.ipv4,mastercmd,args)
    log("Running '%s'..." % (cmd,))
    os.system(cmd)
    log("Finished '%s'..." % (cmd,))
    
    pass
