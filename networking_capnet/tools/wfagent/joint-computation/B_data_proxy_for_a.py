import capnet

p = capnet.Protocol("... intf ...")

rp0 = p.rp0()

b_seal_unseal = rp0.recv_wait()
A_data_update_flow = rp0.recv_wait()
A_data_sealed_rp = rp0.recv_wait()

A_data_ab_sealed_rp = b_seal_unseal.seal(A_data_sealed_rp)
rp0.send(A_data_ab_sealed_rp)

# Run proxy to begin receiving requests


