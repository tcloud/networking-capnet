import capnet

p = capnet.Protocol("...intf...")

rp0 = p.rp0()

a_seal_unseal = rp0.recv_wait()
data_rp = p.create(capnet.RP)
sealed_data_rp = a_seal_unseal.seal(data_rp)
rp0.send(sealed_data_rp)

# Wait till step 5

a_compute_pxy = rp0.recv_wait()

# Run proxy to begin receiving requests
