import capnet
from contextlib import contextmanager

def build_node_db(p, nodes):
    db = {}
    for node in nodes:
        info = node.info()
        rp = p.create(capnet.RP)
        grant = node.reset(rp)
        db[info.name] = NodeEntry(node, info, rp, grant)
    return db

def aaas_send(p, init_rp, nodes):
    """AaaS (Appliance as a Service) Reciver

    This call is used by the party that is trying to isolate their capabilities
    from the party that wants to temporarily use those capabilites. """

    membrane = p.create(capnet.Membrane)
    membrane_ext = membrane.external()
    init_rp.send(membrane_ext)

    membrane_rp = p.create(capnet.RP)
    membrane.send(membrane_rp)

    with membrane_rp.send_iter_context():
        for node in nodes: membrane_rp.send(node)

    for result in membrane_rp.recv_iter():
        yield result

    membrane.clear()
    membrane_rp.revoke()
    membrane_rp.delete()
    membrane.revoke()
    membrane.delete()

@contextmanager
def aaas_recv_context(p, init_rp):
    """AaaS (Appliance as a Service) Receiver.

    This call is used to receive capabili..."""

    membrane_ext = init_rp.recv_wait()
    membrane_rp = membrane_ext.recv_wait()

    wrapped_caps = list(membrane_rp.recv_iter())
    with membrane_rp.send_iter_context():
        yield (membrane_rp, wrapped_caps)
