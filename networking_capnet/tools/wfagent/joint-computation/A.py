import capnet
import capnet.util
import util

DATA_PROXY_NODE = "A-data-proxy"

def split_nodes(n):
    """Split the nodes into those that are for A_data, and those
    for A_compute"""
    ...
    return (A_data, A_compute)

# Where "p" is the protocol object
def setup(p):
    # get the basic bootstrapping objects
    rp0 = p.rp0()
    me = rp0.recv()
    broker = rp0.recv()

    # Get the nodes + A_data and A_compute
    nodes = []
    for n in rp0.recv_iter_exn():
        nodes.append(n)
    A_data, A_compute = split_nodes(nodes)

    # Wait till B has registered its broker RP, then 
    # it and continue
    b_rp = broker.lookup_wait("B")

    # Get the sealed RP and the update flow from the first step.
    aaas_result_gen = util.aaas_send(p, b_rp, A_data)

    a_seal_unseal = p.create(capnet.SealerUnsealer)

    with util.aaas_recv_context(p, b_rp) as (w_rp, A_data_w):
        nodes = util.build_node_db(p, A_data_w)
        # ... Do any kind of setup actions here
        # ... Add A's data onto the nodes for example

        # Give the proxy node our sealer/unsealer
        nodes[DATA_PROXY_NODE].rp0.send(a_seal_unseal)
        # Get the proxy RP from the proxy node
        sealed_proxy_rp = nodes[DATA_PROXY_NODE].rp0.recv_wait()
        # create a proxy flow 
        data_proxy_flow = nodes[DATA_PROXY_NODE].grant.flow()
        # send back our data-proxy flow and the proxy rp
        w_rp.send(data_proxy_flow)
        w_rp.send(sealed_proxy_rp)

    update_flow, double_sealed_rp = aaas_result_gen 

    # Finished Step 2


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_option("interface")
    args = parser.parse_args()
    p = capnet.Protocol(args.interface)
    setup(p)
