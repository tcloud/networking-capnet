import capnet
import capnet.util

import util

from collections import namedtuple

NodeEntry = namedtuple("NodeEntry", ["node", "info", "rp", "grant"])

def setup(p):
    # get the basic bootstrapping objects
    rp0 = p.rp0()
    me = rp0.recv()
    broker = rp0.recv()

    b_update_pxy, b_compute_pxy = rp0.recv_iter_exn()

    broker_rp = p.create(capnet.RP)
    broker.register("B", broker_rp)

    b_seal_unseal = p.create(capnet.SealerUnsealer)

    with util.aaas_recv_context(p, broker_rp) as (w_rp, A_data):
        update_flow, sealed_rp = util.aaas_send(p, w_rp, A_data)
        # Somehow need to wrap the b_update_proxy
        b_update_pxy_rp0 = p.create(capnet.RP)
        b_update_pxy_grant = b_update_pxy.reset(b_update_pxy_rp0)
        update_flow = b_update_pxy_grant.flow()
        # Run the correct capability agent on b_proxy
        # See: B_data_proxy_for_a.py
        b_update_pxy_rp0.send(b_seal_unseal)
        b_update_pxy_rp0.send(update_flow)
        b_update_pxy_rp0.send(sealed_rp)
        double_sealed_rp = b_update_pxy_rp0.recv_wait()
        # Send the results
        w_rp.send(update_flow)
        w_rp.send(double_sealed_rp)

    # Finished Step 2


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_option("interface")
    args = parser.parse_args()
    p = capnet.Protocol(args.interface)
    setup(p)
