import sys
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import capnet.capnet as cn

Entry = namedtuple("Entry", ["node", "info", "rp", "flow"])

def main():
    nodes = {}

    p = cn.Protocol(sys.argv[1])
    i = 0
    while True:
        try:
            print "getting rp0 (try %d)" % (i,)
            rp0 = p.rp0()
            break
        except:
            print "ERROR: failed to get rp0 in 5 seconds; will keep trying!"
            traceback.print_exc()
            pass
        i += 1
        pass
    
    me = None
    myname = None

    # The wfagent cap comes to us first, after rp0.
    print "rp0 recv me"
    me = rp0.recv()
    print "node info"
    info = me.info()
    print "node flow"
    flow = me.flow()
    node_rp = p.create(cn.RP)
    nodes[info.name] = Entry(me,info,node_rp,flow)
    myname = info.name
    print "Received self cap: %s" % (info.name,)

    print "rp0 recv broker"
    broker = rp0.recv()
    if type(broker) != cn.Broker:
        print "Second cap not a broker (was %s)" % (str(type(broker)),)
        pass
    print "Received broker cap"
    
    # Now receive node caps forever, and grant flows from us to everyone
    # else, and vice versa.
    while True:
        (node,info,flow,name) = (None,None,None,None)
        try:
            print "rp0 recv"
            node = rp0.recv()
            print "node info"
            info = node.info()
            print "node flow"
            flow = node.flow()
            node_rp = p.create(cn.RP)
            node.reset(node_rp)
            name = info.name
            nodes[name] = Entry(node,info,node_rp,flow)
            print "Received new node: %s" % (str(info),)
        except Exception, e:
            print "Exception", e
            time.sleep(4)
            continue
            pass
        
        try:
            a = nodes[name]
            for (bname,b) in nodes.iteritems():
                # Don't give caps to send to us.
                if b.info.name == me:
                    continue

                print "Sending flow ->%s to %s" % (b.info.name,name)
                p.debug_grant(a.node,b.flow)
                #a.rp.send(b.flow)
    
                print "Sending flow ->%s to %s" % (name,b.info.name)
                p.debug_grant(b.node,a.flow)
                #b.rp.send(a.flow)
                pass
        except:
            print "ERROR: while adding node %s into all-pairs mesh:" % (name,)
            traceback.print_exc()
            pass
        
        pass

    pass
