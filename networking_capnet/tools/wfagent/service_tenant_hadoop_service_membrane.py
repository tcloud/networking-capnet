#!/usr/bin/env python

##
## This is the service half of a simple example that uses a brokered
## service tenant to configure flows on behalf of some compute tenant.
## This is the master wfa for the service tenant.  It registers itself
## with the broker as the "hadoop" service, and each time it receives
## a capability to a node, it adds it to an all-pairs flow mesh.  The
## hadoop user tenant master wfa is in user_tenant_hadoop_service.py
##

import sys
import os
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import itertools

import capnet as cn
import capnet.util

ME = "SERVICEHADOOPMEMBRANEWFA"
RECVNODES_TIMEOUT = 60.0

def log(msg):
    print msg
    sys.stdout.flush()
    pass

def ts(obj,id,op,t=None):
    if t is None:
        t = time.time()
    log("%s,TIMESTAMP,%s,%s,%s,%f" % (ME,str(obj),str(id),str(op),t,))
    pass

def main():
    log("args: %s" % (' '.join(sys.argv)))

    nodes = {}
    iface = sys.argv[1]
    broker_name = "hadoop"
    runhadoop = True
    extraargs = []
    if len(sys.argv) > 2:
        for arg in sys.argv[2:]:
            sa = arg.split('=')
            if len(sa) == 2:
                if sa[0] == 'broker_name':
                    broker_name = sa[1]
                    log("Using broker name %s" % (broker_name,))
                elif sa[0] == 'runhadoop':
                    if sa[0] in ('False','false','F','f','0','No','no','N','n'):
                        runhadoop = False
                        log("Will not configure hadoop, only flows!")
                else:
                    log("WARNING: unknown argument %s" % (arg,))
                    pass
            else:
                extraargs.append(arg)
                pass
            pass
        pass
    log("iface = %s, broker_name = %s" % (iface,broker_name))
    
    ts('wfa',0,'start')
    
    ts('proto',0,'start')
    p = cn.Protocol(iface)
    ts('proto',0,'stop')

    rp0, me, broker = capnet.util.recv_preamble(p)
    
    # The RP for the hadoop service
    ts('broker-rp',0,'start')
    service_rp = p.create(cn.RP)
    broker.register(broker_name, service_rp)
    log("Registered hadoop service")
    ts('broker-rp',0,'stop')

    #
    # First receive the membrane from the client
    #

    log("Waiting for membrane...")
    ts('membrane',0,'start')
    membrane_ext = service_rp.recv_wait()
    ts('membrane',0,'stop')
    assert membrane_ext.__class__ == cn.Membrane, "must receive membrane from service rp"

    # Get our "receive" membrane
    trans_rp_r = membrane_ext.recv_wait()
    assert trans_rp_r.__class__ == cn.RP

    # Get our "send" membrane
    trans_rp_s = membrane_ext.recv_wait()
    assert trans_rp_s.__class__ == cn.RP

    def trans_rp_recv_iter_debug_wrapper(it):
        for x in it:
            log("trans rp r iter got: %s" % repr(x))
            yield x

    ts('recvnodes',0,'start')
    _it = trans_rp_recv_iter_debug_wrapper(trans_rp_r.recv_iter())
    nodes = capnet.util.recv_nodes(p, _it)
    master = None
    resourcemanager = None 
    slaves = {}
    for (name, node) in nodes.iteritems():
        if name == 'master':
            master = node
        elif name == 'resourcemanager':
            resourcemanager = node 
        else:
            slaves[name] = node 
    ts('recvnodes',0,'stop')

    ts("allpairs",0,'start')
    ts("master flows",0,'start')
    #for (a, b) in itertools.combinations(nodes.itervalues(), 2):
    for a in nodes.values():
        if a == master:
            continue
        a.grant.grant(master.flow)
        master.grant.grant(a.flow)
        pass
    ts("master flows",0,'stop')
    for a in nodes.values():
        if a == master:
            continue
        for b in nodes.values():
            if a == b or b == master:
                continue
            a.grant.grant(b.flow)
            b.grant.grant(a.flow)
            pass
        pass
    ts("allpairs",0,'stop')

    log("Establishing connectivity to the master")
    ts("wfactlchannel",0,'start')
    master.grant.grant(me.flow)
    ts("wfactlchannel",0,'stop')
    log("Finished establishing connectivity to the master")
    
    ts('hadoop-setup',0,'start')
    if not runhadoop:
        log("Not setting up Hadoop, on command.")
    else:
        log("Assuming all nodes have been granted to us; setting up hadoop!")
        args = ""
        pargs = ""
        log("master = %s %s %s" % (master.info.name,master.info.ipv4,
                                   master.info.mac))
        args += " %s" % (master.info.ipv4,)
        log("resourcemanager = %s %s %s" % (resourcemanager.info.name,
                                            resourcemanager.info.ipv4,
                                            resourcemanager.info.mac))
        args += " %s" % (resourcemanager.info.ipv4,)
        # The IP addrs must be ordered.  We can get away with this because of
        # our naming scheme.
        for i in range(1,len(slaves)+1):
            slavename = "slave-%d" % (i,)
            slave = slaves[slavename]
            log("slave = %s %s %s" % (slave.info.name, slave.info.ipv4,
                                      slave.info.mac))
            args += " %s" % (slave.info.ipv4,)
            pargs += " -H %s" % (slave.info.ipv4,)
            pass

        user = 'ubuntu'

        # First, check to see that all slave nodes can be reached via ssh.
        checkcmd = "ssh -o StrictHostKeyChecking=no %s@%s parallel-ssh -i -O StrictHostKeyChecking=no -t 5 %s 'ls >& /dev/null'" % (user,master.info.ipv4,pargs)
        retries = 8
        success = False
        while retries > 0:
            status = os.system(checkcmd)
            if status == 0:
                success = True
                break
            else:
                log("WARNING: could not reach all slave nodes (trying %d more times)" % (retries-1,))
                time.sleep(5)
            retries -= 1
            pass
        if not success:
            log("ERROR: could not reach all slave nodes via ssh!")
            pass

        mastercmd = "/home/ubuntu/setup.sh"
        cmd = "ssh -o StrictHostKeyChecking=no %s@%s %s %s" % (user,master.info.ipv4,mastercmd,args)
        status = 1
        retries = 3
        retryinterval = 4
        i = 1
        while status != 0 and i < retries:
            log("Running '%s'... (%d)" % (cmd,i))
            i += 1
            status = os.system(cmd)
            if status == 0:
                break
            else:
                time.sleep(retryinterval)
            pass
        log("Finished '%s'... (%d, status %d)" % (cmd,i,status))
        ts('hadoop-setup',0,'stop')
        pass

    #
    # Ok, now that hadoop is done, send all the flow caps we created
    # back through the membrane, and exit.
    #
    log("master cap back through membrane...")

    ts('mastersendback',0,'start')
    trans_rp_s.send(master.grant);
    ts('mastersendback',0,'stop')
    
    ts('wfa',0,'stop')
    log("Finished setting up Hadoop and its network!")

if __name__ == "__main__":
    main()
