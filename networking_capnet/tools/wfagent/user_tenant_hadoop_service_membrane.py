#!/usr/bin/env python

##
## This is one half of a simple example that uses a brokered service
## tenant to configure flows on behalf of some compute tenant.  This is
## the master wfa for the compute tenant.  It looks up the "hadoop"
## service, and each time it receives a capability to a node it owns, it
## passes it to the hadoop service tenant.  The hadoop service
## tenant master wfa is in service_tenant_hadoop_service.py .
##

import os
import sys
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import capnet as cn
import capnet.util

RECVNODES_TIMEOUT = 30.0

ME = "USERHADOOPMEMBRANEWFA"

def log(msg):
    print msg
    sys.stdout.flush()
    pass

def ts(obj,id,op,t=None):
    if t is None:
        t = time.time()
    log("%s,TIMESTAMP,%s,%s,%s,%f" % (ME,str(obj),str(id),str(op),t,))

def main():
    log("args: %s" % (' '.join(sys.argv)))

    iface = sys.argv[1]
    broker_name = "hadoop"
    runhadoop = True
    extraargs = []
    if len(sys.argv) > 2:
        for arg in sys.argv[2:]:
            sa = arg.split('=')
            if len(sa) == 2:
                if sa[0] == 'broker_name':
                    broker_name = sa[1]
                    log("Using broker name %s" % (broker_name,))
                elif sa[0] == 'runhadoop':
                    if sa[0] in ('False','false','F','f','0','No','no','N','n'):
                        runhadoop = False
                        log("Will not run hadoop!")
                else:
                    log("WARNING: unknown argument %s" % (arg,))
                    pass
            else:
                extraargs.append(arg)
                pass
            pass
        pass
    log("iface = %s, broker_name = %s" % (iface,broker_name))

    ts('wfa',0,'start')

    ts('proto',0,'start')
    p = cn.Protocol(iface)
    ts('proto',0,'stop')

    rp0, me, broker = capnet.util.recv_preamble(p)
    
    # The Membrane we send caps through to the hadoop service
    log("setting up membrane")
    ts('membrane-create',0,'start')
    membrane_int = p.create(cn.Membrane)
    membrane_ext = membrane_int.external()
    assert membrane_int.cptr != membrane_ext.cptr, "should be different cptrs"
    ts('membrane-create',0,'stop')
    
    caps_from_membrane = []

    ts('recvnodes',0,'start')
    nodes = capnet.util.recv_nodes(p, rp0.recv_iter_timeout(RECVNODES_TIMEOUT))
    ts('recvnodes',0,'stop')
    
    ts('service-rp',0,'start')
    service_rp = broker.lookup_wait(broker_name)
    ts('service-rp',0,'stop')

    ts('trans-rp',0,'start')

    # Send membrane
    service_rp.send(membrane_ext)

    #create trans RPs
    trans_rp_s = p.create(cn.RP)
    trans_rp_r = p.create(cn.RP)

    # Send the transaction RPs
    membrane_int.send(trans_rp_s)
    membrane_int.send(trans_rp_r)
    ts('trans-rp',0,'stop')
    
    log("Found service hadoop, sending it %d nodes" % (len(nodes.keys()),))

    ts('service-node-send',0,'start')
    with trans_rp_s.send_iter_context():
        for (name, node) in nodes.iteritems():
            trans_rp_s.send(node.node)
    ts('service-node-send',0,'stop')
    
    ts('trans-recv',0,'start')
    master_grant = trans_rp_r.recv_wait()
    log("Received master_grant back from membrane: %s" % (str(master_grant)))
    ts('trans-recv',0,'stop')
    
    # Ok, cut the membrane!
    log("Hadoop seems done; cutting membrane!")
    ts('membrane-clear',0,'start')
    membrane_int.clear()
    ts('membrane-clear',0,'stop')
    log("Finished cutting membrane!")

    log("Establishing connectivity to the master")
    master_flow = master_grant.flow()
    master_grant.grant(me.flow)
    log("Finished establishing connectivity to the master")
    
    # Ok, at this point, we can run some hadoop job!

    ts('hadoop-job-run',0,'start')
    if not runhadoop:
        log("Not running Hadoop, on command.")
    else:
        user = 'ubuntu'
        mastercmd = "/home/ubuntu/hadoop-test-wordcount.sh"
        if len(extraargs):
            mastercmd += " " + " ".join(extraargs)
        else:
            # By default, do a size of 128 * nslaves * 1024 * 1024
            size = (len(nodes.keys()) - 3) * 128 * 1024 * 1024
            mastercmd += " %d" % (size,)

        master = nodes['master']
        cmd = "ssh -o StrictHostKeyChecking=no %s@%s %s %s" % (user,master.info.ipv4,mastercmd,' '.join(extraargs))
        log("Running '%s'..." % (cmd,))
        os.system(cmd)
        log("Finished '%s'..." % (cmd,))
        pass
    ts('hadoop-job-run',0,'stop')
    
    ts('wfa',0,'stop')

    log("DONE")
