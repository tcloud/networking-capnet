#!/usr/bin/env python

##
## This is one half of a simple example that uses a brokered service
## tenant to configure flows on behalf of some compute tenant.  This is
## the master wfa for the compute tenant.  It looks up the "allpairs"
## service, and each time it receives a capability to a node it owns, it
## passes it to the allpairs service tenant.  The allpairs service
## tenant master wfa is in service_tenant_allpairs_service.py .
##

import sys
import time
#import argparse
#from pprint import pprint
from collections import namedtuple
import traceback

import capnet.capnet as cn

Entry = namedtuple("Entry", ["node", "info", "rp", "flow"])

def main():
    nodes = {}

    p = cn.Protocol(sys.argv[1])
    i = 0
    while True:
        try:
            print "getting rp0 (try %d)" % (i,)
            rp0 = p.rp0()
            break
        except:
            print "ERROR: failed to get rp0 in 5 seconds; will keep trying!"
            traceback.print_exc()
            pass
        i += 1
        pass
    
    me = None
    myname = None

    # The wfagent cap comes to us first, after rp0.
    print "rp0 recv me"
    me = rp0.recv()
    print "node info"
    info = me.info()
    print "node flow"
    flow = me.flow()
    node_rp = p.create(cn.RP)
    nodes[info.name] = Entry(me,info,node_rp,flow)
    myname = info.name
    print "Received self cap: %s" % (info.name,)

    print "rp0 recv broker"
    broker = rp0.recv()
    if type(broker) != cn.Broker:
        print "Second cap not a broker (was %s)" % (str(type(broker)),)
        pass
    print "Received broker cap"
    
    # The RP for the allpairs service
    service_rp = None
    
    # Now receive node caps forever, and grant flows from us to everyone
    # else, and vice versa.
    while True:
        (node,info,flow,name) = (None,None,None,None)
        try:
            print "rp0 recv"
            node = rp0.recv()
            print "node info"
            info = node.info()
            print "node flow"
            flow = node.flow()
            node_rp = p.create(cn.RP)
            node.reset(node_rp)
            name = info.name
            nodes[name] = Entry(node,info,node_rp,flow)
            print "Received new node: %s" % (str(info),)
        except Exception, e:
            (node,info,flow,node_rp) = (None,None,None,None)
            print "Exception", e
            pass
        
        if not service_rp:
            try:
                service_rp = broker.lookup("allpairs")
            
                print "Found service allpairs, sending it %d nodes" \
                    % (len(nodes.keys()),)
            
                # Send all the nodes we've received so far!
                for (name,entry) in nodes.iteritems():
                    # Except don't send our wfa!
                    if entry.node == me:
                        continue
                    else:
                        service_rp.send(entry.node)
                        pass
                    pass
                pass
            except Exception, e:
                print "Exception: ",e
                pass
            pass
        elif node:
            try:
                print "Sending cap to %s to allpairs service" % (info.name,)
                service_rp.send(node)
                print "Sent cap to %s to allpairs service" % (info.name,)
            except Exception, e:
                print "Exception: ",e
                pass
            pass

        sys.stdout.flush()
        sys.stderr.flush()
        time.sleep(2)
        
        pass

    pass
