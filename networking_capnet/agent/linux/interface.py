# Copyright 2012 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import abc

import netaddr
from oslo_config import cfg
from oslo_log import log as logging
import six

from neutron.agent.linux import interface as n_interface
from neutron.agent.common import ovs_lib
from neutron.agent.linux import ip_lib
from neutron.agent.linux import utils
from neutron.common import constants as n_const
from neutron.common import exceptions
from neutron.common import ipv6_utils
from neutron.i18n import _LE, _LI
from neutron.common import utils as n_utils

#import keystone.common.config as k_config
#from keystonemiddleware import auth_token
from keystoneclient.auth.identity import v2 as v2_auth
from keystoneclient.auth.identity import v3 as v3_auth
from keystoneclient import discover
from keystoneclient.openstack.common.apiclient import exceptions as ks_exc
from keystoneclient import session
from neutronclient.v2_0 import client
from neutronclient.common import clientmanager

from networking_capnet.common import config as cn_config
from networking_capnet.common import capnetneutronclient as cneutronclient

LOG = logging.getLogger(__name__)

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')
cneutronclient.register_opts_for_neutron_conf(cfg.CONF)

# neutron.agent.linux.interface.* requires these opts
cfg.CONF.register_opts(n_interface.OPTS)

class CapnetOVSInterfaceDriver(n_interface.OVSInterfaceDriver):
    """
    Driver for creating an internal interface on an OVS bridge.
    
    If this is a capnet network, we figure out which OVS bridge to plug
    the port into, instead of assuming the integration bridge.  We do
    this by contacting the neutron server and querying the network
    
    Backwards-compat with the OVSInterfaceDriver... it's just an overlay.
    """

    def __init__(self, conf):
        super(CapnetOVSInterfaceDriver, self).__init__(conf)
        self.bridge_mappings = {}
        # network id to OVS bridge name
        self.network_id_to_br = {}
        # network id to port id list
        self.network_ports = {}
        # device_name to bridge name
        self.device_bridges = {}
        # A CapnetNeutronClient
        self._client = None
        
        #LOG.debug("conf keys: %s" % (str(self.conf.keys())))
        
        try:
            self.bridge_mappings = \
                n_utils.parse_mappings(cfg.CONF.CAPNET.bridge_mappings)
            LOG.info("Initialized Capnet bridge_mappings: %s"
                     % (str(self.bridge_mappings)))
            
            self._client = cneutronclient.get_client_for_neutron_conf()
            LOG.info("Initialized Neutron client for Capnet")
            
            client = self._client.get_client()
            networks = client.list_networks()
            LOG.info("Current networks: %s" % (str(networks)))
            
            if networks and networks.has_key('networks') \
              and len(networks['networks']) > 0:
                dev_prefix = n_interface.OVSInterfaceDriver.DEV_NAME_PREFIX
                # Go through all our physical bridges, finding
                # interfaces named with the superclass's prefix, and add
                # all that junk to our cache.
                for network in networks['networks']:
                    if network.has_key('provider:physical_network') \
                      and self.bridge_mappings.has_key(network['provider:physical_network']):
                        network_id = network['id']
                        physical_network = network['provider:physical_network']
                        bridge_name = self.bridge_mappings[physical_network]
                        br = ovs_lib.OVSBridge(bridge_name)
                        ports = br.get_vif_ports()
                        for port in ports:
                            if port.port_name.startswith(dev_prefix):
                                self._cache_capnet_network_port(
                                    network_id,port.vif_id,port.port_name,
                                    bridge_name)
                                pass
                            pass
                        pass
                    pass
                pass
            LOG.info("Capnet state: network_id_to_br = %s"
                     % (str(self.network_id_to_br)))
            LOG.info("Capnet state: network_ports = %s"
                     % (str(self.network_ports)))
            LOG.info("Capnet state: device_bridges = %s"
                     % (str(self.device_bridges)))
            pass
        except Exception:
            LOG.exception("Failed to initialize Capnet Neutron Interface Driver;"
                          " will effectively revert to openvswitch driver")
            self.bridge_mappings = {}
            self.network_id_to_br = {}
            self.network_ports = {}
            self.device_bridges = {}
            self._client = None
            pass
        
        pass

    def _check_network_for_capnet(self, network_id):
        if len(self.bridge_mappings) == 0:
            LOG.debug("Could not check network_id %s to see if it is Capnet;"
                      " no bridge_mappings specified in configuration!"
                      % (network_id,))
            return None
        
        try:
            client = self._client.get_client()
            resp = client.show_network(network_id)
            if resp and resp.has_key('network') \
              and resp['network'].has_key('provider:physical_network'):
                physical_network = resp['network']['provider:physical_network']
                
                if self.bridge_mappings.has_key(physical_network):
                    # Ok, this is a Capnet network...
                    bridge_name = self.bridge_mappings[physical_network]
                    return bridge_name
                pass
            pass
        except Exception:
            LOG.exception("Could not check network_id %s to see if it is Capnet"
                          % (network_id,))
            return None
            pass
        
        # Didn't find anything...
        return None
    
    def _cache_capnet_network_port(self, network_id, port_id, device_name,
                                   bridge_name):
        # ...so enable our cache
        self.network_id_to_br[network_id] = bridge_name
        if not self.network_ports.has_key(network_id):
            self.network_ports[network_id] = [port_id]
        else:
            self.network_ports[network_id].append(port_id)
        self.device_bridges[device_name] = (bridge_name,network_id,port_id)
        
        LOG.debug("cached Capnet device %s on bridge %s (net %s, port %s)"
                  % (device_name,bridge_name,network_id,port_id))
        pass
    
    def _capnet_lookup_device(self, device_name):
        if self.device_bridges.has_key(device_name):
            # Clean out the cache as much as possible.
            # If we have no more ports for a given network, we bump it
            # entirely out of our cache.
            (bridge_name,network_id,port_id) = self.device_bridges[device_name]
        else:
            (bridge_name,network_id,port_id) = (None,None,None)
            pass
        return (bridge_name,network_id,port_id)
    
    def _uncache_capnet_network_port(self,device_name,bridge,
                                     network_id,port_id):
        if bridge and network_id and port_id:
            del self.device_bridges[device_name]
            self.network_ports[network_id].remove(port_id)
            if len(self.network_ports[network_id]) == 0:
                del self.network_ports[network_id]
                del self.network_id_to_br[network_id]
                pass
            pass

    def plug_new(self, network_id, port_id, device_name, mac_address,
                 bridge=None, namespace=None, prefix=None):
        """
        Check with the Neutron server to find the physical_network
        corresponding to network_id.  Then, see if that is a capnet
        physical_network, and if so, which bridge it maps to.  Ugh.
        """
        did_capnet_search = False
        if not bridge:
            did_capnet_search = True
            bridge = self._check_network_for_capnet(network_id)
            if bridge:
                LOG.debug("found bridge %s for network %s"
                          % (bridge,network_id))
            pass
        
        retval = super(CapnetOVSInterfaceDriver,self).plug_new(
            network_id,port_id,device_name,mac_address,
            bridge=bridge,namespace=namespace,prefix=prefix)
        
        if did_capnet_search and bridge:
            self._cache_capnet_network_port(network_id,port_id,device_name,bridge)
            pass
        
        return retval
        

    def unplug(self, device_name, bridge=None, namespace=None, prefix=None):
        """
        This time, we have to check all the Capnet bridges for a port named
        device_name; if we find one, supply that bridge to our superclass.
        """
        if not bridge:
            (bridge,network_id,port_id) = \
                self._capnet_lookup_device(device_name)
            pass
        
        retval = super(CapnetOVSInterfaceDriver,self).unplug(
            device_name,bridge=bridge,namespace=namespace,prefix=prefix)

        if bridge and network_id and port_id:
            self._uncache_capnet_network_port(device_name,bridge,
                                              network_id,port_id)
            pass
        
        return retval

    pass
