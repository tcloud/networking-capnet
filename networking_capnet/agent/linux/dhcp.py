from oslo_config import cfg
from oslo_log import log as logging

from neutron.agent.linux.dhcp import Dnsmasq
from networking_capnet.common import config as cn_config
from networking_capnet.common import constants as cn_const

LOG = logging.getLogger(__name__)

class CapnetDnsmasq(Dnsmasq):
    """
    A simple Dnsmasq wrapper that substitutes a separate Capnet dnsmasq,
    and strips out any --server options from the default config.  Capnet
    networks cannot access external world, so we cannot stall DNS
    queries based on dnsmasq trying to forward the query onto the
    default resolver.
    """

    def _build_cmdline_callback(self, pid_file):
        cmd = super(CapnetDnsmasq,self)._build_cmdline_callback(pid_file)
        LOG.debug("network = %s" % (str(self.network),))
        ccf = cfg.CONF.CAPNET.dnsmasq_config_file
        LOG.debug("CAPNET.dnsmasq_config_file = %s" % (ccf,))
        if 'provider:network_type' in self.network \
          and self.network['provider:network_type'] == cn_const.TYPE_CAPNET:
            # We need to change the config file to point to ours:
            if ccf:
                found = False
                for i in range(0,len(cmd)):
                    if cmd[i].startswith('--conf-file='):
                        cmd[i] = "--conf-file=%s" % (ccf,)
                        found = True
                        break
                    pass
                if not found:
                    cmd.append("--conf-file=%s" % (ccf,))
                    pass
                pass
            # If there is a --server option, we have to get rid of it!
            # Capnet does not allow external access right now.
            newcmd = []
            for c in cmd:
                if c.startswith('--server='):
                    LOG.warn("removed dnsmasq opt %s !" % (c,))
                else:
                    newcmd.append(c)
                    pass
                pass
            cmd = newcmd
            pass

        return cmd
    pass
