
import argparse

from neutronclient.common import extension
from neutronclient.i18n import _
from neutronclient.neutron import v2_0 as neutronV20


class CapnetPolicyExt(extension.NeutronClientExtension):
    resource = "capnet_policy"
    resource_plural = "capnet_policies"
    object_path = '/%s' % resource_plural
    resource_path = '/%s/%%s' % resource_plural
    versions = ['2.0']
    allow_names = False
    pass

class CapnetPolicyList(extension.ClientExtensionList,CapnetPolicyExt):
    shell_command = 'capnet-policy-list'
    list_columns = ['id','network_id','subnet_id','tenant_id',
                    'wfapp_policy','wfapp_path','wfapp_args','wfapp_env']
    pagination_support = True
    sorting_support = True

    def add_known_arguments(self, parser):
        parser.add_argument(
            '--subnet-id',metavar="SUBNET_ID",dest='subnet_id',
            help=_('Specify the subnet name or ID this policy covers.'))
        parser.add_argument(
            '--network_id', metavar='NETWORK',dest='network_id',
            help=_('Network ID or name this policy belongs to.'))
        pass

    def args2body(self, parsed_args):
        client = self.get_client()
        if parsed_args.network_id:
            _network_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'network', parsed_args.network_id)
            body = { 'network_id': _network_id }
            pass
        if parsed_args.subnet_id:
            _subnet_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'subnet', parsed_args.subnet_id)
            body['subnet_id'] = _subnet_id
            pass
        if parsed_args.tenant_id:
            body['tenant_id'] = parsed_args.tenant_id
        if parsed_args.id:
            body['id'] = parsed_args.id
        
        return body
    pass

class CapnetPolicyCreate(extension.ClientExtensionCreate,CapnetPolicyExt):
    shell_command = 'capnet-policy-create'
    list_columns = ['id','network_id','subnet_id','tenant_id',
                    'wfapp_policy','wfapp_path','wfapp_args','wfapp_env']

    def add_known_arguments(self, parser):
        parser.add_argument(
            '--subnet-id',metavar="SUBNET_ID",dest='subnet_id',
            help=_('Specify the subnet name or ID this policy covers.'))
        parser.add_argument(
            '--wfapp-policy',dest='wfapp_policy',
            help=_('Specify the primary workflow policy (either \'default\' or \'explicit\').'))
        parser.add_argument(
            '--wfapp-path',dest='wfapp_path',
            help=_('Specify the primary workflow app.'))
        parser.add_argument(
            '--wfapp-args',dest='wfapp_args',
            help=_('Specify arguments to the primary workflow app.'))
        parser.add_argument(
            '--wfapp-env',dest='wfapp_env',
            help=_('Specify the workflow app\'s environment (\'netns\' is currently the only option).'))
        parser.add_argument(
            'network_id', metavar='NETWORK',
            help=_('Network ID or name this policy belongs to.'))
        pass

    def args2body(self, parsed_args):
        client = self.get_client()
        _network_id = neutronV20.find_resourceid_by_name_or_id(
            client, 'network', parsed_args.network_id)
        body = { 'network_id': _network_id }
        if parsed_args.subnet_id:
            _subnet_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'subnet', parsed_args.subnet_id)
            body['subnet_id'] = _subnet_id
            pass
        if parsed_args.tenant_id:
            body['tenant_id'] = parsed_args.tenant_id
        if parsed_args.wfapp_policy:
            body['wfapp_policy'] = parsed_args.wfapp_policy
        if parsed_args.wfapp_path:
            body['wfapp_path'] = parsed_args.wfapp_path
        if parsed_args.wfapp_args:
            body['wfapp_args'] = parsed_args.wfapp_args
        if parsed_args.wfapp_env:
            body['wfapp_env'] = parsed_args.wfapp_env
        
        body = { 'capnet_policy': body }

        return body
    
    pass

#class CapnetPolicyUpdate(extension.ClientExtensionUpdate,CapnetPolicyExt):
#
#    shell_command = 'capnet-policy-update'
#    
#    pass

class CapnetPolicyDelete(extension.ClientExtensionDelete,CapnetPolicyExt):
    shell_command = 'capnet-policy-delete'
    pass

class CapnetPolicyShow(extension.ClientExtensionShow,CapnetPolicyExt):
    shell_command = 'capnet-policy-show'
    pass


class CapnetWorkflowAgentExt(extension.NeutronClientExtension):
    resource = "capnet_workflow_agent"
    resource_plural = "%ss" % resource
    object_path = '/%s' % resource_plural
    resource_path = '/%s/%%s' % resource_plural
    versions = ['2.0']
    pass

class CapnetWorkflowAgentList(extension.ClientExtensionList,
                              CapnetWorkflowAgentExt):
    shell_command = 'capnet-wfagent-list'
    list_columns = ['id','name','network_id','subnet_id','tenant_id','port_id',
                    'master','wfapp_path','wfapp_args','wfapp_env','status']
    pagination_support = True
    sorting_support = True
    allow_names = True

    def add_known_arguments(self, parser):
        parser.add_argument(
            '--subnet-id',metavar="SUBNET_ID",dest='subnet_id',
            help=_('Specify the subnet name or ID this policy covers.'))
        parser.add_argument(
            '--network_id', metavar='NETWORK',dest='network_id',
            help=_('Network ID or name this policy belongs to.'))
        parser.add_argument(
            '--port_id', metavar='PORT',dest='port_id',
            help=_('Port ID this policy belongs to.'))
        pass

    def args2body(self, parsed_args):
        client = self.get_client()
        if parsed_args.network_id:
            _network_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'network', parsed_args.network_id)
            body = { 'network_id': _network_id }
            pass
        if parsed_args.subnet_id:
            _subnet_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'subnet', parsed_args.subnet_id)
            body['subnet_id'] = _subnet_id
            pass
        if parsed_args.port_id:
            body['port_id'] = _port_id
            pass
        if parsed_args.tenant_id:
            body['tenant_id'] = parsed_args.tenant_id
        if parsed_args.id:
            body['id'] = parsed_args.id
        if parsed_args.name:
            body['name'] = parsed_args.name
        return body
    pass

class CapnetWorkflowAgentCreate(extension.ClientExtensionCreate,
                                CapnetWorkflowAgentExt):
    shell_command = 'capnet-wfagent-create'
    list_columns = ['id','network_id','subnet_id','tenant_id','port_id',
                    'master','wfapp_path','wfapp_args','wfapp_env','status']

    def add_known_arguments(self, parser):
        parser.add_argument(
            '--name',metavar="NAME",dest='name',
            help=_('Specify a name for this workflow agent.  Must be unique per network.'))
        parser.add_argument(
            '--subnet-id',metavar="SUBNET",dest='subnet_id',
            help=_('Specify the subnet this policy covers.'))
        parser.add_argument(
            '--port-id',metavar="PORT",dest='port_id',
            help=_('Specify the port id or name this policy covers.'))
        parser.add_argument(
            '--master',dest='master',action='store_true',
            help=_('Specify the primary workflow app.'))
        parser.add_argument(
            '--wfapp-path',dest='wfapp_path',
            help=_('Specify the primary workflow app.'))
        parser.add_argument(
            '--wfapp-args',dest='wfapp_args',
            help=_('Specify arguments to the primary workflow app.'))
        parser.add_argument(
            '--wfapp-env',dest='wfapp_env',
            help=_('Specify the workflow app\'s environment (\'netns\' is currently the only option).'))
        parser.add_argument(
            'network_id', metavar='NETWORK',
            help=_('Network ID or name this policy belongs to.'))
        pass

    def args2body(self, parsed_args):
        if parsed_args.port_id and (parsed_args.wfapp_path
                                    or parsed_args.wfapp_env):
            raise Exception("cannot both bind existing port, and provide"
                            " wfapp path or env!")
        
        client = self.get_client()
        _network_id = neutronV20.find_resourceid_by_name_or_id(
            client, 'network', parsed_args.network_id)
        body = { 'network_id': _network_id }
        if parsed_args.subnet_id:
            _subnet_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'subnet', parsed_args.subnet_id)
            body['subnet_id'] = _subnet_id
            pass
        if parsed_args.port_id:
            _subnet_id = neutronV20.find_resourceid_by_name_or_id(
                client, 'port', parsed_args.port_id)
            body['port_id'] = _port_id
            pass
        if parsed_args.tenant_id:
            body['tenant_id'] = parsed_args.tenant_id
        if parsed_args.name:
            body['name'] = parsed_args.name
        if parsed_args.master:
            body['master'] = parsed_args.master
        if parsed_args.wfapp_path:
            body['wfapp_path'] = parsed_args.wfapp_path
        if parsed_args.wfapp_args:
            body['wfapp_args'] = parsed_args.wfapp_args
        if parsed_args.wfapp_env:
            body['wfapp_env'] = parsed_args.wfapp_env
        
        body = { 'capnet_workflow_agent': body }

        return body
    
    pass

#class WorkflowAgentUpdate(extension.ClientExtensionUpdate,WorkflowAgent):
#
#    shell_command = 'capnet-wfagent-update'
#
#    pass

class CapnetWorkflowAgentDelete(extension.ClientExtensionDelete,
                          CapnetWorkflowAgentExt):
    shell_command = 'capnet-wfagent-delete'
    allow_names = True
    pass

class CapnetWorkflowAgentShow(extension.ClientExtensionShow,
                              CapnetWorkflowAgentExt):
    shell_command = 'capnet-wfagent-show'
    allow_names = True
    pass
