
import itertools

from neutron.common import topics


def add_consumers(connection, endpoints, prefix, topic_details, node_only=False):
    """
    Add agent RPC consumers to an existing connection.  Derived from
    neutron.agent.rpc:create_consumers.

    :param endpoints: The list of endpoints to process the incoming messages.
    :param prefix: Common prefix for the plugin/agent message queues.
    :param topic_details: A list of topics. Each topic has a name, an
                          operation, and an optional host param keying the
                          subscription to topic.host for plugin calls.
    :param node_only: Set to True if you only want the host-specific topics
                      to be created.

    :returns: None
    """

    for details in topic_details:
        topic, operation, node_name = itertools.islice(
            itertools.chain(details, [None]), 3)

        topic_name = topics.get_topic_name(prefix, topic, operation)
        if not node_only:
            connection.create_consumer(topic_name, endpoints, fanout=True)
        if node_name:
            node_topic_name = '%s.%s' % (topic_name, node_name)
            connection.create_consumer(node_topic_name,
                                       endpoints,
                                       fanout=False)
    return None
