#!/usr/bin/env python
# Copyright 2016 University of Utah
# Copyright 2012 Cisco Systems, Inc.
# Copyright 2011 VMware, Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import collections
import hashlib
import signal
import sys
import time
import uuid
import os
import shutil
import signal
import pprint

import eventlet
eventlet.monkey_patch()

import functools
import netaddr
import six
from oslo_config import cfg
from oslo_log import log as logging
from oslo_concurrency import lockutils
from oslo_service import loopingcall
from oslo_service import service
import oslo_messaging

from neutron.common import rpc as n_rpc
from neutron.agent.common import ovs_lib
from neutron.agent.common import polling
from neutron.agent.common import utils as agent_utils
from neutron.agent.common import config as agent_config
from neutron.agent.linux import ip_lib
from neutron.agent import rpc as agent_rpc
from neutron.common import config as common_config
from neutron.common import constants as n_const
from neutron.common import exceptions
from neutron.common import ipv6_utils
from neutron.common import topics
from neutron.common import utils as n_utils
from neutron import context
from neutron.i18n import _LE, _LI, _LW
from neutron.plugins.common import constants as p_const
from neutron.plugins.ml2.drivers.openvswitch.agent.common \
    import constants as ovs_const
from neutron.agent.linux import external_process

from networking_capnet.common import constants as cn_const
from networking_capnet.common import topics as cn_topics
from networking_capnet.plugins.ml2.drivers.capnet import util as cn_util
from networking_capnet.api.rpc import util as cn_rpc_util

LOG = logging.getLogger(__name__)

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')
cfg.CONF.import_group('AGENT','networking_capnet.plugins.ml2.drivers.capnet.agent_config')
agent_config.register_interface_driver_opts_helper(cfg.CONF)

MAX_DEVICE_RETRIES = 5
# Represent ovs status
OVS_RESTARTED = 0
OVS_NORMAL = 1
OVS_DEAD = 2

UINT64_BITMASK = (1 << 64) - 1

def has_zero_prefixlen_address(ip_addresses):
    return any(netaddr.IPNetwork(ip).prefixlen == 0 for ip in ip_addresses)

class _mac_mydialect(netaddr.mac_unix):
    word_fmt = '%.2x'

class DeviceListRetrievalError(exceptions.NeutronException):
    message = _("Unable to retrieve port details for devices: %(devices)s ")
    
class NonVifPort(object):
    def __init__(self,port_name,ofport,iface_id,mac,switch):
        self.port_name = port_name
        self.ofport = ofport
        self.iface_id = iface_id
        self.mac = mac
        self.switch = switch
        pass
    
    def __str__(self):
        return ("iface-id=" + self.iface_id + ", mac=" + self.mac +
                ", port_name=" + self.port_name + ", ofport=" + str(self.ofport) +
                ", bridge_name=" + self.switch.br_name)
    
    pass
    
class ExtOVSBridge(ovs_lib.OVSBridge):
    # Returns a NonVifPort object for each non-virtual port (i.e., physical
    # ports, patches, etc).
    def get_non_vif_ports(self):
        edge_ports = []
        port_info = self.get_ports_attributes(
            'Interface', columns=['_uuid','name','external_ids','ofport',
                                  'mac_in_use'],
            if_exists=True)
        
        for port in port_info:
            name = port['name']
            external_ids = port['external_ids']
            ofport = port['ofport']
            mac = port['mac_in_use']
            if ("iface-id" in external_ids and "attached-mac" in external_ids) \
              or ("xs-vif-uuid" in external_ids and
                  "attached-mac" in external_ids):
                continue
            else:
                nvp = NonVifPort(name,ofport,str(port['_uuid']),mac,self)
                edge_ports.append(nvp)
            pass
        
        return edge_ports

    pass

class CapnetSegment(object):
    def __init__(self, network_type, physical_network, segmentation_id,
                 arp_responder=None,auto_dhcp_flows=None,auto_metadata_flows=None,
                 wfapp_path=None,wfapp_args=None,wfapp_env=None):
        self.network_type = network_type
        self.physical_network = physical_network
        self.segmentation_id = segmentation_id
        # XXX: for now, just use the segmentation id as the local offset
        # to keep things simple between main agent and 
        self.local_port_offset = segmentation_id
        
        # Set our defaults.  If we don't get any input values, we take them from
        # the config file.
        if arp_responder is not None:
            self.arp_responder = arp_responder
        else:
            self.arp_responder = cfg.CONF.CAPNET.default_arp_responder
        if auto_dhcp_flows is not None:
            self.auto_dhcp_flows = auto_dhcp_flows
        else:
            self.auto_dhcp_flows = cfg.CONF.CAPNET.default_dhcp_flows
        if auto_metadata_flows is not None:
            self.auto_metadata_flows = auto_metadata_flows
        else:
            self.auto_metadata_flows = cfg.CONF.CAPNET.default_metadata_flows
        if wfapp_path is not None:
            self.wfapp_path = wfapp_path
        else:
            self.wfapp_path = cfg.CONF.CAPNET.default_wfapp_path
        self.wfapp_args = wfapp_args
        if wfapp_env is not None:
            self.wfapp_env = wfapp_env
        else:
            self.wfapp_env = cfg.CONF.CAPNET.default_wfapp_env

    def __str__(self):
        return ("type = %s phys-net = %s phys-id = %s port-offset %d" %
                (self.network_type, self.physical_network, self.segmentation_id,
                 self.local_port_offset))
    pass


class CapnetPluginApi(agent_rpc.PluginApi):
    """
    This is the regular part of the plugin-agent RPC, where the agent
    gets notified of network/subnet/port CRUD.
    """
    pass

class CapnetExtPluginApi(object):
    """
    Agent side of the Capnet extended rpc API (workflow agents, notifications).

    This class implements the client side of an rpc interface.  The
    server side of this interface can be found in
    neutron.api.rpc.handlers.capnet_rpc.CapnetExtRpcCallbacks.  For more
    information about changing rpc interfaces, see
    doc/source/devref/rpc_api.rst.

    API version history:
        1.0 - Initial version.
    """

    def __init__(self, topic, context, use_namespaces, host):
        self.context = context
        self.host = host
        self.use_namespaces = use_namespaces
        target = oslo_messaging.Target(
                topic=topic,
                namespace=cn_const.RPC_NAMESPACE_CAPNET_PLUGIN,
                version='1.0')
        self.client = n_rpc.get_client(target)
        
    def hello_world(self):
        cctxt = self.client.prepare(version='1.0')
        retval = cctxt.call(self.context,'hello_world',host=self.host)
        return retval

    def capnet_get_network_info(self,context,network_id=None,host=None):
        """
        Retrieve and return full information about one or all Capnet
        networks.  This method returns *all* Capnet network and subnet
        info, whether or not a host filter kwarg is specified.  If a
        host filter kwarg is specified, though, it only returns ports
        bound to this host.
        """
        cctxt = self.client.prepare(version='1.0')
        networks = cctxt.call(self.context,'capnet_get_network_info',
                              network_id=network_id,host=host)
        if networks:
            retval = []
            for network in networks:
                retval.append(cn_util.DictModel(network))
                pass
            return retval
        else:
            return None
        pass
    
    def capnet_get_port_bindings(self):
        """Get the list of all Capnet port bindings"""
        cctxt = self.client.prepare(version='1.0')
        bindings = cctxt.call(self.context,'capnet_get_port_bindings')
        if bindings:
            retval = []
            for b in bindings:
                retval.append(cn_util.DictModel(b))
                pass
            return retval
        else:
            return None
        pass

    def capnet_port_binding_update(self, binding):
        """Make a remote process call to update a capnet port binding"""
        cctxt = self.client.prepare(version='1.0')
        port = cctxt.call(self.context,'capnet_port_binding_update',
                          binding=binding) #, host=self.host)
        if port:
            return cn_util.DictModel(port)

    def wfagent_port_create(self, port):
        """Make a remote process call to create the workflow port."""
        cctxt = self.client.prepare(version='1.0')
        port = cctxt.call(self.context,'wfagent_port_create',
                          port=port, host=self.host)
        if port:
            return cn_util.DictModel(port)

    def wfagent_port_delete(self, port):
        """Make a remote process call to delete the workflow port."""
        cctxt = self.client.prepare(version='1.0')
        ret = cctxt.call(self.context,'wfagent_port_delete',port=port)
        #                 port=port, host=self.host)
        pass

    def wfagent_update(self,wfagent):
        """Make a remote process call to update the wfagent port_id/status."""
        cctxt = self.client.prepare(version='1.0')
        wfagent = cctxt.call(self.context,'wfagent_update',wfagent=wfagent)
        if wfagent:
            return cn_util.DictModel(wfagent)
        pass

    ## def get_active_networks_info(self):
    ##     """Make a remote process call to retrieve all network info."""
    ##     cctxt = self.client.prepare(version='1.0')
    ##     networks = cctxt.call(self.context, 'get_active_networks_info',
    ##                           host=self.host)
    ##     return [NetModel(self.use_namespaces, n) for n in networks]

    ## def get_network_info(self, network_id):
    ##     """Make a remote process call to retrieve network info."""
    ##     cctxt = self.client.prepare()
    ##     network = cctxt.call(self.context, 'get_network_info',
    ##                          network_id=network_id, host=self.host)
    ##     if network:
    ##         return cn_util.NetModel(self.use_namespaces, network)

    ## def update_workflow_port(self, port_id, port):
    ##     """Make a remote process call to update the workflow port."""
    ##     cctxt = self.client.prepare(version='1.0')
    ##     port = cctxt.call(self.context, 'update_workflow_port',
    ##                       port_id=port_id, port=port, host=self.host)
    ##     if port:
    ##         return cn_util.DictModel(port)

    ## def release_workflow_port(self, network_id, device_id):
    ##     """Make a remote process call to release the workflow port."""
    ##     cctxt = self.client.prepare()
    ##     return cctxt.call(self.context, 'release_workflow_port',
    ##                       network_id=network_id, device_id=device_id,
    ##                       host=self.host)

class CapnetAgentRpcCallbacks(object):
    # Set RPC API version to 1.0 by default.
    target = oslo_messaging.Target(version='1.0')

    def __init__(self, context, agent):
        super(CapnetAgentRpcCallbacks, self).__init__()
        self.context = context
        self.agent = agent
        pass

    ##
    ## These are the agent RPCs.
    ##

    def port_update(self, context, **kwargs):
        port = kwargs.get('port')
        # Put the port identifier in the updated_devices set.
        # Even if full port details might be provided to this call,
        # they are not used since there is no guarantee the notifications
        # are processed in the same order as the relevant API requests
        self.agent.updated_devices.add(port['id'])
        LOG.debug("port_update message processed for port %s (ctx %s, kwargs %s)",
                  port['id'],str(context),str(kwargs))

    def port_delete(self, context, **kwargs):
        port_id = kwargs.get('port_id')
        self.agent.deleted_devices.add(port_id)
        self.agent.updated_devices.discard(port_id)
        LOG.debug("port_delete message processed for port %s (ctx %s, kwargs %s)",
                  port_id,str(context),str(kwargs))

    def network_update(self, context, **kwargs):
        network_id = kwargs['network']['id']
        for port_id in self.agent.network_ports[network_id]:
            # notifications could arrive out of order, if the port is deleted
            # we don't want to update it anymore
            if port_id not in self.agent.deleted_devices:
                self.agent.updated_devices.add(port_id)
        LOG.debug("network_update message processed for network %s ports %s"
                  " (ctx %s, kwargs %s)",
                  network_id,self.agent.network_ports[network_id],
                  str(context),str(kwargs))

    def network_delete(self, context, **kwargs):
        network_id = kwargs['network']['id']
        LOG.debug("network_delete message processed for network %s (ctx %s, kwargs %s)",
                  network_id,str(context),str(kwargs))
        pass
    
    pass

class CapnetControllerMetadataManager(object):
    """
    This class manages the metadata files for the running controller
    process on this machine (because we don't have a metadata service in
    the controller that just listens for events directly from openstack.
    """
    def __init__(self,conf=cfg.CONF):
        self.rundir = conf.CAPNET.capnet_controller_rundir
        # map of datapath_id to ofport to
        #    {'role':role,'line':<line>,'binding':<bindingdict>}
        self.metadata = {}
        self.metadatafile_base = "%s/cnc.metadata" % (self.rundir,)
        self.neutron_metadata_service_location = \
            conf.CAPNET.neutron_metadata_service_location
        pass
    
    def _write_metadata(self,datapath_id=None):
        if not datapath_id is None:
            datapath_ids = [ datapath_id ]
        else:
            datapath_ids = self.metadata.keys()
            pass
        for dpid in datapath_ids:
            fname = "%s.%s" % (self.metadatafile_base,dpid)
            fname_tmp = "%s.%s.updating" % (self.metadatafile_base,dpid)
            f = file(fname_tmp,'w')
            for (k,v) in self.metadata[dpid].iteritems():
                f.write('%s\n' % (v['line'],))
                pass
            f.close()
            shutil.move(fname_tmp,fname)
            pass
        pass
    
    def _create_metadata_dict_from_binding(self,binding):
        """
        node <ofport> <domainid> <role> <port_id> <nodename> <ipaddr> <mask>
          (where role is node|master|uplink)
        """
        LOG.debug("creating metadata dict from binding %s" % (str(binding),))
        
        wfagent_name = None
        if 'role' in binding:
            role = binding['role']
        elif 'wfagent' in binding and not binding['wfagent'] is None:
            if binding['wfagent']['master']:
                role = 'master'
            else:
                # For now, wfagents aren't special; just nodes
                role = 'node'
                pass
            if 'name' in binding['wfagent']:
                wfagent_name = binding['wfagent']['name']
                pass
        else:
            role = 'node'
            pass
        
        if 'port' in binding and 'device_owner' in binding['port'] \
          and binding['port']['device_owner'] == 'network:dhcp':
            role = "%s:dhcp" % (role,)
            pass
        
        extra_role = ''
        if self.neutron_metadata_service_location == 'dhcp' \
          and 'port' in binding and 'device_owner' in binding['port'] \
          and binding['port']['device_owner'] == 'network:dhcp':
            extra_role = "osmetadata"
        elif self.neutron_metadata_service_location == 'l3' \
          and 'port' in binding and 'device_owner' in binding['port'] \
          and binding['port']['device_owner'] == 'network:router_interface':
            extra_role = "osmetadata"
            pass
        
        if not extra_role == '':
            if role.find(':') > -1:
                role = "%s,%s" % (role,extra_role)
            else:
                role = "%s:%s" % (role,extra_role)
                pass
            pass
        
        # Uplinks are special!
        if role == 'uplink':
            port_id = binding['port_id']
            name = port_id
            mac = binding['mac']
            line = "uplink %s %s %s %s" \
              % (str(binding['ofport']),port_id,name,mac)
        
            md = { 'role':role,'line':line,'mac':mac,
                   'ofport':binding['ofport'],
                   'datapath_id':binding['datapath_id'],'binding':binding }
            pass
        else:
            network_id = binding['port']['network_id']
            tenant_id = binding['port']['tenant_id']
            subnet_id = 'None'
            if 'ipallocation' in binding:
                subnet_id = binding['ipallocation']['subnet_id']
                pass
            domain_id = "%s-%s" % (network_id,subnet_id)
            owner_id = tenant_id
            
            name = None
            if 'name' in binding['port'] and binding['port']['name']:
                name = binding['port']['name']
            elif 'dns_name' in binding['port'] and binding['port']['dns_name']:
                dns_name = binding['port']['dns_name']
                pass
            mac = binding['port']['mac_address']
            port_id = binding['port_id']
            ipaddr = binding['ipallocation']['ip_address']
            nodename = name or wfagent_name or ipaddr or port_id
            # XXX: we currently don't get a mask!
            mask = '24'
            line = "node %s %s %s %s %s %s %s %s %s" \
              % (str(binding['ofport']),domain_id,owner_id,role,port_id,nodename,mac,
                 ipaddr,mask)
        
            md = { 'role':role,'line':line,'mac':mac,
                   'ofport':binding['ofport'],
                   'datapath_id':binding['datapath_id'],'binding':binding }
            pass
        return md
            
    @lockutils.synchronized('cnc-metadata')
    def set_uplink_metadata(self,datapath_id,ofport,port_id,mac):
        line = "uplink %s %s %s" % (str(ofport),str(port_id),str(mac))
        if not datapath_id in self.metadata:
            self.metadata[datapath_id] = {}
            pass
        md = { 'role':'uplink','line':line,'mac':mac,'ofport':ofport,
               'binding':None }
        LOG.debug("new metadata: %s" % (str(md),))
        self.metadata[datapath_id][ofport] = md
        self._write_metadata(datapath_id=datapath_id)
        pass
    
    @lockutils.synchronized('cnc-metadata')
    def set_metadata(self,bindings):
        self.metadata = {}
        for binding in bindings:
            dpid = binding['datapath_id']
            ofport = binding['ofport']
            md = self._create_metadata_dict_from_binding(binding)
            if not dpid in self.metadata:
                self.metadata[dpid] = {}
                pass
            self.metadata[dpid][ofport] = md
            pass
        self._write_metadata()
        pass
    
    @lockutils.synchronized('cnc-metadata')
    def add_metadata(self,binding):
        dpid = binding['datapath_id']
        ofport = binding['ofport']
        md = self._create_metadata_dict_from_binding(binding)
        if not dpid in self.metadata:
            self.metadata[dpid] = {}
            pass
        self.metadata[dpid][ofport] = md
        self._write_metadata()
        pass

    @lockutils.synchronized('cnc-metadata')
    def remove_metadata(self,binding):
        dpid = binding['datapath_id']
        ofport = binding['ofport']
        del self.metadata[dpid][ofport]
        self._write_metadata(datapath_id=dpid)
        pass
    
    pass


class CapnetControllerProcesses(object):
    """
    This class manages a set of running capnet controller processes.
    """
    def __init__(self,agent,physnet,physnet_idx,bridge,conf=cfg.CONF):
        self.agent = agent
        self.conf = conf
        self.physnet = physnet
        self.idx = physnet_idx
        self.bridge = bridge
        self.name = bridge.br_name
        self.datapath_id = bridge.get_datapath_id()
        self.running = False
        self.metadata = {}
        
        self.mul_core_pid = None
        self.controller_pid = None
        self.mul_cli_pid = None
        
        self.multi_controller_mode = self.conf.CAPNET.multi_controller_mode
        self.master_controller_ip = self.conf.CAPNET.master_controller_ip
        self.master_is_local = False
        ipdev = ip_lib.IPWrapper().get_device_by_ip(self.master_controller_ip)
        if ipdev is not None:
            self.master_is_local = True
            pass
        
        self.rundir = self.conf.CAPNET.capnet_controller_rundir
        
        self.mul_core_path = self.conf.CAPNET.mul_core_path
        self.mul_core_args = self.conf.CAPNET.mul_core_args.split(' ')
        self.mul_core_listen_address = self.conf.CAPNET.mul_core_listen_address
        self.mul_core_port = int(self.conf.CAPNET.mul_core_base_port) + self.idx
        self.mul_core_logfile = '%s/%s-mul-core.log' % (self.rundir,self.name)
        self.mul_core_pidfile = '%s/%s-mul-core.pid' % (self.rundir,self.name)
        self.mul_cli_path = self.conf.CAPNET.mul_cli_path
        self.mul_cli_args = self.conf.CAPNET.mul_cli_args.split(' ')
        self.mul_cli_port = int(self.conf.CAPNET.mul_cli_base_port) + self.idx
        self.mul_cli_logfile = '%s/%s-mul-cli.log' % (self.rundir,self.name)
        self.mul_cli_pidfile = '%s/%s-mul-cli.pid' % (self.rundir,self.name)
        self.controller_path = self.conf.CAPNET.capnet_controller_path
        self.controller_args = self.conf.CAPNET.capnet_controller_args.split(' ')
        self.controller_logfile = '%s/%s-controller.log' % (self.rundir,self.name)
        self.controller_pidfile = '%s/%s-controller.pid' % (self.rundir,self.name)
        self.controller_port = int(self.conf.CAPNET.capnet_controller_base_port) + self.idx
        self.capnet_controller_gdb_attach_wait_time = \
            self.conf.CAPNET.capnet_controller_gdb_attach_wait_time
        
        self._load_existing()
        pass
    
    def _load_existing(self):
        """
        Check the pidfiles and see if the pids in them are running.
        """
        try:
            f = file(self.mul_core_pidfile,'r')
            pid = f.read()
            pid = int(pid)
            os.kill(pid)
            self.mul_core_pid = pid
            LOG.debug("found existing mul core process %d for bridge %s"
                      % (pid,self.name))
        except:
            pass
        try:
            f = file(self.mul_cli_pidfile,'r')
            pid = f.read()
            pid = int(pid)
            os.kill(pid)
            self.mul_cli_pid = pid
            LOG.debug("found existing mul cli process %d for bridge %s"
                      % (pid,self.name))
        except:
            pass
        try:
            f = file(self.controller_pidfile,'r')
            pid = f.read()
            pid = int(pid)
            os.kill(pid)
            self.controller_pid = pid
            LOG.debug("found existing controller process %d for bridge %s"
                      % (pid,self.name))
        except:
            pass
        pass
    
    def ensure_running(self):
        if self.mul_core_pid == None:
            self.mul_core_start()
            time.sleep(1)
            pass
        if self.mul_cli_pid == None:
            self.mul_cli_start()
            time.sleep(1)
            pass
        if self.controller_pid == None:
            self.controller_start()
            time.sleep(1)
            pass
        pass
    
    def _spawn(self,path,argv,rundir=None,logfile=None,pidfile=None,
               run_as_root=False,run_as_root_with_fork=False,
               root_pidfile=None,root_pidfile_timeout=4):
        """
        This method has certainly evolved.  Basically, you don't want to ever
        run with both run_as_root=True AND run_as_root_with_fork=True; olso
        stuff like logging and messaging don't let us do simple os.fork()...
        you'll see duplicate messages in the log, etc.
        """
        if run_as_root_with_fork or not run_as_root:
            pid = os.fork()
        else:
            pid = 0
            pass
        
        if pid == 0:
            if run_as_root_with_fork or not run_as_root:
                os.setsid()
                if rundir:
                    os.chdir(rundir)
                try:
                    os.close(0)
                except:
                    pass
                if logfile:
                    try:
                        os.close(1)
                    except:
                        pass
                    try:
                        os.close(2)
                    except:
                        pass
                    logfd = os.open(logfile,os.O_CREAT | os.O_APPEND | os.O_WRONLY)
                    os.dup2(logfd,1)
                    os.dup2(logfd,2)
                    os.close(logfd)
                    pass
                pass
            if run_as_root:
                # Use rootwrap, sigh
                # Also, strip out the fully-qualified part of the path
                # and just use the basename -- the policy must be in
                # /etc/neutron/rootwrap.d/capnet.filters as a basename;
                # the full path is in the policy file :(.
                # So, we're really executing a filter from rootwrap.
                slash_idx = path.rfind('/')
                if slash_idx > -1:
                    path = path[slash_idx+1:]
                    pass
                cmd = [path]
                if argv:
                    cmd.extend(argv)
                    pass
                agent_utils.execute(cmd,run_as_root=run_as_root)
                if root_pidfile:
                    # NB: must wait for pidfile to be written to get the pid
                    while root_pidfile_timeout > 0:
                        try:
                            statres = os.stat(self.mul_core_pidfile)
                            if statres and statres.st_size > 0:
                                break
                            pass
                        except:
                            pass
                        time.sleep(1)
                        root_pidfile_timeout -= 1
                        pass
            
                    if root_pidfile_timeout > 0:
                        # stat returned something, assume it's the pid
                        f = file(root_pidfile,'r')
                        pid = int(f.read())
                        f.close()
                        LOG.debug("pid from rootwrap is %d for bridge %s cmd %s"
                                  % (pid,self.name,path))
                    else:
                        LOG.error("could not read a pid from %s for cmd %s"
                                  " for bridge %s; this will cause problems;"
                                  " check your config!"
                                  % (root_pidfile,path,self.name))
                        pid = None
                        pass
                    pass
                else:
                    # We have no way to pass back a pid!
                    pid = None
                    pass
                pass
            else:
                LOG.debug("spawning %s with args %s" % (path,str(argv)))
                os.execv(path,argv)
                sys.exit(-1)
                pass
            pass
        elif pidfile:
            f = file(pidfile,'w')
            f.write("%d\n" % (pid,))
            f.close()
            pass
        
        return pid
    
    def start(self):
        self.mul_core_start()
        time.sleep(1)
        self.mul_cli_start()
        time.sleep(1)
        self.controller_start()
        pass
    
    def mul_core_start(self):
        LOG.debug("starting mul core for physnet %s, bridge %s"
                  % (self.physnet,self.name))
        try: os.unlink(self.mul_core_pidfile)
        except: pass
            
        av = []
        if self.mul_core_args:
            av = self.mul_core_args
            pass
        av.extend(['-P','%d' % (self.mul_core_port,)])
        
        path_is_rootwrap_filter = self.mul_core_path.find('/') > -1
        
        if not path_is_rootwrap_filter:
            av.extend(['--pidfile',self.mul_core_pidfile,
                       '--logfile',self.mul_core_logfile])
            self.mul_core_pid = self._spawn(self.mul_core_path,av,
                                            rundir=self.rundir,
                                            run_as_root=True,
                                            root_pidfile=self.mul_core_pidfile)
        else:
            self.mul_core_pid = self._spawn(self.mul_core_path,av,
                                            rundir=self.rundir,
                                            logfile=self.mul_core_logfile,
                                            pidfile=self.mul_core_pidfile)
        if not self.mul_core_pid:
            LOG.error("failed to start mul core for physnet %s, bridge %s --"
                      " expect problems!"
                      % (self.physnet,self.name))
        pass
    
    def controller_start(self):
        LOG.debug("starting mul controller %s for physnet %s, bridge %s"
                  % (self.controller_path,self.physnet,self.name))
        try: os.unlink(self.controller_pidfile)
        except: pass
        
        if self.controller_path == 'valgrind':
            av = [ '/opt/tcloud/capnet/bin/capnet-controller' ]
        else:
            av = []
            pass
        if self.controller_args:
            av.extend(self.controller_args)
            pass
        av.extend(['-S','%s' % (self.rundir,)])
        if self.multi_controller_mode:
            if not self.master_is_local:
                av.extend(['-M','%s:%d' % (self.master_controller_ip,
                                           self.controller_port)])
            else:
                av.extend(['-L','%s:%d' % (self.master_controller_ip,
                                           self.controller_port)])
                pass
            pass
        
        path_is_rootwrap_filter = self.controller_path.find('/') > -1
        
        if not path_is_rootwrap_filter:
            av.extend(['--pidfile',self.controller_pidfile,
                       '--logfile',self.controller_logfile])
            self.controller_pid = self._spawn(self.controller_path,av,
                                            rundir=self.rundir,
                                            run_as_root=True,
                                            root_pidfile=self.controller_pidfile)
        else:
            self.controller_pid = self._spawn(self.controller_path,av,
                                            rundir=self.rundir,
                                            logfile=self.controller_logfile,
                                            pidfile=self.controller_pidfile)
        if not self.controller_pid:
            LOG.error("failed to start capnet controller for physnet %s, bridge %s --"
                      " expect problems!"
                      % (self.physnet,self.name))
        if self.capnet_controller_gdb_attach_wait_time > 0:
            i = 0
            while i < self.capnet_controller_gdb_attach_wait_time:
                LOG.info("waiting %d more seconds for GDB to attach to"
                         " controller pid %d ..."
                         % (self.capnet_controller_gdb_attach_wait_time - i,
                            self.controller_pid))
                i += 1
                time.sleep(1)
                pass
            pass
        
        pass
    
    def mul_cli_start(self):
        LOG.debug("starting mulcli %s for physnet %s, bridge %s"
                  % (self.mul_cli_path,self.physnet,self.name))
        try: os.unlink(self.mul_cli_pidfile)
        except: pass
            
        av = []
        if self.mul_cli_args:
            av = self.mul_cli_args
            pass
        av.extend(['-V','10000','-s','127.0.0.1'])
        
        path_is_rootwrap_filter = self.mul_cli_path.find('/') > -1
        
        if not path_is_rootwrap_filter:
            av.extend(['--pidfile',self.mul_cli_pidfile,
                       '--logfile',self.mul_cli_logfile])
            self.mul_cli_pid = self._spawn(self.mul_cli_path,av,
                                            rundir=self.rundir,
                                            run_as_root=True,
                                            root_pidfile=self.mul_cli_pidfile)
        else:
            self.mul_cli_pid = self._spawn(self.mul_cli_path,av,
                                            rundir=self.rundir,
                                            logfile=self.mul_cli_logfile,
                                            pidfile=self.mul_cli_pidfile)
        if not self.mul_cli_pid:
            LOG.error("failed to start capnet mulcli for physnet %s, bridge %s --"
                      " expect problems!"
                      % (self.physnet,self.name))
        pass
    
    def stop(self):
        self.controller_stop()
        self.mul_cli_stop()
        self.mul_core_stop()
        pass
    
    def _safe_kill(self,pid,name,rootwrap_filter=None):
        if not os.path.exists("/proc/%s" % (str(pid),)):
            LOG.debug("%s pid %s for bridge %s already gone"
                      % (name,str(pid),self.name))
            return True
        
        running = 4
        while running > 0:
            try:
                if rootwrap_filter:
                    cmd = [rootwrap_filter,'-INT',str(pid)]
                    agent_utils.execute(cmd,run_as_root=True)
                else:
                    os.kill(pid,signal.SIGINT)
                    pass
                time.sleep(1)
            except:
                pass
            if not os.path.exists("/proc/%s" % (str(pid),)):
                LOG.debug("interrupted %s pid %s for bridge %s"
                          % (name,str(pid),self.name))
                return True
            running -= 1
            pass
        # If we get here, we think it might still be running.
        running = 4
        while running > 0:
            try:
                if rootwrap_filter:
                    cmd = [rootwrap_filter,'-9',str(pid)]
                    agent_utils.execute(cmd,run_as_root=True)
                else:
                    os.kill(pid,signal.SIGKILL)
                    pass
                time.sleep(1)
            except:
                pass
            if not os.path.exists("/proc/%s" % (str(pid),)):
                LOG.debug("killed %s pid %d for bridge %s"
                          % (name,pid,self.name))
                return True
            running -= 1
            pass
            
        # If we get here, SIGINT and SIGKILL have failed!
        LOG.warn("could not interrupt nor kill %s pid %s for bridge %s!"
                 % (name,str(pid),self.name))
        return False
    
    def mul_core_stop(self):
        if self.mul_core_path.find('/') > -1:
            rootwrap_filter = None
        else:
            rootwrap_filter = 'kill-mul'
            pass
        
        if not self.mul_core_pid == None:
            if self._safe_kill(self.mul_core_pid,'mul',
                               rootwrap_filter=rootwrap_filter):
                self.mul_core_pid = None
            else:
                # For now, just null it out and hope that we can start
                # a replacement.
                self.mul_core_pid = None
                pass
            pass
        pass
    
    def mul_cli_stop(self):
        if self.mul_cli_path.find('/') > -1:
            rootwrap_filter = None
        else:
            rootwrap_filter = 'kill-mulcli'
            pass
        
        if not self.mul_cli_pid == None:
            if self._safe_kill(self.mul_cli_pid,'mulcli',
                               rootwrap_filter=rootwrap_filter):
                self.mul_cli_pid = None
            else:
                # For now, just null it out and hope that we can start
                # a replacement.
                self.mul_cli_pid = None
                pass
            pass
        pass
    
    def controller_stop(self):
        if self.controller_path.find('/') > -1:
            rootwrap_filter = None
        else:
            rootwrap_filter = 'kill-capnet-controller'
            pass
        
        if not self.controller_pid == None:
            if self._safe_kill(self.controller_pid,'controller',
                               rootwrap_filter=rootwrap_filter):
                self.controller_pid = None
            else:
                # For now, just null it out and hope that we can start
                # a replacement.
                self.controller_pid = None
                pass
            pass
        pass
    
    pass


class CapnetWorkflowAgentManager(object):
    """
    This class manages the execution of more workflow applications for a
    CapnetNeutronAgent.
    """
    def __init__(self,agent,conf=cfg.CONF,use_namespaces=True):
        self.conf = conf
        self.agent = agent
        self.ifdriver = agent_utils.load_interface_driver(conf)
        self.use_namespaces = use_namespaces
        self.agents = {}
        self.wfapp_launcher = conf.CAPNET.wfapp_launcher
        self.rundir = conf.CAPNET.capnet_controller_rundir
        self.ip = ip_lib.IPWrapper()
        pass
    
    def get_interface_name(self,port):
        """
        Return interface(device) name for use by the wfagent.
        """
        return self.ifdriver.get_device_name(port)

    def get_device_id(self,wfagent):
        """Return a unique DHCP device ID for this host on the network."""
        # There could be more than one wfagent per network, so create
        # a device id that combines host and wfagent ids.
        # Split host so as to always use only the hostname and
        # not the domain name. This will guarantee consistentcy
        # whether a local hostname or an fqdn is passed in.
        local_hostname = self.conf.host.split('.')[0]
        host_uuid = uuid.uuid5(uuid.NAMESPACE_DNS, str(local_hostname))
        return 'wfa%s-%s' % (host_uuid,wfagent['id'])

    def _setup_wfagent_port(self,context,wfagent):
        """
        Create wfagent port for the host if needed and return port.
        """
        # The ID that the wfagent port will have (or already has).
        device_id = self.get_device_id(wfagent)
        network_id = wfagent.network_id
        tenant_id = wfagent.tenant_id
        fixed_ips = None
        subnet_id = None
        if 'subnet_id' in wfagent and wfagent.subnet_id is not None:
            subnet_id = wfagent.subnet_id
            fixed_ips = [ dict(subnet_id=subnet_id) ]
            pass
        
        LOG.debug('creating wfagent port %s on network %s, tenant %s, subnet %s'
                  % (device_id,network_id,tenant_id,subnet_id))

        port_dict = dict(name='',admin_state_up=True,device_id=device_id,
                         network_id=network_id,tenant_id=tenant_id)
        if fixed_ips:
            port_dict['fixed_ips'] = fixed_ips
            pass
        port = self.agent.capnet_ext_plugin_rpc.wfagent_port_create(port_dict)
        return port

    def cleanup_stale_namespaces(self):
        LOG.debug("Cleaning stale wfa namespaces")
        for ns in self.ip.get_namespaces():
            if not ns.startswith('qwfa'):
                continue
            ns_ip = ip_lib.IPWrapper(namespace=ns)
            if ns_ip.namespace_is_empty():
                LOG.debug("Cleaning stale wfa namespace %s" % (ns,))
                ns_ip.garbage_collect_namespace()
            pass
        pass

    def ensure_running(self,wfagent,port):
        self.__add_namespace(wfagent)
        
        network_id = port.network_id
        network = self.agent.get_network(network_id)
        interface_name = self.get_interface_name(port)

        if ip_lib.ensure_device_is_ready(interface_name,
                                         namespace=wfagent.namespace):
            LOG.debug('Reusing existing device: %s.',interface_name)
        else:
            #
            # NB: this little bit of code ensures that the wfagent/port
            # binding is sent to the server *BEFORE* the port is
            # plugged.  This is important, because when the agent main
            # thread sees a new port plugged into the switch, it will
            # send a binding_update to the server, and server will send
            # back a binding notification that the metadata service
            # handles.  If the binding update hits the server before the
            # wfagent update hits it, the biding notification will hit
            # the agent *without* the important little bit of 'wfagent'
            # field in it -- and the metadata manager will write the
            # role of the node as simply 'node', when perhaps it should
            # have been 'master'!
            #
            # This solution costs an extra RPC, but it's all right.  The
            # alternative is for the metadata manager and workflow agent
            # manager to collaborate (i.e. metadata manager would see
            # device owner neutron:capnet-wfagent, but no wfagent in the
            # notification, and then call over here to the wfagent
            # manager who would know the binding).
            #
            wfagent.port_id = port['id']
            wfagent.status = 'plugging'
            
            LOG.debug("calling wfagent_update (plugging): %s" % (str(wfagent),))
            self.agent.capnet_ext_plugin_rpc.wfagent_update(wfagent)
            
            self.ifdriver.plug(network.id,port.id,interface_name,
                             port.mac_address,namespace=wfagent.namespace)
            pass

        ip_cidrs = []
        for fixed_ip in port.fixed_ips:
            subnet_id = fixed_ip.subnet_id
            subnet = self.agent.get_subnet(subnet_id,network_id)
            if not ipv6_utils.is_auto_address_subnet(subnet):
                net = netaddr.IPNetwork(subnet.cidr)
                ip_cidr = '%s/%s' % (fixed_ip.ip_address, net.prefixlen)
                ip_cidrs.append(ip_cidr)
            pass
        
        self.ifdriver.init_l3(interface_name, ip_cidrs,
                              namespace=wfagent.namespace)

        ## if self.use_namespaces:
        ##     self._set_default_route(network, interface_name)
        ##     try:
        ##         self._cleanup_stale_devices(network, port)
        ##     except Exception:
        ##         # catch everything as we don't want to fail because of
        ##         # cleanup step
        ##         LOG.error(_LE("Exception during stale dhcp device cleanup"))
        ##     pass

        # actually run the agent program... need a rootwrap wrapper.
        LOG.debug("launching workflow app:")
        pidfile = "%s/wfagent.%s.%s.pid" % (self.rundir,wfagent.name,wfagent.id)
        logfile = "%s/wfagent.%s.%s.log" % (self.rundir,wfagent.name,wfagent.id)
        cmd = [ 'ip','netns','exec',str(wfagent.namespace),
                self.wfapp_launcher,pidfile,logfile,interface_name,
                wfagent.wfapp_path ]
        if 'wfapp_args' in wfagent and wfagent.wfapp_args is not None \
          and len(wfagent.wfapp_args) > 0:
            cmd.extend(wfagent.wfapp_args.split(' '))
            pass
        agent_utils.execute(cmd,run_as_root=True)
        
        # Call wfagent_update(wfagent) to update wfagent status, and
        # port_id, now that we've created one. XXX
        wfagent.port_id = port['id']
        wfagent.status = 'running'
        
        LOG.debug("calling wfagent_update: %s" % (str(wfagent),))
        
        self.agent.capnet_ext_plugin_rpc.wfagent_update(wfagent)
        return interface_name
    
    def _setup(self,context,wfagent):
        """
        Create and initialize a device for the wfagent on this host.
        """
        try:
            self.cleanup_stale_namespaces()
        except:
            LOG.error(_LE("Exception during stale wfa namespace cleanup"))
            pass
        
        port = self._setup_wfagent_port(context,wfagent)
        interface_name = self.ensure_running(wfagent,port)
        
        self.agents[wfagent.id] = wfagent
        
        return interface_name

    def delete(self,context,wfagent):
        """
        Unplugs the wfagent port, and requests that the server delete
        the port object.
        """
        self.__add_namespace(wfagent)
        
        if wfagent.binding and wfagent.binding.device_name:
            self.ifdriver.unplug(wfagent.binding.device_name,
                                 namespace=wfagent.namespace)
        else:
            LOG.debug('No interface exists for wfagent %s', wfagent.id)

        if wfagent.binding and wfagent.binding.port:
            self.agent.capnet_ext_plugin_rpc.wfagent_port_delete(
                wfagent.binding.port)
            pass
        
        if wfagent.id in self.agents:
            del self.agents[wfagent.id]
            pass
        
        try:
            self.cleanup_stale_namespaces()
        except:
            LOG.error(_LE("Exception during stale wfa namespace cleanup"))
            pass
        
        pass
    
    def get_wfagent_name_by_port_id(self,port_id):
        wfagent = None
        for wfagent in self.agents:
            if wfagent.port_id == port_id:
                break
            else:
                wfagent = None
                pass
            pass
        if not wfagent is None and 'name' in wfagent:
            return wfagent.name
            pass
        
        return None

    def _bind(self,context,wfagent):
        """
        To bind a port, all we do is update the wfagent and its binding
        with the right role (wfagent or wfagent-master).  Technically,
        it is not necessary for the Neutron server to push a "bind" out
        to the host the port is located at, but this gets the
        information to the agent, in case it needs it in the future.
        Right now, all bind does here locally is verifies that the port
        is up, plugged in, and updates the role in the binding and sends
        the Neutron server.
        """
        port_id = wfagent['port_id']
        binding = wfagent['binding'] or {}
        
        vifport = self.agent.get_vif_port_by_id(port_id)
        if not vifport:
            LOG.warn("port_id %s does not exist on this host; cannot bind!"
                     % (port_id,))
            return
        
        binding['datapath_id'] = vifport.switch.get_datapath_id()
        binding['ofport'] = int(vifport.ofport)
        binding['device_name'] = vifport.port_name
        binding['host'] = cfg.CONF.host
        
        self.agent.capnet_ext_plugin_rpc.capnet_port_binding_update(binding)
        
        self.agents[wfagent.id] = wfagent
        
        pass
    
    def __add_namespace(self,wfagent):
        if self.use_namespaces:
            wfagent.namespace = "%s%s" % (cn_const.CAPNET_WFAGENT_NS_PREFIX,
                                          wfagent.id)
        else:
            wfagent.namespace = None
        pass
    
    def create(self,context,wfagent):
        """
        Creates a new port for a wfagent and gets one going on it, in a netns;
        or "binds" an existing port.
        """
        self.__add_namespace(wfagent)

        if wfagent['port_id']:
            return self._bind(context,wfagent)
        else:
            return self._setup(context,wfagent)
        pass
    
    pass


class CapnetAgentExtRpcCallbacks(object):
    """
    Capnet agent client-side RPCs; server calls these to notify us.
    
    We use really coarse-grained locking for now; in some cases, it
    could be updated per-wfagent or per-controller.

    Note that the public methods of this class are exposed as the server
    side of an rpc interface.  The neutron server uses
    neutron.api.rpc.agentnotifiers.capnet_rpc_agent_api.CapnetAgentNotifyApi
    as the client side to execute the methods here.  For more
    information about changing rpc interfaces, see
    doc/source/devref/rpc_api.rst.
    """
    target = oslo_messaging.Target(topic=cn_topics.AGENT_CAPNET,
                                   version='1.0')

    def __init__(self,context,agent):
        super(CapnetAgentExtRpcCallbacks, self).__init__()
        self.context = context
        self.agent = agent
        pass
    
    @lockutils.synchronized('wfagent-lock')
    def wfagent_create(self,context,wfagent):
        wfagent = cn_util.DictModel(wfagent)
        LOG.debug("wfagent_create: context = %s , wfagent = %s"
                  % (str(context),str(wfagent)))
        self.agent.wfagent_manager.create(context,wfagent)
        pass
    
    @lockutils.synchronized('wfagent-lock')
    def wfagent_restart(self,context,wfagent):
        wfagent = cn_util.DictModel(wfagent)
        LOG.debug("wfagent_restart: context = %s , wfagent = %s"
                  % (str(context),str(wfagent)))
        self.agent.wfagent_manager.restart(context,wfagent)
        pass
    
    @lockutils.synchronized('wfagent-lock')
    def wfagent_delete(self,context,wfagent):
        wfagent = cn_util.DictModel(wfagent)
        LOG.debug("wfagent_delete: context = %s , wfagent = %s"
                  % (str(context),str(wfagent)))
        self.agent.wfagent_manager.delete(context,wfagent)
        pass

    @lockutils.synchronized('metadata-lock')
    def portbinding_update(self,context,binding):
        """
        Get a notification that a capnet port binding has been updated.
        If we're hosting a controller, we need to update the metadata
        file for it with this info.
        """
        binding = cn_util.DictModel(binding)
        LOG.debug("portbinding_update: binding = %s" % (str(binding),))
        self.agent.metadata_manager.add_metadata(binding)
        pass
    
    #@lockutils.synchronized('dhcp-agent')
    def port_update_end(self, context, payload):
        """Handle the port.update.end notification event."""
        updated_port = cn_util.DictModel(payload['port'])
        network = self.cache.get_network_by_id(updated_port.network_id)
        if network:
            driver_action = 'reload_allocations'
            if self._is_port_on_this_agent(updated_port):
                orig = self.cache.get_port_by_id(updated_port['id'])
                # assume IP change if not in cache
                old_ips = {i['ip_address'] for i in orig['fixed_ips'] or []}
                new_ips = {i['ip_address'] for i in updated_port['fixed_ips']}
                if old_ips != new_ips:
                    driver_action = 'restart'
            self.cache.put_port(updated_port)
            self.call_driver(driver_action, network)

    def _is_port_on_this_agent(self, port):
        thishost = n_utils.get_dhcp_agent_device_id(
            port['network_id'], self.conf.host)
        return port['device_id'] == thishost

    # Use the update handler for the port create event.
    port_create_end = port_update_end

    #@lockutils.synchronized('dhcp-agent')
    def port_delete_end(self, context, payload):
        """Handle the port.delete.end notification event."""
        port = self.cache.get_port_by_id(payload['port_id'])
        if port:
            network = self.cache.get_network_by_id(port.network_id)
            self.cache.remove_port(port)
            self.call_driver('reload_allocations', network)
    
    pass


class CapnetNeutronAgent(service.Service):
    """
    Implements Capnet networks.
    
    This agent supports multiple capnet provider networks.  Multiple
    virtual networks can be hosted atop a single capnet provider network
    without segmentation, because capnet provides edge node isolation
    and forces its users to construct dataplane flows they require.
    This contrasts with current L2 networks, which provide ambient
    authority that allows full L2 connectivity, and may be restricted by
    firewalls/ACLs.  Thus, in Capnet, there is no need for segmentation
    devices like vlans to segregate L2 domains.  Each L2 domain is
    governed by its users, who control the flows between nodes in that
    (or other) domains.  In fact, the only real reason to have more than
    one Capnet OpenVSwitch switch is utilize the additional bandwidth of
    another physical LAN, or to segregate networks based on how many
    rules/capability flows they require.
    
    Right now, the agent is quite basic.  All it must do is add ports to
    the proper openvswitch switch on a hypervisor node.  The mechanism
    driver does the work of running the controller and any workflow
    agents on the networkmanager/controller node.
    """

    # history
    #   1.0 Initial version
    target = oslo_messaging.Target(version='1.0')

    def __init__(self, bridge_mappings, polling_interval, quitting_rpc_timeout):
        """
        Constructor.

        :param bridge_mappings: mappings from physical network name to bridge.
        :param polling_interval: interval (secs) to poll DB.
        :param quitting_rpc_timeout: timeout in seconds for rpc calls after
               stop is called.
        """
        super(CapnetNeutronAgent, self).__init__()
        
        self.running = False
        
        self.bridge_mappings = bridge_mappings
        self.uplink_bindings = []
        # Store network mapping to segments
        self.network_map = {}
        self.phys_brs = {}
        # XXX: we need to deal with changing binding between ofport number
        # and tap/phys interface.  But, we already don't know what to do with
        # existing caps when OVS restarts, so punt for now.
        # keeps association between ports and ofports to detect ofport change
        #self.vifname_to_ofport_map = {}
        self.updated_devices = set()
        self.deleted_devices = set()
        self.network_ports = collections.defaultdict(set)
        
        self.polling_interval = polling_interval
        self.quitting_rpc_timeout = quitting_rpc_timeout
        self.iter_num = 0
        
        self.context = None
        self.plugin_rpc = None
        self.state_rpc = None
        self.capnet_ext_plugin_rpc = None
        self.agent_state = None
        self.device_count = 0
        
        self.ip = ip_lib.IPWrapper()
        
        self.hosts_controllers = cfg.CONF.CAPNET.hosts_controllers
        self.master_controller_ip = cfg.CONF.CAPNET.master_controller_ip
        self.multi_controller_mode = cfg.CONF.CAPNET.multi_controller_mode
        self.hosts_workflow_apps = cfg.CONF.CAPNET.hosts_workflow_apps
        
        self.is_master = True
        try:
            if self.master_controller_ip \
                and not self.ip.get_device_by_ip(self.master_controller_ip):
                self.is_master = False
                pass
            pass
        except:
            self.is_master = False
            pass
        
        self.phys_br_controller_processes = {}
        
        self.metadata_manager = CapnetControllerMetadataManager()
        self.wfagent_manager = CapnetWorkflowAgentManager(self)

        LOG.info(_LI("Capnet bridge_mappings = %s"),self.bridge_mappings)
        
        pass
    
    def _refresh_network_info(self):
        nets = self.capnet_ext_plugin_rpc.capnet_get_network_info(
            self.context,host=cfg.CONF.host)
        if nets:
            for net in nets:
                self.networks[net.id] = net
            pass
        
        pp = pprint.PrettyPrinter(indent=2)
        ns = pp.pformat(self.networks)
        LOG.debug("capnet networks: %s" % (ns,))
        pass
    
    # Only public methods lock.
    def __get_network(self,network_id):
        if network_id in self.networks.keys():
            return self.networks[network_id]
        else:
            self._refresh_network_info()
            if network_id in self.networks.keys():
                return self.networks[network_id]
            else:
                return None
        pass
    
    @lockutils.synchronized('network-info')
    def get_network(self,network_id):
        return self.__get_network(network_id)
    
    # Only public methods lock.
    def __get_subnet(self,subnet_id,network_id=None):
        if network_id:
            network = self.__get_network(network_id)
            if not network:
                return None
            for subnet in network.subnets:
                if subnet.id == subnet_id:
                    return subnet
            return None
        else:
            for (network_id,network) in self.networks.iteritems():
                for subnet in network.subnets:
                    if subnet.id == subnet_id:
                        return subnet
                    pass
                pass
            
            # Need to try a refresh...
            self._refresh_network_info()
            
            for (network_id,network) in self.networks.iteritems():
                for subnet in network.subnets:
                    if subnet.id == subnet_id:
                        return subnet
                    pass
                pass
            # Ok, forget it, nothing here.
            return None
        pass
    
    @lockutils.synchronized('network-info')
    def get_subnet(self,subnet_id,network_id=None):
        return self.__get_subnet(subnet_id,network_id=network_id)
    
    def start(self):
        self.setup_physical_bridges(self.bridge_mappings)
        self.validate_bridge_mappings()
        
        configurations = {
            'devices': self.device_count,
            'bridge_mappings': self.bridge_mappings,
            'uplink_bindings': self.uplink_bindings,
            'hosts_controllers': self.hosts_controllers,
            'master_controller_ip': str(self.master_controller_ip),
            'multi_controller_mode': self.multi_controller_mode,
            'hosts_workflow_apps': self.hosts_workflow_apps,
            'is_master': self.is_master,
            }
        
        self.agent_state = {
            'binary': 'neutron-capnet-agent',
            'host': cfg.CONF.host,
            'topic': n_const.L2_AGENT_TOPIC,
            'configurations': configurations,
            'agent_type': cn_const.AGENT_TYPE_CAPNET,
            'start_flag': True,
        }
        
        self.device_count = 0

        # Stores port update notifications for processing in main rpc loop
        self.updated_devices = set()
        # Stores port delete notifications
        self.deleted_devices = set()
        self.network_ports = collections.defaultdict(set)
        #self.vifname_to_ofport_map = {}

        self.setup_rpc()
        self.iter_num = 0
        self.running = True
        
        # Grab all current Capnet network info for this host:
        self.networks = {}
        self._refresh_network_info()
        
        # Grab all current Capnet port binding metadata from the server:
        bindings = self.capnet_ext_plugin_rpc.capnet_get_port_bindings()
        self.metadata_manager.set_metadata(bindings)
        
        # One last thing we must do: we have to send a binding update
        # for the physical ports in the OVS bridges -- they are our
        # "uplink" ports.  The Capnet Ext RPC functions in the plugin
        # know how to handle these fake ports with fake port_ids.
        # NB: this mapping *cannot* change; if it does, you must restart
        # the agent to tell the switch about the new binding.
        #
        # We also put this info in bridge_uplink_mappings above,
        # so that when/if the agent restarts on the hosts_controller
        # node, it can get all the mappings on startup (see the call just
        # above to capnet_get_port_bindings()
        for cb in self.uplink_bindings:
            LOG.debug("sending uplink binding update: %s" % (str(cb),))
            self.capnet_ext_plugin_rpc.capnet_port_binding_update(cb)
            # NB: we also force them into our metadata manager, because
            # we are about to startup the controllers, and those require
            # this metadata to be present!
            self.metadata_manager.add_metadata(cb)
            pass
        
        # Actually startup our controllers, now that we have reasonable
        # metadata written.
        self.run_physical_bridge_controllers()
        
        try:
            self.wfagent_manager.cleanup_stale_namespaces()
        except:
            LOG.error(_LE("Exception during stale wfa namespace cleanup"))
            pass
        
        # Fire off any workflow agents that need to be running...
        if self.hosts_workflow_apps:
            for binding in bindings:
                if 'wfagent' in binding and not binding['wfagent'] is None \
                  and 'port' in binding and not binding['port'] is None \
                  and 'host' in binding and binding['host'] == cfg.CONF.host:
                    LOG.debug("ensure_running(%s)" % (str(binding),))
                    self.wfagent_manager.ensure_running(binding['wfagent'],
                                                        binding['port'])
                    pass
                pass
            pass
        
        self.daemon_loop()

    @lockutils.synchronized('capnet_agent_stop')
    def stop(self, graceful=True):
        if not self.running:
            return
        LOG.info(_LI("Stopping Capnet agent."))
        for (physical_network,ccp) in self.phys_br_controller_processes.iteritems():
            LOG.info(_LI("Stopping mul and controller processes for physical network %s"
                         % (physical_network)))
            ccp.stop()
            pass
        if graceful and self.quitting_rpc_timeout:
            self.set_rpc_timeout(self.quitting_rpc_timeout)
        self.running = False
        super(CapnetNeutronAgent, self).stop(graceful)

    def reset(self):
        common_config.setup_logging()
        self.setup_physical_bridges(self.bridge_mappings)
        self.validate_bridge_mappings()
        #self.updated_devices = set()
        #self.deleted_devices = set()
        #self.network_ports = collections.defaultdict(set)
        ##self.vifname_to_ofport_map = {}
        #self.iter_num = 0
        pass

    def _report_state(self):
        try:
            # How many devices are likely used by a VM
            self.agent_state.get('configurations')['devices'] = self.device_count
            self.state_rpc.report_state(self.context,self.agent_state)
            self.agent_state.pop('start_flag', None)
        except Exception:
            LOG.exception(_LE("Failed reporting state!"))
        pass
    
    def _build_agent_id(self):
        aid = ""
        brv = self.phys_brs.values()
        if len(brv) > 0:
            aid = brv[0].get_datapath_id()
        else:
            # XXX: a config reset should not change our uuid?
            aid = uuid.uuid4() & UINT64_BITMASK
            pass
        return 'capnet-agent-%s' % (str(aid),)

    def setup_rpc(self):
        self.agent_id = self._build_agent_id()
        LOG.info(_LI("RPC agent_id: %s"), self.agent_id)

        self.context = context.get_admin_context_without_session()
        
        self.plugin_rpc = CapnetPluginApi(topics.PLUGIN)
        self.state_rpc = agent_rpc.PluginReportStateAPI(topics.PLUGIN)
        self.capnet_ext_plugin_rpc = CapnetExtPluginApi(
            cn_topics.PLUGIN_CAPNET,self.context,
            cfg.CONF.CAPNET.use_namespaces,cfg.CONF.host)
        
        # Handle notifications from server, both the regular agent stuff,
        # and the extended capnet stuff.
        endpoints = [ CapnetAgentRpcCallbacks(self.context,self) ]
        
        # Define the listening consumers for the regular agent stuff...
        consumers = [[topics.PORT, topics.UPDATE],
                     [topics.PORT, topics.DELETE],
                     [topics.NETWORK, topics.UPDATE],
                     [topics.NETWORK, topics.DELETE]]
        # Don't start up the rpc listener until we load in our extended
        # agent stuff.
        self.connection = agent_rpc.create_consumers(endpoints,topics.AGENT,
                                                     consumers,
                                                     start_listening=True) #False)
        if self.hosts_workflow_apps:
            # ... and define the consumers for the extended capnet stuff.
            # This is just like having a second agent, without having one.
            # Maybe this is a bad idea...
            endpoints = [ CapnetAgentExtRpcCallbacks(self.context,self) ]
            # NOTE: these workflow endpoints are host-specific endpoints,
            # so set the node_only boolean to make sure only those topic
            # subscriptions get made.
            consumers = [[cn_topics.WORKFLOW_AGENT,topics.CREATE,cfg.CONF.host],
                         [cn_topics.WORKFLOW_AGENT,topics.DELETE,cfg.CONF.host],
                         [cn_topics.WORKFLOW_AGENT,cn_topics.RESTART,cfg.CONF.host],
                         [cn_topics.PORT_BINDING,topics.UPDATE,cfg.CONF.host]]
            #
            # NB: it seems one must make a separate connection for different
            # topics!  So we make one instead of adding consumers to our
            # existing generic agent connection.
            #
            #cn_rpc_util.add_consumers(self.connection,endpoints,
            #                          cn_topics.AGENT_CAPNET,
            #                          consumers)
            self.connection2 = agent_rpc.create_consumers(endpoints,cn_topics.AGENT_CAPNET,
                                                     consumers,
                                                     start_listening=True) #False)
            LOG.info("listening on extended capnet agent rpc queues (%s)" % (cfg.CONF.host,))
            pass
        
        # Actually start the RPC listener, now that we've configured
        # all the endpoints
        #self.connection.consume_in_threads()
        
        report_interval = cfg.CONF.AGENT.report_interval
        if report_interval:
            heartbeat = loopingcall.FixedIntervalLoopingCall(
                self._report_state)
            heartbeat.start(interval=report_interval)
        
        pass
    
    def setup_physical_bridges(self, bridge_mappings):
        """
        Setup the physical network bridges.

        Creates physical network bridges and links them to the
        integration bridge using veths or patch ports.

        :param bridge_mappings: map physical network names to bridge names.
        """
        # XXX: we have to choose i in a better way that is global.
        i = 0
        self.phys_br_mul_offsets = {}
        
        self.phys_brs = {}
        self.uplink_bindings = []
        ovs = ovs_lib.BaseOVS()
        ovs_bridges = ovs.get_bridges()
        for physical_network, bridge in six.iteritems(bridge_mappings):
            LOG.info(_LI("Mapping physical network %s to bridge %s"),
                     physical_network,bridge)
            # setup physical bridge
            if bridge not in ovs_bridges:
                LOG.error(_LE("Bridge %(bridge)s for physical network "
                              "%(physical_network)s does not exist. Agent "
                              "terminated!"),
                          {'physical_network': physical_network,
                           'bridge': bridge})
                sys.exit(1)
            
            br = ExtOVSBridge(bridge)
            
            # XXX: get physical ports and add them to metadata file as
            # uplink ports.  This is sort of bad as a general solution,
            # but it's ok in Cloudlab, and for the majority of
            # deployments.
            nvps = br.get_non_vif_ports()
            dpid = br.get_datapath_id()
            for nvp in nvps:
                LOG.debug("considering possible uplink port %s" % (str(nvp)))
                isphys = False
                try:
                    link = os.readlink('/sys/class/net/%s' % (nvp.port_name,))
                    if not 'virtual' in link:
                        isphys = True
                        pass
                    pass
                except:
                    LOG.exception(_LE("failed to check port name %s for uplink"
                                      % (nvp.port_name)))
                    pass
                
                if isphys:
                    # Just make up a port id; this is not a real OpenStack port.
                    port_id = str(dpid) + "-" + nvp.port_name
                    cn_binding = { 'port_id':port_id }
                    # Also, specify a deliberate role to avoid putting this
                    # in the DB.
                    cn_binding['role'] = 'uplink'
                    cn_binding['datapath_id'] = dpid
                    cn_binding['ofport'] = nvp.ofport
                    cn_binding['device_name'] = nvp.port_name
                    cn_binding['mac'] = nvp.mac
                    cn_binding['host'] = cfg.CONF.host
                    
                    self.uplink_bindings.append(cn_binding)
                    #self.metadata_manager.set_uplink_metadata(dpid,nvp.ofport,
                    #                                          nvp.mac)
                    pass
                
                pass
            
            # We delete flows and controllers when we restart, for now.
            br.del_controller()
            br.remove_all_flows()

            self.phys_brs[physical_network] = br
            
            # XXX: Save off the MUL offset for this physical network for
            # controller startup/connection later on.
            self.phys_br_mul_offsets[physical_network] = i
            
            i += 1
            
            pass
        pass
    
    def run_physical_bridge_controllers(self):
        for (physical_network,br) in self.phys_brs.iteritems():
            i = self.phys_br_mul_offsets[physical_network]
            
            # We have the port offset here, so we setup our controller
            # configuration -- BUT WE DON'T START THEM UNTIL METADATA is
            # written.
            if cfg.CONF.CAPNET.hosts_controllers:
                # In this case, the bridge connects to our local controller
                ccp = CapnetControllerProcesses(self,physical_network,i,br)
                ccp.ensure_running()
                ovs_ctl_str = "tcp:%s:%d" % ('127.0.0.1',ccp.mul_core_port)
                self.phys_br_controller_processes[physical_network] = ccp
            else:
                # In this case, the bridge connects to the one master controller
                port = int(cfg.CONF.CAPNET.mul_core_base_port) + i
                ovs_ctl_str = "tcp:%s:%d" % (cfg.CONF.CAPNET.master_controller_ip,
                                             port)
                pass
            
            if cfg.CONF.CAPNET.impotent_mode:
                LOG.info("setting %s into impotent mode (no controller, L2"
                         " forwarding NORMAL mode; controller would have been %s)"
                         % (br.br_name,ovs_ctl_str))
                br.add_flow(table=0,priority=0,actions="normal")
            else:
                LOG.info("setting controller for bridge %s to %s"
                         % (br.br_name,ovs_ctl_str))
                br.set_controller([ovs_ctl_str])
                pass
            pass
        pass

    def validate_bridge_mappings(self):
        for (physnet,bridge) in self.bridge_mappings.items():
            if not ip_lib.device_exists(bridge):
                LOG.error(_LE("Bridge %s for physical network %s"
                              " does not exist. Agent terminated!"),
                          bridge,physnet)
                sys.exit(1)
        pass
    
    def get_all_vif_ports(self):
        all_ports = set()
        for (pn,br) in six.iteritems(self.phys_brs):
            all_ports.update(br.get_vif_port_set())
            pass
        LOG.info("all_ports set: %s" % (str(all_ports)))
        return all_ports
    
    def get_vif_port_by_id(self,port_id):
        port = None
        for (pn,br) in six.iteritems(self.phys_brs):
            port = br.get_vif_port_by_id(port_id)
            if port:
                break
            pass
        return port

    def check_ovs_status(self):
        # XXX: we need to figure a canary approach...
        status = OVS_NORMAL
        #status = self.int_br.check_canary_table()
        if status == ovs_const.OVS_RESTARTED:
            LOG.warn(_LW("OVS is restarted. OVSNeutronAgent will reset "
                         "bridges and recover ports."))
        elif status == ovs_const.OVS_DEAD:
            LOG.warn(_LW("OVS is dead. OVSNeutronAgent will keep running "
                         "and checking OVS status periodically."))
        return status
    
    def get_phys_br(self,physical_network):
        if not self.phys_brs.has_key(physical_network):
            LOG.error(_LE("no such physical network  %s"),physical_network)
            raise NeutronException("no such physical network %s" % (physical_network))
        else:
            return self.phys_brs[physical_network]
        pass
    
    def get_phys_br_mtu(self,physical_network):
        br = self.get_phys_br(physical_network)
        if self.phys_br_mtus.has_key(physical_network):
            return self.phys_br_mtus[physical_network]
        
        mtu = 65536
        localmac = br.get_local_port_mac()
        ports = br.get_vif_ports
        for port in ports:
            if port.vif_mac == localmac:
                continue
            if port.port_name.startswith(n_const.TAP_DEVICE_PREFIX):
                continue
            ipdev = ip_lib.IPDevice(port.port_name)
            if not ipdev:
                continue
            if ipdev.link.mtu < mtu:
                mtu = ipdev.link.mtu
            pass
        
        if mtu == 0:
            LOG.warn(_LW("could not autodetect MTU for bridge %s; maybe need to"
                         " add a physical device?"),br.br_name)
            return 0
        else:
            self.phys_br_mtus[physical_network] = mtu
            LOG.debug("minimum autodetected phys MTU in bridge %s is %d",
                      br.br_name,mtu)
            return mtu
        pass

    def ensure_tap_mtu(self, tap_dev_name, phy_dev_name):
        """Ensure the MTU on the tap is the same as the physical device."""
        phy_dev_mtu = ip_lib.IPDevice(phy_dev_name).link.mtu
        tap_dev_mtu = ip_lib.IPDevice(tap_dev_name).link.mtu
        if tap_dev_mtu != phy_dev_mtu:
            LOG.debug("Setting tap device MTU to phys MTU (%s, %s, %d)",
                      tap_dev_name,phy_dev_name,phy_dev_mtu)
            ip_lib.IPDevice(tap_dev_name).link.set_mtu(phy_dev_mtu)
        pass

    def get_tap_device_name(self, interface_id):
        if not interface_id:
            LOG.warning(_LW("Invalid Interface ID, will lead to incorrect "
                            "tap device name"))
        tap_device_name = n_const.TAP_DEVICE_PREFIX + interface_id[0:11]
        return tap_device_name

    def add_tap_interface(self, network_id, network_type, physical_network,
                          segmentation_id, tap_device_name):
        """Add tap interface.

        If a VIF has been plugged into a network, this function will
        add the corresponding tap device to the relevant bridge.
        """
        if not ip_lib.device_exists(tap_device_name):
            LOG.debug("Tap device: %s does not exist on this host, skipped",
                      tap_device_name)
            return False

        br = self.get_phys_br(physical_network)

        self.ensure_tap_mtu(tap_device_name, br.br_name)

        # Check if device needs to be added to bridge
        if not tap_device_name in br.get_iface_name_list():
            LOG.debug("Adding device %s to bridge %s",tap_device_name,br.br_name)
            br.add_port(tap_device_name)
        else:
            data = {'tap_device_name': tap_device_name,
                    'bridge_name': br.br_name}
            LOG.debug("%s already exists on bridge %s",tap_device_name,br.br_name)
        return True

    def add_interface(self, network_id, network_type, physical_network,
                      segmentation_id, port_id):
        self.network_map[network_id] = CapnetSegment(network_type,
                                                     physical_network,
                                                     segmentation_id)
        tap_device_name = self.get_tap_device_name(port_id)
        return self.add_tap_interface(network_id, network_type,
                                      physical_network, segmentation_id,
                                      tap_device_name)

    def remove_port_binding(self, network_id, physical_network, port_id):
        br = self.get_phys_br(physical_network)
        tap_device_name = self.get_tap_device_name(port_id)
        br.delete_port(tap_device_name)
        pass
    
    def process_deleted_network_devices(self, device_info):
        # don't try to process removed ports as deleted ports since
        # they are already gone
        if 'removed' in device_info:
            self.deleted_devices -= device_info['removed']
        while self.deleted_devices:
            port_id = self.deleted_devices.pop()
            port = self.get_vif_port_by_id(port_id)
            self._network_ports_remove(port_id)
            if port:
                port.switch.delete_port(port.port_name)
        pass

    def process_network_devices(self, device_info):
        resync_a = False
        resync_b = False

        # Updated devices are processed the same as new ones, as their
        # admin_state_up may have changed. The set union prevents duplicating
        # work when a device is new and updated in the same polling iteration.
        devices_added_updated = (set(device_info.get('added'))
                                 | set(device_info.get('updated')))
        if devices_added_updated:
            resync_a = self.treat_devices_added_updated(devices_added_updated)

        if device_info.get('removed'):
            resync_b = self.treat_devices_removed(device_info['removed'])
        # If one of the above operations fails => resync with plugin
        return (resync_a | resync_b)
    
    def __find_vm_name(self,device_name):
        """
        This is a nasty, nasty hack to find a VM name for a nova VM, and
        pass it to Neutron.  We scrape it out of whichever libvirt
        config file contains the device name!  Oh, I feel dirty.  But
        it's Neutron's fault for not actually having a valid DNS story
        that corresponds to Nova's VM name.  Whoever thought that might
        be something anyone would ever want...
        """
        NOVADIR = '/var/lib/nova/instances'
        try:
            dirs = os.listdir(NOVADIR)
            for dir in dirs:
                lvcf = '%s/%s/libvirt.xml' % (NOVADIR,dir)
                if os.path.exists(lvcf):
                    LOG.debug("considering instance %s" % (dir,))
                    f = file(lvcf)
                    found_dname = False
                    found_vname = False
                    vname = None
                    for line in f:
                        LOG.debug("line = %s" % (line,))
                        if 'target dev' in line and device_name in line:
                            found_dname = True
                        if '<nova:name>' in line and '</nova:name>' in line:
                            found_vname = True
                            vname = line.replace(' ','')
                            vname = vname.replace('<nova:name>','')
                            vname = vname.replace('</nova:name>','')
                            vname = vname.rstrip('\n')
                            pass
                        if found_dname and found_vname:
                            break
                        pass
                    f.close()
                    if found_dname and found_vname:
                        # Found it!
                        return vname
                    pass
                pass
        except:
            import traceback
            traceback.print_exc()
            pass
        return None

    def treat_devices_added_updated(self, devices):
        try:
            devices_details_list = self.plugin_rpc.get_devices_details_list(
                self.context, devices, self.agent_id)
            LOG.debug("Got port details list: %s" % (str(devices_details_list)))
        except Exception as e:
            LOG.debug("Unable to get port details for %(devices)s: %(e)s",
                      {'devices': devices, 'e': e})
            # resync is needed
            return True

        for device_details in devices_details_list:
            device = device_details['device']
            LOG.debug("Port %s added", device)

            if 'port_id' in device_details:
                port_id = device_details['port_id']
                
                LOG.info(_LI("Port %(device)s updated. Details: %(details)s"),
                         {'device': device, 'details': device_details})
                
                if device_details['admin_state_up']:
                    # create the networking for the port
                    network_type = device_details.get('network_type')
                    segmentation_id = device_details.get('segmentation_id')
                    if self.add_interface(device_details['network_id'],
                                          network_type,
                                          device_details['physical_network'],
                                          segmentation_id,port_id):

                        # update plugin about port status
                        self.plugin_rpc.update_device_up(self.context,
                                                         device,
                                                         self.agent_id,
                                                         cfg.CONF.host)
                    else:
                        self.plugin_rpc.update_device_down(self.context,
                                                           device,
                                                           self.agent_id,
                                                           cfg.CONF.host)
                    
                    # Send a Capnet binding update, just to be safe.
                    cn_binding = { 'port_id':port_id }
                    port = self.get_vif_port_by_id(port_id)
                    cn_binding['datapath_id'] = port.switch.get_datapath_id()
                    cn_binding['ofport'] = port.ofport
                    cn_binding['device_name'] = port.port_name
                    cn_binding['host'] = cfg.CONF.host
                    
                    # XXX: if Nova VM, figure out the name -- ugh.
                    if device_details['device_owner'] == 'compute:nova' \
                      or device_details['device_owner'].startswith('compute:'):
                        vm_name = self.__find_vm_name(port.port_name)
                        if vm_name:
                            cn_binding['dns_name'] = vm_name
                        pass
        
                    LOG.debug("sending binding update: %s" % (str(cn_binding),))
                    
                    self.capnet_ext_plugin_rpc.capnet_port_binding_update(cn_binding)
                    pass
                else:
                    physical_network = device_details['physical_network']
                    self.remove_port_binding(device_details['network_id'],
                                             physical_network,
                                             device_details['port_id'])
            else:
                LOG.info(_LI("Device %s not defined on plugin"), device)
        return False

    def treat_devices_removed(self, devices):
        resync = False
        for device in devices:
            LOG.info(_LI("Attachment %s removed"), device)
            details = None
            try:
                details = self.plugin_rpc.update_device_down(self.context,
                                                             device,
                                                             self.agent_id,
                                                             cfg.CONF.host)
            except Exception as e:
                LOG.debug("port_removed failed for %(device)s: %(e)s",
                          {'device': device, 'e': e})
                resync = True
            if details and details['exists']:
                LOG.info(_LI("Port %s updated."), device)
            else:
                LOG.debug("Device %s not defined on plugin", device)
        return resync

    def scan_devices(self, previous, sync):
        device_info = {}

        # Save and reinitialise the set variable that the port_update RPC uses.
        # This should be thread-safe as the greenthread should not yield
        # between these two statements.
        updated_devices = self.updated_devices
        self.updated_devices = set()

        current_devices = self.get_all_vif_ports()
        device_info['current'] = current_devices

        if previous is None:
            # This is the first iteration of daemon_loop().
            previous = {'added': set(),
                        'current': set(),
                        'updated': set(),
                        'removed': set()}

        if sync:
            # This is the first iteration, or the previous one had a problem.
            # Re-add all existing devices.
            device_info['added'] = current_devices

            # Retry cleaning devices that may not have been cleaned properly.
            # And clean any that disappeared since the previous iteration.
            device_info['removed'] = (previous['removed'] | previous['current']
                                      - current_devices)

            # Retry updating devices that may not have been updated properly.
            # And any that were updated since the previous iteration.
            # Only update devices that currently exist.
            device_info['updated'] = (previous['updated'] | updated_devices
                                      & current_devices)
        else:
            device_info['added'] = current_devices - previous['current']
            device_info['removed'] = previous['current'] - current_devices
            device_info['updated'] = updated_devices & current_devices

        return device_info
    
    def _network_ports_remove(self, port_id):
        for port_set in self.network_ports.values():
            if port_id in port_set:
                port_set.remove(port_id)
                break

    def _device_info_has_changes(self, device_info):
        return (device_info.get('added')
                or device_info.get('updated')
                or device_info.get('removed'))
    
    def get_device_stats(self, device_info):
        device_stats = {
            'regular': {
                'added': len(device_info.get('added', [])),
                'updated': len(device_info.get('updated', [])),
                'removed': len(device_info.get('removed', []))
            }
        }
        return device_stats

    def loop_count_and_wait(self,start_time,device_stats):
        # sleep till end of polling interval
        elapsed = time.time() - start_time
        LOG.debug("Agent rpc_loop - iteration:%d completed."
                  " Processed ports statistics: %s. Elapsed:%.3f",
                  self.iter_num,device_stats,elapsed)
        if elapsed < self.polling_interval:
            time.sleep(self.polling_interval - elapsed)
        else:
            LOG.debug("Loop iteration exceeded interval (%s vs. %s)!",
                      self.polling_interval,elapsed)
        self.iter_num = self.iter_num + 1
        pass

    def daemon_loop(self):
        LOG.info(_LI("Capnet Agent RPC Daemon Started!"))
        device_info = None
        sync = True
        ovs_restarted = False
        consecutive_resyncs = 0

        while True:
            start = time.time()
            LOG.debug("Agent rpc_loop - iteration:%d started",self.iter_num)
            
            #LOG.debug("Hello plugin: %s"
            #          % (str(self.capnet_ext_plugin_rpc.hello_world())))

            if sync:
                LOG.info(_LI("Agent out of sync with plugin!"))
                #force_scan = True
                consecutive_resyncs = consecutive_resyncs + 1
                if consecutive_resyncs >= MAX_DEVICE_RETRIES:
                    LOG.warn(_LW("Clearing cache of registered ports, retrials"
                                 " to resync were > %s"),MAX_DEVICE_RETRIES)
                    device_info = None
                    #sync = False
                    consecutive_resyncs = 0
            else:
                consecutive_resyncs = 0
                pass
            
            ovs_status = self.check_ovs_status()
            if ovs_status == OVS_RESTARTED:
                LOG.debug("OVS was restarted, resetting!")
                self.reset()
            elif ovs_status == OVS_DEAD:
                # Agent doesn't apply any operations when ovs is dead, to
                # prevent unexpected failure or crash. Sleep and continue
                # loop in which ovs status will be checked periodically.
                device_stats = self.get_device_stats({}, {})
                self.loop_count_and_wait(start_time,device_stats)
                continue
            
            ovs_restarted |= (ovs_status == OVS_RESTARTED)
            
            LOG.debug("Agent rpc_loop - iteration:%d - "
                      "starting polling. Elapsed:%.3f",
                      self.iter_num,time.time() - start)
            
            # We have to poll for new devices every time, unless there's
            # a way to get notifications from OVS...
            device_info = self.scan_devices(device_info, sync)
                    
            LOG.debug("Agent rpc_loop - iteration:%(iter_num)d - "
                      "port information retrieved. "
                      "elapsed:%(elapsed).3f",
                      {'iter_num': self.iter_num,
                       'elapsed': time.time() - start})

            if sync or ovs_restarted or self._device_info_has_changes(device_info):
                try:
                    
                    LOG.debug("Starting to process devices in: %s",device_info)

                    # First process deleted ports.
                    self.process_deleted_network_devices(device_info)
                    
                    sync = False
                    
                    # Secure and wire/unwire VIFs and update their status
                    # on Neutron server
                    if ovs_restarted or self._device_info_has_changes(device_info):
                        LOG.debug("Agent loop found changes! %s", device_info)
                        # If treat devices fails - must resync with plugin
                        sync = self.process_network_devices(device_info)
                        pass
                        
                    LOG.debug("Agent rpc_loop - iteration:%d - "
                              "ports processed. Elapsed:%.3f",
                              self.iter_num,time.time() - start)

                    self.device_count = len(device_info['current'])
                    if not sync:
                        ovs_restarted = False
                except Exception:
                    LOG.exception(_LE("Error while processing VIF ports"))
                    sync = True
                pass
            
            device_stats = self.get_device_stats(device_info)
            self.loop_count_and_wait(start,device_stats)
            pass
        pass

    def set_rpc_timeout(self, timeout):
        for rpc_api in (self.plugin_rpc,self.state_rpc,
                        self.capnet_ext_plugin_rpc):
            rpc_api.client.timeout = timeout
        pass

    pass


def main():
    common_config.init(sys.argv[1:])
    common_config.setup_logging()
    n_utils.log_opt_values(LOG)
    
    try:
        bridge_mappings = n_utils.parse_mappings(cfg.CONF.CAPNET.bridge_mappings)
    except ValueError as e:
        raise ValueError(_("Parsing bridge_mappings failed: %s.") % e)
    LOG.info(_LI("Bridge mappings: %s"), bridge_mappings)
    
    if cfg.CONF.CAPNET.multi_controller_mode:
        # Make sure the controller IP exists on this node:
        validate_local_ip(cfg.CONF.CAPNET.mul_core_listen_address)
        pass
    
    polling_interval = cfg.CONF.AGENT.polling_interval
    quitting_rpc_timeout = cfg.CONF.AGENT.quitting_rpc_timeout
    
    try:
        agent = CapnetNeutronAgent(bridge_mappings, polling_interval,
                                   quitting_rpc_timeout)
    except (RuntimeError, ValueError) as e:
        LOG.error(_LE("%s Agent failed to initialize!"), e)
        sys.exit(1)
    LOG.info(_LI("Agent initialized successfully, now running... "))
    launcher = service.launch(cfg.CONF, agent)
    launcher.wait()

if __name__ == "__main__":
    main()
