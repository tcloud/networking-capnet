from oslo_log import log

from neutron.agent import securitygroups_rpc
from neutron.common import constants
from neutron.extensions import portbindings
from neutron.plugins.common import constants as p_constants
from neutron.plugins.ml2.drivers import mech_agent
from networking_capnet.common import constants as cn_const
from neutron.plugins.ml2 import driver_api as api

LOG = log.getLogger(__name__)


class CapnetMechanismDriver(mech_agent.SimpleAgentMechanismDriverBase):
    """Attach ports to networks using capnet L2 agent.

    The CapnetMechanismDriver uses the agent mechanism driver class to
    provide high-level API/sanity checking, but otherwise leaves the
    grunt work to the Capnet L2 agent, to actually implement the port
    binding.  The Capnet L2 agent must be running on the port's host,
    and the agent must be connected to at least one segment of the
    port's network.
    
    Capnet requires at least one controller per physical network that
    provides capnet virtual networks.  In our design, we support up to
    one controller per switch, but in the first version, there is a
    single controller for all physical switches.  In an
    openstack/openvswitch network, that means you'll immediately have at
    least one switch on the networkmanager node, and another switch for
    each hypervisor.  In the first generation of this driver, we only
    support a single controller instance (due to complexity of
    capability locking).  But in future generations, we will certainly
    support a single controller per switch.  However, our initial design
    supports both styles.
    
    The agent-based mechanism driver attempts to save us from any of the
    interaction with the network API itself, and tries to reduce the
    work of the agent to simple port binding -- taking a VM's "port"
    (the veth (of whatever type) of a VM) and binding it to the network
    that will carry the virtual traffic.
    
    So we reduce (nearly) all our work to this model.  Everything
    happens in the port binding in the agent.  The startup of any switch
    controller processes happens at agent startup.  Everything else
    happens later in the agent, when binding a port.  If we're running
    in multi-controller mode, if we try to bind a port on the
    hypervisor, and no ports have previously been bound on that
    hypervisor, we startup a controller on the switch, connect it to the
    controller fabric, and then bind the port.  If we're running in
    single-controller mode, we add the switch to the controller, then
    bind the port.
    """

    def __init__(self):
        vif_details = { portbindings.CAP_PORT_FILTER : False,
                        portbindings.OVS_HYBRID_PLUG: False }
        super(CapnetMechanismDriver,self).__init__(
            cn_const.AGENT_TYPE_CAPNET,cn_const.VIF_TYPE_CAPNET,vif_details)
        pass

    def get_allowed_network_types(self,agent):
        """
        For now, we only support a flat network, since we assume that
        the capnet controller runs on the single networkmanager node,
        not on each hypervisor (where local networks are isolated to a
        hypervisor).
        
        (Capnet's network "type" doesn't fit very well with ML2 types.
        Because of how capnet is structured, a single provider switch
        can host multiple virtual capnet networks without requiring a
        provider segmentation mechanism (like a vlan id or a GRE tunnel
        id) --- because Capnet is the isolation mechanism, and the
        controller can enforce isolation via the Capnet capability
        mechanism.  Thus, we should invent our own type, most likely,
        that is very similar to flat but does not impose a 1-to-1
        provider to virtual network... but we'll punt on that for now.)
        """
        return ([ cn_const.TYPE_CAPNET ])

    def get_mappings(self,agent):
        return agent['configurations'].get('bridge_mappings', {})

    def check_vlan_transparency(self,context):
        """Currently, the Capnet driver doesn't support vlan transparency."""
        return False
    
    #def create_port_postcommit(self, context):
    #    retval = super(CapnetMechanismDriver,self).create_port_postcommit(context)
    #    
    #    return retval

    #def update_port_postcommit(self, context):
    #    retval = super(CapnetMechanismDriver,self).update_port_postcommit(context)

        
    #    return retval

    #def delete_port_postcommit(self, context):
    #    retval = super(CapnetMechanismDriver,self).delete_port_postcommit(context)

        
    #    return retval

    def check_segment_for_agent(self, segment, agent):
        """Check if segment can be bound for agent.

        :param segment: segment dictionary describing segment to bind
        :param agent: agents_db entry describing agent to bind
        :returns: True iff segment can be bound for agent

        Called outside any transaction during bind_port so that derived
        MechanismDrivers can use agent_db data along with built-in
        knowledge of the corresponding agent's capabilities to
        determine whether or not the specified network segment can be
        bound for the agent.
        """
        network_type = segment[api.NETWORK_TYPE]
        
        ret = super(CapnetMechanismDriver,self).check_segment_for_agent(segment,
                                                                        agent)
        if not ret or not network_type == cn_const.VIF_TYPE_CAPNET:
            return ret

        # Add an additional sanity check here to avoid going all the way
        # to the agent unnecessarily.
        mappings = self.get_mappings(agent)
        physnet = segment[api.PHYSICAL_NETWORK]
        if not self.physnet_in_mappings(physnet, mappings):
            LOG.warn('Network %(network_id)s is connected to physical '
                     'network %(physnet)s, but agent %(agent)s reported '
                     'physical networks %(mappings)s. '
                     'The physical network must be configured on the '
                     'agent if binding is to succeed.',
                     {'network_id': segment['id'],
                      'physnet': physnet,
                      'agent': agent['host'],
                      'mappings': mappings})
            return False

        return True

    pass
