# Copyright (c) 2013, 2016 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg
from oslo_db import exception as db_exc
from oslo_log import log
import six
import sqlalchemy as sa

from neutron.common import exceptions as exc
from neutron.db import model_base
from neutron.i18n import _LI, _LW
from neutron.plugins.common import constants as p_const
from neutron.plugins.ml2 import driver_api as api
from neutron.plugins.ml2.drivers import helpers
from neutron.db import api as db_api
from networking_capnet.common import constants as cn_const
from networking_capnet.db.models.capnet_models import CapnetAllocation

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')

LOG = log.getLogger(__name__)


class CapnetIdInUse(exc.InUse):
    message = _("Unable to create the network. "
                "The VLAN %(vlan_id)s on physical network "
                "%(physical_network)s is in use.")

class CapnetTypeDriver(helpers.BaseTypeDriver):
    """
    Manage state for capnet networks with ML2.

    The CapnetTypeDriver implements the 'capnet' network_type. Capnet
    network segments provide connectivity between VMs and other devices
    using any connected IEEE 802.1D conformant physical_network, without
    the use of VLAN tags, tunneling, or other segmentation mechanisms.
    Capnet is self-isolating, so many capnet virtual networks can be
    hosted atop each physical provider network.
    
    This type driver is much like type_vlan, but it doesn't have a pool of
    IDs; rather it increments to find new IDs.
    """

    def __init__(self):
        super(CapnetTypeDriver, self).__init__()
        self.capnet_networks = dict()
        self.capnet_network_ids = dict()
        self._parse_networks(cfg.CONF.CAPNET.capnet_networks)
        pass

    def _parse_networks(self, entries):
        if not all(entries):
            msg = _("physical network name is empty")
            raise exc.InvalidInput(error_message=msg)
        else:
            for cn in entries:
                if cn == '*':
                    LOG.warn(_LW("Arbitrary capnet physical_network names"
                                 " not allowed, skipping"))
                else:
                    cna = cn.split(':')
                    if len(cna) == 1:
                        self.capnet_networks[cn] = -1
                    else:
                        cn = cna[0]
                        self.capnet_networks[cn] = int(cna[1])
                        pass
                    self.capnet_network_ids[cn] = []
                    pass
                pass
            pass
            pass
        LOG.info(_LI("Allowable capnet physical_network names: %s"),
                     str(self.capnet_networks.keys()))

    def _init_capnet_allocations(self):
        session = db_api.get_session()
        with session.begin(subtransactions=True):
            # get existing allocations for all physical networks
            allocs = (session.query(CapnetAllocation).with_lockmode('update'))
            for alloc in allocs:
                if not self.capnet_networks.has_key(alloc.physical_network):
                    LOG.warn(_LI("allocated physical_network %s not in config" \
                                 % (alloc.physical_network)))
                    self.capnet_networks[alloc.physical_network] = -1
                    pass
                self.capnet_network_ids[alloc.physical_network].append(alloc.capnet_id)
                pass
            
            pass
        pass

    def get_type(self):
        return cn_const.TYPE_CAPNET

    def initialize(self):
        self._init_capnet_allocations()
        LOG.info(_LI("ML2 CapnetTypeDriver initialization complete"))

    def is_partial_segment(self, segment):
        return segment.get(api.SEGMENTATION_ID) is None

    def validate_provider_segment(self, segment):
        physical_network = segment.get(api.PHYSICAL_NETWORK)
        segmentation_id = segment.get(api.SEGMENTATION_ID)
        if physical_network:
            if not self.capnet_networks.has_key(physical_network):
                msg = (_("physical_network '%s' unknown "
                         " for Capnet provider network") % physical_network)
                raise exc.InvalidInput(error_message=msg)
            pass
        elif segmentation_id:
            msg = _("segmentation_id requires physical_network for Capnet"
                    " provider network")
            raise exc.InvalidInput(error_message=msg)

        for (key,value) in segment.items():
            if value and key not in [api.NETWORK_TYPE,
                                     api.PHYSICAL_NETWORK,
                                     api.SEGMENTATION_ID]:
                msg = _("%s prohibited for Capnet provider network") % key
                raise exc.InvalidInput(error_message=msg)

    # Even though we're not subclassing SegmentTypeDriver, we follow its API.
    def allocate_partially_specified_segment(self, session, **filters):
        network_type = self.get_type()
        with session.begin(subtransactions=True):
            select = (session.query(CapnetAllocation).with_lockmode('update'))
            
            # Choose a physical network if not specified:
            physical_network = filters.get(api.PHYSICAL_NETWORK)
            if not physical_network:
                most_remaining = 0
                for pn in self.capnet_network_ids.keys():
                    if self.capnet_networks[pn] == -1:
                        LOG.info("using Capnet physical network %s with infinite tenant network space" % (pn,))
                        physical_network = pn
                        break
                    else:
                        remaining = self.capnet_networks[pn] \
                            - len(self.capnet_network_ids[pn])
                        if remaining > most_remaining:
                            most_remaining = remaining
                            physical_network = pn
                        pass
                    pass
                if not physical_network:
                    LOG.warn(_LW("no Capnet physical networks have space left"))
                    return
                pass
            
            segmentation_id = filters.get(api.SEGMENTATION_ID)
            if not segmentation_id:
                maxid = 0
                for row in select:
                    if row.capnet_id > maxid:
                        maxid = row.capnet_id
                    pass
                segmentation_id = maxid + 1
                pass
            elif segmentation_id in self.network_ids[physical_network]:
                LOG.warn("Capnet network id %d already used on physical network %s" % (segmentation_id,physical_network))
                return
            
            newcn = CapnetAllocation(physical_network=physical_network,
                                     capnet_id=segmentation_id)
            session.add(newcn)
            
            self.capnet_network_ids[physical_network].append(segmentation_id)

            LOG.debug("Capnet segment allocated capnet_id %d on physical network %s" % (segmentation_id,physical_network))
            return newcn

    def allocate_fully_specified_segment(self, session, **raw_segment):
        return self.allocate_partially_specified_segment(session,raw_segment)

    def reserve_provider_segment(self, session, segment):
        filters = {}
        physical_network = segment.get(api.PHYSICAL_NETWORK)
        if physical_network is not None:
            filters['physical_network'] = physical_network
            capnet_id = segment.get(api.SEGMENTATION_ID)
            if capnet_id is not None:
                filters['capnet_id'] = capnet_id

        if self.is_partial_segment(segment):
            alloc = self.allocate_partially_specified_segment(session, **filters)
            if not alloc:
                raise exc.NoNetworkAvailable()
        else:
            alloc = self.allocate_fully_specified_segment(session, **filters)
            if not alloc:
                raise CapnetIdInUse(**filters)

        return {api.NETWORK_TYPE: cn_const.TYPE_CAPNET,
                api.PHYSICAL_NETWORK: alloc.physical_network,
                api.SEGMENTATION_ID: alloc.capnet_id,
                api.MTU: self.get_mtu(alloc.physical_network)}

    def allocate_tenant_segment(self, session):
        alloc = self.allocate_partially_specified_segment(session)
        if not alloc:
            return
        return {api.NETWORK_TYPE: cn_const.TYPE_CAPNET,
                api.PHYSICAL_NETWORK: alloc.physical_network,
                api.SEGMENTATION_ID: alloc.capnet_id,
                api.MTU: self.get_mtu(alloc.physical_network)}

    def release_segment(self, session, segment):
        physical_network = segment[api.PHYSICAL_NETWORK]
        capnet_id = segment[api.SEGMENTATION_ID]

        self.capnet_network_ids[physical_network].remove(capnet_id)

        with session.begin(subtransactions=True):
            query = (session.query(CapnetAllocation).
                     filter_by(physical_network=physical_network,
                               capnet_id=capnet_id))
            count = query.delete()
            if count:
                LOG.debug("Releasing capnet %(capnet_id)s on physical"
                          " network %(physical_network)s",
                          { 'capnet_id': capnet_id,
                            'physical_network': physical_network })
                pass
            pass

        if not count:
            LOG.warning(_LW("No capnets %(capnet_id)s found on physical "
                            "network %(physical_network)s"),
                            { 'capnet_id': capnet_id,
                              'physical_network': physical_network })
            pass
        pass

    def get_mtu(self, physical_network):
        seg_mtu = super(CapnetTypeDriver, self).get_mtu()
        mtu = []
        if seg_mtu > 0:
            mtu.append(seg_mtu)
        if physical_network in self.physnet_mtus:
            mtu.append(int(self.physnet_mtus[physical_network]))
        return min(mtu) if mtu else 0
