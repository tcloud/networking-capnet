
from oslo_config import cfg
from oslo_log import log as logging

from neutron.i18n import _LI, _LW
from neutron.plugins.ml2 import driver_api as api
#from neutron.db import common_db_mixin
from neutron.common import exceptions as n_exc
from networking_capnet.common import constants as cn_const
from networking_capnet.extensions import capnet as cap_ext
from networking_capnet.db.models.capnet_models import CapnetNetwork
from neutron.api.v2 import attributes
from neutron.db import models_v2

cfg.CONF.import_group('CAPNET','networking_capnet.common.config')

LOG = logging.getLogger(__name__)


class CapnetExtensionDriver(api.ExtensionDriver):
#                            common_db_mixin.CommonDbMixin):
    _supported_extension_alias = 'capnet'

    def initialize(self):
        LOG.info(_LI("CapnetExtensionDriver initialization complete"))

    @property
    def extension_alias(self):
        return self._supported_extension_alias

    def process_create_network(self, context, data, result):
        LOG.info(_LI("data = %s, result = %s" % (str(data),str(result))))
        
        # If there are *any* capnet attrs, fill in any missing ones;
        # else ignore this request -- we add nothing.
        hasCapnet = False
        for k in data.keys():
            if cap_ext.EXTENDED_ATTRIBUTES_2_0['networks'].has_key(k) \
              and data[k] != attributes.ATTR_NOT_SPECIFIED:
                hasCapnet = True
                break
            pass
        
        if data.has_key('provider:network_type') \
            and data['provider:network_type'] == cn_const.TYPE_CAPNET:
            hasCapnet = True
        # XXX: we don't want to parse the config every net create; how do we
        # get a handle to the type driver from here?
        #elif data.has_key('provider:physical_network') \
        #    and data['provider:physical_network'] in cfg.CONF.CAPNET.capnet_networks.keys():
        #    hasCapnet = True
            pass
        
        if not hasCapnet:
            return
        
        dbdict = {}
        # Add in default attributes if they are missing.
        for ck in cap_ext.EXTENDED_ATTRIBUTES_2_0['networks'].keys():
            # Translate the neutron API Capnet network attr
            rk = ck.replace('capnet:','')
            
            if data.has_key(ck) and data[ck] != attributes.ATTR_NOT_SPECIFIED:
                dbdict[rk] = data[ck]
                result[ck] = data[ck]
            else:
                def_conf_val = getattr(cfg.CONF.CAPNET,'default_%s' % (rk,))
                dbdict[rk] = def_conf_val
                result[ck] = def_conf_val
                pass
            
            # Do some real True/False conversion...
            if rk in ('arp_responder','dhcp_flows','metadata_flows'):
                v = dbdict[rk]
                if type(v) == str \
                  and v in ['false','False','f','F','FALSE','0','n','no','No','NO']:
                    v = False
                else:
                    v = True
                    pass
                dbdict[rk] = v
                result[ck] = v
                pass
                    
            pass
        dbdict['network_id'] = result['id']
        
        # Create the network extension attributes.
        with context.session.begin(subtransactions=True):
            db = CapnetNetwork(**dbdict)
            context.session.add(db)
        return

    def process_update_network(self, context, data, result):
        # If there are *any* capnet attrs, it's an error.  We don't allow
        # updates; once they start something, that's what they get.
        # XXX: we need to relax this... the behavior of course is to "reboot"
        # the capability network if they change *any* of the capnet attrs.
        hasCapnet = False
        for k in data.keys():
            if cap_ext.EXTENDED_ATTRIBUTES_2_0['networks'].has_key(k):
                hasCapnet = True
                break
            pass
        
        if not hasCapnet:
            return
        else:
            raise n_exc.BadRequest("cannot update Capnet network properties!")
        pass

    def extend_network_dict(self, session, base_model, result):
        pass

    pass
