
from oslo_log import log

from neutron.plugins.ml2.plugin import Ml2Plugin

from neutron.common import rpc as n_rpc
from neutron.common import topics
from neutron.plugins.ml2 import db as ml2_db
from neutron.i18n import _LE

from networking_capnet.common import constants as cn_const
from networking_capnet.common import topics as cn_topics
#from networking_capnet.api.rpc import util as cn_rpc_util
from networking_capnet.api.rpc.handlers.capnet_rpc import CapnetExtRpcCallbacks
from networking_capnet.api.rpc.agentnotifiers.capnet_rpc_agent_api \
  import CapnetExtAgentAPI


LOG = log.getLogger(__name__)


class CapnetMl2Plugin(Ml2Plugin):
    """
    This simple class wraps the Ml2Plugin, because it's not modular
    enough to allow its RPC endpoints to be extended!  I can't easily
    see another way to hook in, either, that would work in the style of
    plugin RPC.  Maybe this can be recast as a custom extension later.
    Oh, of course we could probably stuff this stuff into a mechanism
    driver, but that might require a second messaging connection.
    """
    def __init__(self):
        super(CapnetMl2Plugin,self).__init__()
        try:
            self.agent_notifiers[cn_const.AGENT_TYPE_CAPNET] = \
              CapnetExtAgentAPI(self)
        except:
            LOG.exception(_LE("failed to setup Capnet Agent Notifiers;"
                              " Capnet networks won't work!"))
        pass

    def start_rpc_listeners(self):
        """Start the RPC loop to let the plugin communicate with agents."""
        LOG.debug("starting default rpc listeners")
        
        #super(CapnetMl2Plugin,self).start_rpc_listeners()
        self._setup_rpc()
        self.topic = topics.PLUGIN
        self.conn = n_rpc.create_connection(new=True)
        self.conn.create_consumer(self.topic, self.endpoints, fanout=False)
        
        LOG.debug("starting rpc listeners")
        
        self.extra_endpoints = \
            { cn_topics.PLUGIN_CAPNET : [ CapnetExtRpcCallbacks() ] }
        for (topic,endpoints) in self.extra_endpoints.iteritems():
            self.conn.create_consumer(topic,endpoints,fanout=False)
            pass
        return self.conn.consume_in_threads()
    
    pass
